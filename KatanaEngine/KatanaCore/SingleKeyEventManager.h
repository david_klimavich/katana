#ifndef SingleKeyEventManager_
#define SingleKeyEventManager_

#include "AzulCore.h"
#include "ContainerAlias.h"
#include "InputEventType.h"

class Inputable;
class SingleKeyEventManager {
public:
	explicit SingleKeyEventManager(AZUL_KEY k_);
	virtual ~SingleKeyEventManager();

	void Register(Inputable&, INPUT_EVENT_TYPE eventType_);
	void Deregister(Inputable&, INPUT_EVENT_TYPE);
	void ProcessKeyEvent();

	bool IsEmpty();

private:

	void ProcessPressedList();
	void ProcessReleasedList();
	void ProcessHeldDownList(); 
	void ProcessHeldUpList(); 

	AZUL_KEY mKey;
	KL::InputableList pressedList;
	KL::InputableList releasedList;
	KL::InputableList heldDownList;
	KL::InputableList heldUpList; 

	bool lastKeyState;
	bool currentlyDown;
};
#endif // !SingleKeyEventManager_
