#ifndef ModelManager_
#define ModelManager_

#include <map>
#include <string>
#include "Model.h"
#include "ContainerAlias.h"
#include "KatanaEngine.h"

class ModelManagerAttorney;
class ModelManager {
	friend class ModelManagerAttorney;
public:
	static void LoadModel(std::string key_, std::string fileName_);
	static Model* GetModel(std::string key_);

	// default models
	static std::string UnitBox_WF;
	static std::string UnitSphere;
	static std::string UnitSquareXY;

private:
	ModelManager();
	ModelManager(const ModelManager&) = delete;
	ModelManager operator=(const ModelManager&) = delete;
	~ModelManager() = default;

	static void CleanUp();
	void PrivateCleanUp();

	static ModelManager* instance;
	static ModelManager& GetInstance() {
		if(!instance) {
			instance = new ModelManager();
		}
		return *instance;
	}

	// Private Methods
	void PrivateLoadModel(std::string key_, std::string fileName_);
	Model* PrivateGetModel(std::string key_);
	void LoadDefault();

	bool ModelInMap(std::string key_);

	KM::ModelMap modelMap;
	std::string rootPath = "Models/";

};

#endif // !ModelManager_
