#ifndef Visualizer_
#define Visualizer_
#include "Vect.h"
#include "GraphicsObject_WireframeConstantColor.h"
#include "VisualizerBroker.h"

class Collidable;
class CollisionVolumeBSphere;
class CollisionVolumeAABB;
class CollisionVolumeOBB;

class Visualizer {
public:
	static void ShowBSphere(const CollisionVolumeBSphere& sphere_, const Vect& color_ = DEFAULT_COLOR);
	static void ShowAABB(const CollisionVolumeAABB& AABB_, const Vect& color_ = DEFAULT_COLOR);
	static void ShowOBB(const CollisionVolumeOBB& OBB_, const Vect& color_ = DEFAULT_COLOR);
	static void ProcessVisualizerBroker();
	static void ShowCollisionVolume(const Collidable* collidable_, const Vect& color_);

private:
	friend class VisualizerRenderCommand;
	friend class VisualizerRenderAABB_cmd;
	friend class VisualizerRenderOBB_cmd;

	Visualizer();
	Visualizer(const Visualizer&) = delete;
	Visualizer& operator=(const Visualizer&) = delete;
	~Visualizer() {
		instance = nullptr;
		delete WFUnitSphere;
		delete WFAABB;
		delete WFOBB;
	}
	static Visualizer* instance;
	static Visualizer& GetInstance() {
		if(!instance) {
			instance = new Visualizer;
		}
		return *instance;
	}

	friend class KatanaEngine;
	static void Delete() {
		delete instance;
	}

	ViualizerBroker mVisualizerBroker;
	static Vect DEFAULT_COLOR;
	GraphicsObject_WireframeConstantColor* WFUnitSphere;
	GraphicsObject_WireframeConstantColor* WFAABB;
	GraphicsObject_WireframeConstantColor* WFOBB;

	CommandBase* renderCmd;

	void PrivRenderBSphere(Matrix& sphereWorld_, const Vect& color_) const;
	void PrivRenderAABB(Matrix& AABBWorld_, const Vect& color_) const;
	void PrivRenderOBB(Matrix& OBBWorld_, const Vect& color_) const;

	void PrivShowBSphere(const CollisionVolumeBSphere& sphere_, const Vect& color_);
	void PrivShowAABB(const CollisionVolumeAABB& AABB_, const Vect& color_);
	void PrivShowOBB(const CollisionVolumeOBB& OBB_, const Vect& color_);

	void PrivProcessVisualizerBroker();
	void PrivShowCollisionVolume(const Collidable* collidable_, const Vect& color_);
};
#endif // !Visualizer_
