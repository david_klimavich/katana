#ifndef DrawableAttorney_
#define DrawableAttorney_

#include "Drawable.h"

class DrawableManager;
class DrawableRegistrationCommand;
class DrawableDeregistrationCommand;

class DrawableAttorney {
public:
	class GameLoop {
	private:
		friend class DrawableManager;
		static void Draw(Drawable& drawable_)
		{
			drawable_.Draw();
		}
	};
	class Registration {
		friend class DrawRegistrationCommand;
		friend class DrawDeregistrationCommand;
	private:
		static void SceneRegistration(Drawable& drawable_)
		{
			drawable_.SceneRegistration();
		}
		static void SceneDeregistration(Drawable& drawable_)
		{
			drawable_.SceneDeregistration();
		}

	};
};
#endif // !DrawableAttorney.h
