#ifndef ImageManager_
#define ImageManager_

#include "ContainerAlias.h"
#include <assert.h>

class ImageManager {
public:
	static void LoadImageFromFile(std::string key_, std::string fileName_);
	static Image* GetImage(std::string key_);

private:
	ImageManager() = default;
	ImageManager(const ImageManager&) = delete;
	ImageManager& operator=(const ImageManager&) = delete;
	~ImageManager() {
		assert(instance);
		delete instance;
	};

	static void CleanUp();
	void PrivateCleanUp();

	static ImageManager* instance;
	static ImageManager& GetInstance() {
		if(!instance)
		{
			instance = new ImageManager;
		}
		return *instance;
	}

	void PrivateLoadImage(std::string key_, std::string fileName_);
	Image* PrivateGetImage(std::string key_);

	KM::ImageMap mImageMap;
};
#endif // !ImageManagers_
