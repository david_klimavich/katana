#ifndef CollisionVolumeAABB_
#define CollisionVolumeAABB_

#include "CollisionVolumeBB.h"
#include "Vect.h"
#include "Matrix.h"
#include "Align16.h"

class CollisionVolumeAABB : public CollisionVolumeBB, public Align16 {
public:
	CollisionVolumeAABB();
	~CollisionVolumeAABB();

	const Vect& getMin() const {
		return min;
	}
	const Vect& getMax() const {
		return max;
	}
	const Matrix& getWorld() const override {
		return *mpWorld;
	}
	const Vect& getHalfDiagonal() const override {
		return mHalfDiagonal;
	}
	const Vect& getCenter() const override {
		return mCenter;
	}
	float getScaleSquared() const override {
		return mScaleSquared;
	}
	const Matrix& getInv() const override {
		Matrix *identity = new Matrix(IDENTITY);
		return *identity; // magic
	}

	void ResetAABB();
	void ComputeData(Model* model_, const Matrix& matrix_) override;
	void ComputeData(const CollisionVolumeBSphere& bsphere_);
	void DebugView(const Vect& color_) const;

	bool IntersectAccept(const CollisionVolume& other_) const override;
	bool IntersectVisit(const CollisionVolumeBSphere& other) const override;
	bool IntersectVisit(const CollisionVolumeAABB& other_) const override;
	bool IntersectVisit(const CollisionVolumeOBB& other_) const override;

private:
	Vect min;
	Vect max;

	Matrix *mpWorld;
	Vect mHalfDiagonal;
	Vect mCenter;
	float mScaleSquared;


};

#endif CollisionVolumeAABB_