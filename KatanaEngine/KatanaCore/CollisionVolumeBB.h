#ifndef CollisionVolumeBB_
#define CollisionVolumeBB_

#include "CollisionVolume.h"

class CollisionVolumeBB : public CollisionVolume {
public:
	CollisionVolumeBB() = default;
	virtual ~CollisionVolumeBB() = default;

	virtual const Vect& getCenter() const = 0;
	virtual const Vect& getHalfDiagonal() const = 0;
	virtual float getScaleSquared() const = 0;
	virtual const Matrix& getWorld() const = 0;
	virtual const Matrix& getInv() const = 0;

private:

};

#endif