#ifndef VisualizerRenderAABB_cmd_
#define VisualizerRenderAABB_cmd_

#include "CommandBase.h"
#include "Visualizer.h"
#include "Align16.h"

class VisualizerRenderAABB_cmd : public CommandBase, public Align16
{
public:
	VisualizerRenderAABB_cmd(Matrix& world_, const Vect& color_) : mWorld(world_), mColor(color_) {};

	void Execute() override
	{
		Visualizer::GetInstance().PrivRenderAABB(mWorld, mColor);
	}

private:
	Matrix mWorld; 
	const Vect mColor;

};

#endif // !VisualizerRenderCommand_
