#include "KeyboardEventManager.h"
#include "DebugOut.h"

KeyboardEventManager::KeyboardEventManager() {}

KeyboardEventManager::~KeyboardEventManager() {

	// delete all singleKeyEventManagers
	auto it = skEventManagersMap.begin();
	while(it != skEventManagersMap.end()) {
		auto tmp = it;
		it++;
		delete (*tmp).second;
	}
}

void KeyboardEventManager::Register(Inputable &inputable_, AZUL_KEY key_,
	INPUT_EVENT_TYPE eventType_) {
	auto search = skEventManagersMap.find(key_);
	if(search != skEventManagersMap.end()) {
		search->second->Register(inputable_, eventType_);
	} else {
		SingleKeyEventManager *newManager = new SingleKeyEventManager(key_);
		skEventManagersMap.insert(skEventManagersMap.end(), { key_,newManager });
		newManager->Register(inputable_, eventType_);
	}
}

void KeyboardEventManager::Deregister(Inputable &inputable_, AZUL_KEY key_,
	INPUT_EVENT_TYPE eventType_) {
	auto search = skEventManagersMap.find(key_);
	search->second->Deregister(inputable_, eventType_);
	if(search->second->IsEmpty()) {
		skEventManagersMap.erase(search);
	}
}

void KeyboardEventManager::ProcessKeyEvents() {
	for(auto it = skEventManagersMap.begin(); it != skEventManagersMap.end(); it++) {
		it->second->ProcessKeyEvent();
	}
}
