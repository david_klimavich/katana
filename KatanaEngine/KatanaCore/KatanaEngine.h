#ifndef GAME_H
#define GAME_H

#include "AzulCore.h"
#include "projectSettings.h"
#include "CameraManager.h"
#include "../Resources/EngineSettings.h"

class KatanaEngine : public Engine {
public:

	///-------------------------------------------------------------------------------------------------
	/// @fn	static void KatanaEngine::run();
	///
	/// @brief	/** Constructor for the Engine
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	static void run();

	static void EndGame(); 

	///-------------------------------------------------------------------------------------------------
	/// @fn	static int KatanaEngine::GetWindowHeight();
	///
	/// @brief	Gets window height
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///
	/// @return	The window height.
	///-------------------------------------------------------------------------------------------------

	static int GetWindowHeight();

	///-------------------------------------------------------------------------------------------------
	/// @fn	static int KatanaEngine::GetWindowWidth();
	///
	/// @brief	Gets window width
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///
	/// @return	The window width.
	///-------------------------------------------------------------------------------------------------

	static int GetWindowWidth();

	// should be private and friend to time freeze

	///-------------------------------------------------------------------------------------------------
	/// @fn	static float KatanaEngine::GetTimeInSeconds();
	///
	/// @brief	Gets time in seconds
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///
	/// @return	The time in seconds.
	///-------------------------------------------------------------------------------------------------

	static float GetTimeInSeconds();

private:

	KatanaEngine() {};
	KatanaEngine(const KatanaEngine&) = delete;
	KatanaEngine operator=(const KatanaEngine&) = delete;
	~KatanaEngine() {};

	static KatanaEngine* instance;

	///-------------------------------------------------------------------------------------------------
	/// @fn	static KatanaEngine& KatanaEngine::GetInstance()
	///
	/// @brief	Gets the instance
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///
	/// @return	The instance.
	///-------------------------------------------------------------------------------------------------

	static KatanaEngine& GetInstance() {
		if(!instance) {
			instance = new KatanaEngine;
		}
		return *instance;
	}

	// Private Methods

	///-------------------------------------------------------------------------------------------------
	/// @fn	void KatanaEngine::privateRun();
	///
	/// @brief	Runs the game loop
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void privateRun();

	void PrivateEndGame(); 

	///-------------------------------------------------------------------------------------------------
	/// @fn	float KatanaEngine::PrivateGetTimeInSeconds();
	///
	/// @brief	Private get time in seconds
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///
	/// @return	A float.
	///-------------------------------------------------------------------------------------------------

	float PrivateGetTimeInSeconds();

	///-------------------------------------------------------------------------------------------------
	/// @fn	virtual void KatanaEngine::Initialize();
	///
	/// @brief	Initializes this object
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	virtual void Initialize();

	///-------------------------------------------------------------------------------------------------
	/// @fn	virtual void KatanaEngine::LoadContent() override;
	///
	/// @brief	Loads the content
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	virtual void LoadContent() override;

	///-------------------------------------------------------------------------------------------------
	/// @fn	virtual void KatanaEngine::Update() override;
	///
	/// @brief	Updates this object
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	virtual void Update() override;

	///-------------------------------------------------------------------------------------------------
	/// @fn	virtual void KatanaEngine::Draw() override;
	///
	/// @brief	Draws this object
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	virtual void Draw() override;

	///-------------------------------------------------------------------------------------------------
	/// @fn	virtual void KatanaEngine::UnLoadContent() override;
	///
	/// @brief	Un load content
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	virtual void UnLoadContent() override;

	// accessors 

	///-------------------------------------------------------------------------------------------------
	/// @fn	int KatanaEngine::PrivateGetHeight();
	/// @brief	Private get height
	/// @return	An int.
	///-------------------------------------------------------------------------------------------------

	int PrivateGetHeight();

	///-------------------------------------------------------------------------------------------------
	/// @fn	int KatanaEngine::PrivateGetWidth();
	/// @brief	Private get width
	/// @return	An int.
	///-------------------------------------------------------------------------------------------------

	int PrivateGetWidth();

	///-------------------------------------------------------------------------------------------------
	/// @fn	void KatanaEngine::LoadAllResources();
	/// @brief	Loads all resources
	///-------------------------------------------------------------------------------------------------
	void LoadAllResources();

	///-------------------------------------------------------------------------------------------------
	/// @fn	void KatanaEngine::SetStartingScene();
	/// @brief	Sets starting scene
	///-------------------------------------------------------------------------------------------------
	void SetStartingScene();

	///-------------------------------------------------------------------------------------------------
	/// @fn	void KatanaEngine::InitGameWindow();
	///
	/// @brief	Initializes the game window
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void InitGameWindow();


	// legacy
	KatanaEngine(const char* windowName, const int Width, const int Height);


};

#endif