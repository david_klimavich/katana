#ifndef NullScene_
#define NullScene_

#include "Scene.h"

class NullScene : public Scene {

public:
	NullScene()
	{};
	~NullScene()
	{};

	void Init() override
	{};

	void CleanUp() override
	{};

};
#endif // !NullScene_
