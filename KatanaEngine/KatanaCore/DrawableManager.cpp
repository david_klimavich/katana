#include "DrawableManager.h"
#include "DrawableAttorney.h"
#include "DebugOut.h"


void DrawableManager::Register(Drawable &drawable_) {
	KLI::DrawableListIt it = drawableList.insert(drawableList.end(), &drawable_);
	drawable_.SetIt(it);
}

void DrawableManager::Deregister(Drawable &drawable_) {
	drawableList.erase(drawable_.GetIt());
}

void DrawableManager::ProcessElements() {
	for(auto it = drawableList.begin(); it != drawableList.end(); it++) {
		DrawableAttorney::GameLoop::Draw(**it);
	}
}

DrawableManager::~DrawableManager() {
	drawableList.clear();
}