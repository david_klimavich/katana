#ifndef InputableDeregistrationCommand_
#define InputabelDeregistrationCommand_

#include "CommandBase.h"
#include "Inputable.h"
class InputableDeregistrationCommand : public CommandBase {
public:
	InputableDeregistrationCommand(Inputable&, AZUL_KEY, INPUT_EVENT_TYPE);
	~InputableDeregistrationCommand();

	void Execute() override;

private:
	Inputable* mpInputable;
	AZUL_KEY mKey;
	INPUT_EVENT_TYPE mEventType;
};
#endif // !InputableDeregistrationCommand_
