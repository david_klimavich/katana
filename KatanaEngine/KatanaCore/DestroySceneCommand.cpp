#include "DestroySceneCommand.h"

DestroySceneCommand::DestroySceneCommand(Scene &scene_)
	: mpScene(&scene_) {}

DestroySceneCommand::~DestroySceneCommand() {}

void DestroySceneCommand::Execute() {
	delete mpScene;
	mpScene = nullptr;
}
