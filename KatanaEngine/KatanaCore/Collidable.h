#ifndef Collidable_
#define Collidable_

#include "DebugOut.h"
#include "CollisionRegistrationCommand.h"
#include "CollisionDeregistrationCommand.h"
#include "SceneAttorney.h"
#include "CollisionVolumeBSphere.h"

class Collidable {

public:

	enum CollisionVolumeType {
		BSPHERE,
		AABB,
		OBB
	};

	Collidable() = default;
	virtual ~Collidable();

	virtual void Collision(Collidable&) = 0;

	// move to protected later
	const CollisionVolume& GetCollisionVolume() const;
	const CollisionVolumeBSphere& GetDefaultVolume()const;

protected:
	void SetColliderModel(Model* model_, CollisionVolumeType type_);
	void UpdateCollisionData(const Matrix& matrix_);

	template<typename CollidableType>
	void CollisionRegistration(CollidableType& collidable_) {
		if(mpRegCmd == nullptr) {
			mpRegCmd = new CollisionRegistrationCommand<CollidableType>(collidable_);
		}
		SceneAttorney::Registration::SubmitCommand(mpRegCmd);
	};

	template<typename CollidableType>
	void CollisionDeregistration(CollidableType& collidable_) {
		if(mpDeregCmd == nullptr) {
			mpDeregCmd = new CollisionDeregistrationCommand<CollidableType>(collidable_);
		}
		SceneAttorney::Registration::SubmitCommand(mpDeregCmd);
	};
private:
	// members 
	SceneRegistrationCommand * mpRegCmd = nullptr;
	SceneRegistrationCommand* mpDeregCmd = nullptr;

	Model* mpColModel;
	CollisionVolume *mpCollisionVolume;

	CollisionVolumeBSphere mDefaultSphere;
};

#endif