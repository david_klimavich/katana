#ifndef InputableAttorney_
#define InputableAttorney_

#include "Inputable.h"

class InputableAttorney {
public:
	class KeyEvent {
	private:
		friend class SingleKeyEventManager;
		static void KeyPressed(Inputable& inputable_, AZUL_KEY key_)
		{
			inputable_.KeyPressed(key_);
		}
		static void KeyReleased(Inputable& inputable_, AZUL_KEY key_)
		{
			inputable_.KeyReleased(key_);
		}
		static void KeyHeldDown(Inputable& inputable_, AZUL_KEY key_)
		{
			inputable_.KeyHeldDown(key_); 
		}
		static void KeyHeldUp(Inputable& inputable_, AZUL_KEY key_)
		{
			inputable_.KeyHeldUp(key_); 
		}
	};
	class Registration {
		friend class InputableRegistrationCommand;
		friend class InputableDeregistrationCommand;
	private:
		static void SceneRegistration(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
		{
			inputable_.SceneRegistration(key_, eventType_);
		}
		static void SceneDeregistration(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
		{
			inputable_.SceneDeregistration(key_, eventType_);
		}

	};
};
#endif // !DrawableAttorney.h
