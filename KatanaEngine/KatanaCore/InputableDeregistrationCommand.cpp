#include "InputableDeregistrationCommand.h"
#include "InputableAttorney.h"

InputableDeregistrationCommand::InputableDeregistrationCommand(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
	: mpInputable(&inputable_), mKey(key_), mEventType(eventType_)
{}

InputableDeregistrationCommand::~InputableDeregistrationCommand()
{}

void InputableDeregistrationCommand::Execute()
{
	InputableAttorney::Registration::SceneDeregistration(*mpInputable, mKey, mEventType);
	delete this;
}
