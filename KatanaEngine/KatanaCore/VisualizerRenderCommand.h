#ifndef VisualizerRenderCommand_
#define VisualizerRenderCommand_

#include "CommandBase.h"
#include "Matrix.h"
#include "Visualizer.h"
#include "Align16.h"

class VisualizerRenderCommand : public CommandBase, public Align16 {
private:
	Matrix mWorld;
	const Vect mColor;

public:
	VisualizerRenderCommand(Matrix& world_, const Vect& color_) : mWorld(world_), mColor(color_) {};
	~VisualizerRenderCommand() {
		//DebugMsg::out("Visualizer Render Command Delete\n");
	};

	void Execute() override {
		Visualizer::GetInstance().PrivRenderBSphere(mWorld, mColor);
		delete this;
	};

};


#endif
