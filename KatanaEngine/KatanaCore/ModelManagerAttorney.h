#ifndef ModelManagerAttorney_
#define ModelManagerAttorney_
#include "ModelManager.h"

class KatanaEngine;
class ModelManagerAttorney {
	friend class KatanaEngine;

private:
	static void CleanUp()
	{
		ModelManager::CleanUp();
	}
};
#endif // !ModelManagerAttorney_
