#include "GameObject.h"
#include "GameObjectEntryCommand.h"
#include "GameObjectExitCommand.h"
#include "SceneAttorney.h"
#include "DestroyObjectCommand.h"

GameObject::GameObject() {
	mpEntryCommand = new GameObjectEntryCommand(*this);
	mpExitCommand = new GameObjectExitCommand(*this);
	mpDestroyObjectCommand = new DestroyObjectCommand(*this);
}

GameObject::~GameObject() {
	delete mpEntryCommand;
	delete mpExitCommand;

	mpEntryCommand = nullptr;
	mpExitCommand = nullptr;
}

void GameObject::SubmitEntry() {
	SceneAttorney::Registration::SubmitCommand(mpEntryCommand);
}

void GameObject::SubmitExit() {
	SceneAttorney::Registration::SubmitCommand(mpExitCommand);
}

void GameObject::MarkForDestroy()
{
	SceneAttorney::Command::SubmitCommand(mpDestroyObjectCommand);
}
