#include "SpriteManager.h"
#include "Sprite.h"
#include "SpriteAttorney.h"

void Sprite2DManager::Register(Sprite &sprite_) {
	KLI::SpriteListIt it =
		spriteList.insert(spriteList.end(), &sprite_);
	sprite_.SetIt(it);
}

void Sprite2DManager::Deregister(Sprite &sprite_) {
	spriteList.erase(sprite_.GetItRef());
}

void Sprite2DManager::ProcessElements() {
	for(auto it = spriteList.begin(); it != spriteList.end(); it++)
	{
		SpriteAttorney::GameLoop::Draw2D(**it);
	}
}
