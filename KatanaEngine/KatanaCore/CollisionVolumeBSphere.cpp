#include "CollisionVolumeBSphere.h"
#include "Matrix.h"
#include "Model.h"
#include "Katana_Math.h"
#include "Visualizer.h"

void CollisionVolumeBSphere::ComputeData(Model * pModel_, const Matrix & matrix_) {
	mCenter = pModel_->getCenter() * matrix_;
	Vect matrixScale = Vect(0, 0, 1, 0) * matrix_;
	mRadius = pModel_->getRadius() * matrixScale.mag();
}

void CollisionVolumeBSphere::DebugView(const Vect & col) const {
	Visualizer::ShowBSphere(*this, col);
}

bool CollisionVolumeBSphere::IntersectAccept(const CollisionVolume & other_) const {
	return other_.IntersectVisit(*this);
}

bool CollisionVolumeBSphere::IntersectVisit(const CollisionVolumeBSphere & other_) const {
	return Katana_Math::TestIntersection(*this, other_);
}

bool CollisionVolumeBSphere::IntersectVisit(const CollisionVolumeAABB & other_) const {
	return Katana_Math::TestIntersection(*this, other_);
}

bool CollisionVolumeBSphere::IntersectVisit(const CollisionVolumeOBB & other_) const {
	return Katana_Math::TestIntersection(other_, *this);
}

const Vect & CollisionVolumeBSphere::GetCenter() const {
	return mCenter;
}

float CollisionVolumeBSphere::GetRadius() const {
	return mRadius;
}
