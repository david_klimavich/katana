#ifndef Terrain_
#define Terrain_

#include "Drawable.h"

class Model;
class Texture;
class GraphicsObject_TextureFlat;

struct TerrainSettings {
	const char* heightMapFile;
	int maxHeight;
	int lowestHeight;
	int sideLength;
	int uRep;
	int vRep;
};

class Terrain : public Drawable {
public:
	explicit Terrain(TerrainSettings& settings_);
	~Terrain();

private:
	void Draw() override;
	Model * mpTerrainModel;
	GraphicsObject_TextureFlat *mpGObj_Terrain;

	//VertexStride_VUN *pVerts;
	//TriangleIndex *pTriList;

	int mSideLength;
	int mMaxHeight;

	int VertexIndex(int i, int j);
	int PixelIndex(int i, int j);
	int TriIndex(int i, int j);
};

#endif Terrain_