#ifndef DrawRegistrationCommand_
#define DrawRegistrationCommand_

#include "CommandBase.h"
#include "Drawable.h"
#include "DebugOut.h"
#include "DrawableAttorney.h"

class DrawRegistrationCommand : public CommandBase {
public:
	explicit DrawRegistrationCommand(Drawable* drawable_)
		: mpDrawable(drawable_) {}

	void Execute() override {
		DrawableAttorney::Registration::SceneRegistration(*mpDrawable);
	};

private:
	Drawable * mpDrawable;
};

#endif // !DrawRegistrationCommand_
