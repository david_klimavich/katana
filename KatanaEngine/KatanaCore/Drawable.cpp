#include <assert.h>
#include "Drawable.h"
#include "SceneAttorney.h"
#include "DrawRegistrationCommand.h"
#include "DrawDeregistrationCommand.h"


Drawable::Drawable() {
	registrationCmd = new DrawRegistrationCommand(this);
	deregistrationCmd = new DrawDeregistrationCommand(this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
}

Drawable::Drawable(const Drawable &) {
	this->REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
	this->registrationCmd = new DrawRegistrationCommand(this);
	this->deregistrationCmd = new DrawDeregistrationCommand(this);
}

Drawable & Drawable::operator=(const Drawable &drawable_) {
	if(&drawable_ != this) {
		this->REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;

		delete this->registrationCmd;
		delete this->deregistrationCmd;
		this->registrationCmd = new DrawRegistrationCommand(this);
		this->deregistrationCmd = new DrawDeregistrationCommand(this);
	}
	return *this;
}

Drawable::~Drawable() {
	delete registrationCmd;
	delete deregistrationCmd;

	registrationCmd = nullptr;
	deregistrationCmd = nullptr;
}
KLI::DrawableListIt& Drawable::GetIt() {
	return it;
}
void Drawable::SetIt(KLI::DrawableListIt &it_) {
	it = it_;
}

void Drawable::SubmitRegister() {
	assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
	SceneAttorney::Registration::SubmitCommand(registrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
}

void Drawable::SubmitDeregister() {
	assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
	SceneAttorney::Registration::SubmitCommand(deregistrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
}

void Drawable::SceneRegistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
	SceneAttorney::Registration::Register(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
}

void Drawable::SceneDeregistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
	SceneAttorney::Registration::Deregister(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
}


