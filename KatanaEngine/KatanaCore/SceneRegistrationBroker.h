#ifndef SceneRegistrationBroker_
#define SceneRegistrationBroker_

#include <list>

class CommandBase;
class SceneRegistrationBroker {
public:

	SceneRegistrationBroker();
	~SceneRegistrationBroker();

	void AddCommand(CommandBase*);
	void ProcessCommands();


private:
	using CommandList = std::list<CommandBase*>;
	using CmdIterator = std::list<CommandBase*>::const_iterator;
	CommandList commandList;

};
#endif SceneRegistrationBroker_