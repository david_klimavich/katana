#ifndef ShaderManagerAttorney_
#define ShaderManagerAttorney_
#include "ShaderManager.h"

class ShaderManagerAttorney {
	friend class KatanaEngine;

private:
	static void CleanUp()
	{
		ShaderManager::CleanUp();
	}
};
#endif // !ShaderManagerAttorney_
