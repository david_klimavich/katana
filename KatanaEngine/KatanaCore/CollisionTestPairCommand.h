#ifndef CollisionTestPairCommand_
#define CollisionTestPairCommand_

#include "CollisionTestCommandBase.h"
#include "CollidableGroup.h"
#include "ContainerAlias.h"
#include "Visualizer.h"
#include "Katana_Math.h"
#include "../Resources/EngineSettings.h"

template <class CollidableType_1, class CollidableType_2>
class CollisionTestPairCommand : public CollisionTestCommandBase {
private:
	typename const CollidableGroup<CollidableType_1>::CollidableCollection& mCollidableCollection_1;
	typename const CollidableGroup<CollidableType_2>::CollidableCollection& mCollidableCollection_2;

public:
	CollisionTestPairCommand() :
		mCollidableCollection_1(CollidableGroup<CollidableType_1>::GetCollideableCollection()),
		mCollidableCollection_2(CollidableGroup<CollidableType_2>::GetCollideableCollection()) {}

	void Execute() override {

		for(auto it1 = mCollidableCollection_1.begin(); it1 != mCollidableCollection_1.end(); it1++) {
			for(auto it2 = mCollidableCollection_2.begin(); it2 != mCollidableCollection_2.end(); it2++) {

				bool collidable_group_result = Katana_Math::TestIntersection(CollidableGroup<CollidableType_1>::GetGroupAABB(), CollidableGroup<CollidableType_2>::GetGroupAABB());

				if(collidable_group_result) {

					// Test basic spheres first
					bool sphere_test_result = Katana_Math::TestIntersection((*it1)->GetDefaultVolume(), (*it2)->GetDefaultVolume());
					if(sphere_test_result) {
#if Visualizer_ON
						// Visualize Bsphere Collision 
						Visualizer::ShowBSphere((*it1)->GetDefaultVolume(), Color::DarkGray);
						Visualizer::ShowBSphere((*it2)->GetDefaultVolume(), Color::DarkGray);
#endif // Visualizer_ON
						// run mroe advanced AABB/OBB tests now
						bool test_result = Katana_Math::TestIntersection(*it1, *it2);
						if(test_result) {
#if Collision_ON
							(*it1)->Collision(**it2);
							(*it2)->Collision(**it1);
#endif // CollisionOn

#if Visualizer_ON
							Visualizer::ShowCollisionVolume((*it1), Color::Red);
							Visualizer::ShowCollisionVolume((*it2), Color::Red);
#endif  // VisualizerOn
						} else {

#if Visualizer_ON
							Visualizer::ShowCollisionVolume((*it1), Color::Blue);
							Visualizer::ShowCollisionVolume((*it2), Color::Blue);
#endif // VisualizerOn
						}
					} else {
#if Visualizer_ON
						// Visualize Bsphere Collision 
						Visualizer::ShowBSphere((*it1)->GetDefaultVolume(), Color::Green);
						Visualizer::ShowBSphere((*it2)->GetDefaultVolume(), Color::Green);
#endif // VisualizerOn
					}
				}

			}
		}
	};
};
#endif