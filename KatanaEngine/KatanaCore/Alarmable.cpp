#include "Alarmable.h"
#include <assert.h>
#include "SceneAttorney.h"
#include "AlarmRegistrationCommand.h"
#include "AlarmDeregistrationCommand.h"

Alarmable::Alarmable() {
	mAlarmArray[0].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
	mAlarmArray[1].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
	mAlarmArray[2].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;

	mAlarmArray[0].pRegistrationCmd =
		new AlarmRegistrationCommand(this, AlarmableManager::ALARM_0);
	mAlarmArray[1].pRegistrationCmd =
		new AlarmRegistrationCommand(this, AlarmableManager::ALARM_1);
	mAlarmArray[2].pRegistrationCmd =
		new AlarmRegistrationCommand(this, AlarmableManager::ALARM_2);

	mAlarmArray[0].pDeregistrationCmd =
		new AlarmDeregistrationCommand(this, AlarmableManager::ALARM_0);
	mAlarmArray[1].pDeregistrationCmd =
		new AlarmDeregistrationCommand(this, AlarmableManager::ALARM_1);
	mAlarmArray[2].pDeregistrationCmd =
		new AlarmDeregistrationCommand(this, AlarmableManager::ALARM_2);
}

Alarmable::~Alarmable() {
	for(int i = 0; i < AlarmableManager::ALARM_COUNT; i++) {
		delete mAlarmArray[i].pRegistrationCmd;
		delete mAlarmArray[i].pDeregistrationCmd;

		mAlarmArray[i].pRegistrationCmd = nullptr;
		mAlarmArray[i].pDeregistrationCmd = nullptr;
	}
}

void Alarmable::SetRef(AlarmableManager::TimeLineMap::iterator ref_, AlarmableManager::ALARM_ID alarmID_) {
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		mAlarmArray[0].pDeleteRef = ref_;
		break;
	case AlarmableManager::ALARM_1:
		mAlarmArray[1].pDeleteRef = ref_;
		break;
	case AlarmableManager::ALARM_2:
		mAlarmArray[2].pDeleteRef = ref_;
		break;
	}
}

AlarmableManager::TimeLineMap::iterator&  Alarmable::GetRef(AlarmableManager::ALARM_ID alarmID_) {
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		return mAlarmArray[0].pDeleteRef;
		break;
	case AlarmableManager::ALARM_1:
		return mAlarmArray[1].pDeleteRef;
		break;
	case AlarmableManager::ALARM_2:
		return mAlarmArray[2].pDeleteRef;
		break;
	}

	// TEMP
	return mAlarmArray[3].pDeleteRef;
}

const Alarmable::RegistrationData& Alarmable::GetAlarm0() {
	return mAlarmArray[0];
}

const Alarmable::RegistrationData & Alarmable::GetAlarm1() {
	return mAlarmArray[1];
}

const Alarmable::RegistrationData & Alarmable::GetAlarm2() {
	return mAlarmArray[2];
}

void Alarmable::SubmitRegister(float time_, AlarmableManager::ALARM_ID alarmID_) {
	RegistrationData* alarm;
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		alarm = &mAlarmArray[0];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
		alarm->pRegistrationCmd->SetTimer(time_);
		SceneAttorney::Registration::SubmitCommand(alarm->pRegistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
		break;
	case AlarmableManager::ALARM_1:
		alarm = &mAlarmArray[1];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
		alarm->pRegistrationCmd->SetTimer(time_);
		SceneAttorney::Registration::SubmitCommand(alarm->pRegistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
		break;
	case AlarmableManager::ALARM_2:
		alarm = &mAlarmArray[2];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
		alarm->pRegistrationCmd->SetTimer(time_);
		SceneAttorney::Registration::SubmitCommand(alarm->pRegistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
		break;
	}

}

void Alarmable::SubmitDeregister(AlarmableManager::ALARM_ID alarmID_) {
	RegistrationData* alarm;
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		alarm = &mAlarmArray[0];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
		SceneAttorney::Registration::SubmitCommand(alarm->pDeregistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
		break;
	case AlarmableManager::ALARM_1:
		alarm = &mAlarmArray[1];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
		SceneAttorney::Registration::SubmitCommand(alarm->pDeregistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
		break;
	case AlarmableManager::ALARM_2:
		alarm = &mAlarmArray[2];
		assert(alarm->REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
		SceneAttorney::Registration::SubmitCommand(alarm->pDeregistrationCmd);
		alarm->REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
		break;
	}

}

void Alarmable::SceneRegistration(float time_, AlarmableManager::ALARM_ID alarmID_) {
	RegistrationData* alarm;
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		alarm = &mAlarmArray[0];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
		SceneAttorney::Registration::Register(*this, time_, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
		break;
	case AlarmableManager::ALARM_1:
		alarm = &mAlarmArray[1];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
		SceneAttorney::Registration::Register(*this, time_, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
		break;
	case AlarmableManager::ALARM_2:
		alarm = &mAlarmArray[2];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
		SceneAttorney::Registration::Register(*this, time_, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
		break;
	}

}

void Alarmable::SceneDeregistration(AlarmableManager::ALARM_ID alarmID_) {
	RegistrationData* alarm;
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		alarm = &mAlarmArray[0];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
		SceneAttorney::Registration::Deregister(*this, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		break;
	case AlarmableManager::ALARM_1:
		alarm = &mAlarmArray[1];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
		SceneAttorney::Registration::Deregister(*this, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		break;
	case AlarmableManager::ALARM_2:
		alarm = &mAlarmArray[2];
		assert(alarm->REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
		SceneAttorney::Registration::Deregister(*this, alarmID_);
		alarm->REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		break;

	}

}


void Alarmable::TriggerAlarm(AlarmableManager::ALARM_ID alarmID_) {
	switch(alarmID_) {
	case AlarmableManager::ALARM_0:
		mAlarmArray[0].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		this->Alarm0();
		break;
	case AlarmableManager::ALARM_1:
		mAlarmArray[1].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		this->Alarm1();
		break;
	case AlarmableManager::ALARM_2:
		mAlarmArray[2].REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
		this->Alarm2();
		break;
	}
}
