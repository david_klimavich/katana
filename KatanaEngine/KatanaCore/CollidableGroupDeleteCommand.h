#ifndef CollidableGroupDelete_
#define CollidableGroupDelete_

#include "CommandBase.h"

template<class CollisionType>
class CollidableGroupDeleteCommand : public CommandBase {
public:
	void Execute() {
		CollidableGroup<CollisionType>::Delete();
	};

};


#endif // !CollidableGroupDelete_

