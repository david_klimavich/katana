#ifndef Sprite_
#define Sprite_

#include <string>
#include "GraphicsObject_Sprite.h"
#include "RegistrationState.h"
#include "ContainerAlias.h"
#include "Align16.h"


class SpriteRegistrationCommand;
class SpriteDeregistrationCommand;
class Sprite : public Align16 {
public:
	Sprite(std::string imageKey_);
	~Sprite();

	void Draw2D();

	void SetIt(KLI::SpriteListIt ref_);
	KLI::SpriteListIt GetItRef();

	void SetScale(const float x, const float y); 
	void SetPositionByFactor(const float FactorX, const float FactorY);

	void Sprite::SubmitRegister();
	void Sprite::SubmitDeregister();

	REGISTRATION_STATE GetRegState() {
		return REG_STATE;
	};

private:
	friend class SpriteAttorney;
	void Sprite::SceneRegistration();
	void Sprite::SceneDeregistration();

	Model * mpModelSprite;
	Texture* mpTexture; 
	Image* mpImage;
	Rect* mpRect; 
	ShaderObject* mpShaderObject_sprite;
	GraphicsObject_Sprite * mpGObj_sprite;
	REGISTRATION_STATE REG_STATE;

	SpriteRegistrationCommand *mpRegistrationCmd;
	SpriteDeregistrationCommand *mpDeregistrationCmd;

	KLI::SpriteListIt listRefToSelf;


	Matrix mPos;
	Matrix mWorld;
	Matrix mScale;
	Matrix mRotZ;
};
#endif // !Sprite_
