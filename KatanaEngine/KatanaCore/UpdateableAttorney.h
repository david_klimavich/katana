#ifndef UpdateableAttorney_
#define UpdateableAttorney_

#include "Updateable.h"

class UpdateableAttorney {
public:
	class GameLoop {
	private:
		friend class UpdateableManager;
		static void Update(Updateable& updateable_)
		{
			updateable_.Update();
		}
	};
	class Registration {
		friend class UpdateRegistrationCommand;
		friend class UpdateDeregistrationCommand;
	private:
		static void SceneRegistration(Updateable& updateable_)
		{
			updateable_.SceneRegistration();
		}
		static void SceneDeregistration(Updateable& updateable_)
		{
			updateable_.SceneDeregistration();
		}

	};
};
#endif // !UpdateableAttorney.h
