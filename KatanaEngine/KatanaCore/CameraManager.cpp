#include "CameraManager.h"
#include "KatanaEngine.h"
#include "GodCam.h"
#include "DebugOut.h"

CameraManager::CameraManager() {
	// Create Default Camera 
	mpDefaultCamera = new Camera(Camera::Type::PERSPECTIVE_3D);
	mpGodCamController = new GodCamController;

	// Set Defualt Orientation of Camera
	Vect up3DCam(0.0f, 1.0f, 0.0f);
	Vect pos3DCam(50.0f, 50.0f, 150.0f);
	Vect lookAt3DCam(0.0f, 0.0f, 0.0f);
	mpDefaultCamera->setOrientAndPosition(up3DCam, lookAt3DCam, pos3DCam);

	mpDefaultCamera->setViewport(0, 0, KatanaEngine::GetWindowWidth(), KatanaEngine::GetWindowHeight());
	mpDefaultCamera->setPerspective(35.0f, float(KatanaEngine::GetWindowWidth()) / float(KatanaEngine::GetWindowHeight()),
		1.0f, 5000.0f);
	mpDefaultCamera->updateCamera();  // Note: if the camera moves/changes, we need to call update again

									  // set active camera 
	mpActiveCamera = mpDefaultCamera;
	mpUserCam = mpActiveCamera;


	// 2D camera
	mpActive2DCamera = new Camera(Camera::Type::ORTHOGRAPHIC_2D);
	mpActive2DCamera->setViewport(0, 0, KatanaEngine::GetWindowWidth(), KatanaEngine::GetWindowHeight());
	mpActive2DCamera->setOrthographic(-0.5f*KatanaEngine::GetWindowWidth(), 0.5f*KatanaEngine::GetWindowWidth(),
		-0.5f*KatanaEngine::GetWindowHeight(), 0.5f*KatanaEngine::GetWindowHeight(),
		1.0f, 1000.0f);

	// Orient Camera
	Vect up2DCam(0.0f, 1.0f, 0.0f);
	Vect pos2DCam(0.0f, 0.0f, 0.0f);
	Vect lookAt2DCam(0.0f, 0.0f, -1.0f);
	mpActive2DCamera->setOrientAndPosition(up2DCam, lookAt2DCam, pos2DCam);

	mpActive2DCamera->updateCamera();


}

Camera* CameraManager::GetActiveCamera() {
	return mpActiveCamera;
}

Camera* CameraManager::GetActive2DCamera() {
	return mpActive2DCamera;
}

void CameraManager::SetActiveCamera(Camera& camera_) {
	mpActiveCamera = &camera_;
	mpUserCam = mpActiveCamera;
}

void CameraManager::ToggleGodCam() {
	if(mpUserCam == mpActiveCamera) {
		mpGodCamController->InitGodCam(*mpUserCam);
		mpGodCam = &mpGodCamController->GetCam();
		mpActiveCamera = mpGodCam;
		mpGodCamController->SubmitEntry();
	} else {
		mpActiveCamera = mpUserCam;
		mpGodCamController->SubmitExit();
	}

}

void CameraManager::RestoreDefualtCamera() {
	mpActiveCamera = mpDefaultCamera;
}

CameraManager::~CameraManager() {
	// do not delete active camera because it is created by the user. 
	delete mpDefaultCamera;
	delete mpActive2DCamera;
	mpActiveCamera = nullptr;
	mpActive2DCamera = nullptr;
}
