#include "UpdateableManager.h"
#include "Updateable.h"
#include "UpdateableAttorney.h"
#include "DebugOut.h"

// register updateable and return the iterator to be store inside the updateable
void UpdateableManager::Register(Updateable &updateable_) {
	KLI::UpdateableListIt it = updateableList.insert(updateableList.end(), &updateable_);
	updateable_.SetIt(it);
}

void UpdateableManager::Deregister(Updateable &updateable_) {
	updateableList.erase(updateable_.GetIt());
}

void UpdateableManager::ProcessElements() {
	for(auto it = updateableList.begin(); it != updateableList.end(); it++) {
		UpdateableAttorney::GameLoop::Update(**it);
	}
}

UpdateableManager::~UpdateableManager() {
	updateableList.clear();
}