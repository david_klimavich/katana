#ifndef DestroyObjectCommand_
#define DestroyObjectCommand_

#include "GameObject.h"
#include "CommandBase.h"

class DestroyObjectCommand : public CommandBase{
public:
	DestroyObjectCommand(GameObject& go_) : mpGameObject(&go_)
	{}
	
	inline void Execute() override { delete mpGameObject; }


private:
	GameObject* mpGameObject;
};

#endif