#include "SceneAttorney.h"
#include "Scene.h"
#include "NullScene.h"
#include "DestroySceneCommand.h"

SceneManager* SceneManager::instance = nullptr;

SceneManager::SceneManager() {
	mpActiveScene = new NullScene;
	mChangeScene = false;
}

void SceneManager::Update() {
	GetInstance().PrivateUpdate();
}

void SceneManager::Draw() {
	GetInstance().PrivateDraw();
}

void SceneManager::TriggerSceneChange(Scene *scene_) {
	GetInstance().PrivateTriggerSceneChange(scene_);
}

void SceneManager::PrivateSetActiveScene() {

	mpActiveScene->EndScene();
	delete mpActiveScene;
	mpActiveScene = nullptr;

	mpActiveScene = mpNextScene;
	SceneAttorney::Initialize::InitScene();
}

void SceneManager::PrivateTriggerSceneChange(Scene *scene_) {
	mpNextScene = scene_;
	mChangeScene = true;
}

Scene *SceneManager::GetActiveScene() {
	return GetInstance().PrivateGetActiveScene();
}

void SceneManager::PrivateUpdate() {
	if(mChangeScene) {
		PrivateSetActiveScene();
		mChangeScene = false;
	}
	SceneAttorney::GameLoop::Update(*mpActiveScene);
}

void SceneManager::PrivateDraw() {
	SceneAttorney::GameLoop::Draw(*mpActiveScene);
}

void SceneManager::CleanUp() {
	GetInstance().PrivateCleanUp();
}

void SceneManager::PrivateCleanUp() {
	mpActiveScene->EndScene();
	delete mpActiveScene;
	mpActiveScene = nullptr;
	GetInstance().Delete();
}

void SceneManager::Delete() {
	delete instance;
}

Scene *SceneManager::PrivateGetActiveScene() const {
	return mpActiveScene;
}

SceneManager::~SceneManager() {
	delete mpActiveScene;
	mpActiveScene = nullptr;
	instance = nullptr;
}