#ifndef CollisionManager_
#define CollisionManager_

#include "CollisionTestPairCommand.h"
#include "CollisionTestSelfCommand.h"

class CommandBase;
class CollisionManager {
public:
	~CollisionManager();
	void ProcessCollisions();

	template <typename ColliderType_1, typename ColliderType_2>
	void SetCollisionPair() {
		auto pairCmd = new CollisionTestPairCommand<ColliderType_1, ColliderType_2>();
		testCmdCollection.insert(testCmdCollection.begin(), pairCmd);
	};

	template <typename ColliderType>
	void SetCollisionSelf() {
		auto selfCmd = new CollisionTestSelfCommand<ColliderType>();
		testCmdCollection.insert(testCmdCollection.begin(), selfCmd);
	};

	void AddDeleteCommand(CommandBase*);
	void AddComputeCommand(CommandBase*);

private:
	KL::CollisionTestCmdList testCmdCollection;
	KL::GroupCollectionDelete mGroupCollectionDelete;
	KL::GroupCollectionCompute mGroupCollectionCompute;

};
#endif // !CollisionManager_