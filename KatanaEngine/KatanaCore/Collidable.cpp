#include "Collidable.h"
#include "CollisionVolumeBSphere.h"
#include "CollisionVolumeAABB.h"
#include "CollisionVolumeOBB.h"

Collidable::~Collidable() {
	delete mpRegCmd;
	delete mpDeregCmd;

	mpRegCmd = nullptr;
	mpDeregCmd = nullptr;

}

const CollisionVolume & Collidable::GetCollisionVolume() const {
	return *mpCollisionVolume;
}

const CollisionVolumeBSphere & Collidable::GetDefaultVolume() const {
	return mDefaultSphere;
}

void Collidable::SetColliderModel(Model * model_, CollisionVolumeType type_) {
	switch(type_) {
	case CollisionVolumeType::BSPHERE:
		mpCollisionVolume = new CollisionVolumeBSphere;
		break;
	case CollisionVolumeType::AABB:
		mpCollisionVolume = new CollisionVolumeAABB;
		break;
	case CollisionVolumeType::OBB:
		mpCollisionVolume = new CollisionVolumeOBB;
		break;
	}

	mpColModel = model_;
}

void Collidable::UpdateCollisionData(const Matrix & matrix_) {
	mpCollisionVolume->ComputeData(mpColModel, matrix_);
	mDefaultSphere.ComputeData(mpColModel, matrix_);
}
