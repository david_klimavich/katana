#include "Scene.h"
#include "Updateable.h"
#include "Drawable.h"
#include "SceneRegistrationBroker.h" 
#include "Inputable.h"
#include "DebugOut.h"
#include <assert.h>
#include "SceneAttorney.h"
#include "NullScene.h"
#include "DestroySceneCommand.h"


Scene::Scene() : mCameraManager(), mBroker(), mUpdateableManager(), mDrawableManager(), mDrawable2DManager(), mAlarmableManager(), mCollidableMangaer(), mKeyboardEventManager() {
	mpDestroySceneCmd = new DestroySceneCommand(*this);
}

Scene::~Scene() {
	delete mpDestroySceneCmd;
}

void Scene::Update() {
	mBroker.ProcessCommands();
	mAlarmableManager.ProcessAlarms();
	mCollidableMangaer.ProcessCollisions();
	mUpdateableManager.ProcessElements();
	mKeyboardEventManager.ProcessKeyEvents();
}

void Scene::Draw() {
	Visualizer::ProcessVisualizerBroker();
	mDrawableManager.ProcessElements();
	mDrawable2DManager.ProcessElements();
}



void Scene::SubmitCommand(CommandBase* cmd_) {
	mBroker.AddCommand(cmd_);
}

void Scene::Register(Updateable &updateable_) {
	mUpdateableManager.Register(updateable_);
}

void Scene::Register(Drawable& drawable_) {
	mDrawableManager.Register(drawable_);
}

void Scene::Register(Sprite & drawable2D_) {
	mDrawable2DManager.Register(drawable2D_);
}

void Scene::Register(Alarmable &alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_) {
	mAlarmableManager.Register(alarmable_, time_, alarmID_);
}

void Scene::Register(Inputable &inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_) {
	mKeyboardEventManager.Register(inputable_, key_, eventType_);
}

void Scene::Deregister(Updateable &updateable_) {
	mUpdateableManager.Deregister(updateable_);
}

void Scene::Deregister(Drawable &drawable_) {
	mDrawableManager.Deregister(drawable_);
}

void Scene::Deregister(Sprite & drawable2D_) {
	mDrawable2DManager.Deregister(drawable2D_);
}

void Scene::Deregister(Alarmable &alarmable_, AlarmableManager::ALARM_ID alarmID_) {
	mAlarmableManager.Deregister(alarmable_, alarmID_);
}

void Scene::Deregister(Inputable & inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_) {
	mKeyboardEventManager.Deregister(inputable_, key_, eventType_);
}

CameraManager& Scene::GetCameraManager() {
	return mCameraManager;
}

void Scene::AddDeleteCommand(CommandBase * cmd) {
	mCollidableMangaer.AddDeleteCommand(cmd);
}

void Scene::AddComputeCommand(CommandBase * cmd_) {
	mCollidableMangaer.AddComputeCommand(cmd_);
}

void Scene::EndScene() {
	CleanUp();
	mBroker.ProcessCommands();
}

void Scene::DeleteScene() {
	SubmitCommand(mpDestroySceneCmd);
}

