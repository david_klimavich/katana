#ifndef DrawableManager_
#define DrawableManager_
#include "ContainerAlias.h"

class DrawableManager {
public:
	DrawableManager() {};
	~DrawableManager();

	void Register(Drawable&);
	void Deregister(Drawable&);

	void ProcessElements();

private:

	KL::DrawableList drawableList;
};

#endif