#ifndef SceneManager_
#define SceneManager_


class Scene;
class DestroySceneCommand;
class SceneManager {
public:
	static Scene* GetActiveScene();
	static void TriggerSceneChange(Scene*);
private:
	SceneManager();
	SceneManager(const SceneManager&) = delete;
	SceneManager operator=(const SceneManager&) = delete;
	~SceneManager();

	static SceneManager* instance;
	static SceneManager& GetInstance() {
		if(!instance) {
			instance = new SceneManager();
		}
		return *instance;
	}

	friend class SceneManagerAttorney;
	static void Update();
	static void Draw();
	void PrivateUpdate();
	void PrivateDraw();
	void static CleanUp();
	void PrivateCleanUp();
	void Delete(); // delete this


	Scene* PrivateGetActiveScene() const;
	void PrivateSetActiveScene();
	void PrivateTriggerSceneChange(Scene*);

	// members
	Scene *mpActiveScene;
	Scene *mpNextScene;

	DestroySceneCommand* mpDestorySceneCmd;
	bool mChangeScene;

};
#endif // !SceneManager_

