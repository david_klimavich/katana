#ifndef ViualizerBroker_
#define ViualizerBroker_
#include <list>

class CommandBase; 
class ViualizerBroker {
public:

	ViualizerBroker() {};
	~ViualizerBroker() {};

	void AddCommand(CommandBase*);
	void ProcessElements();

private:
	using CommandList = std::list<CommandBase*>;
	using CmdIterator = std::list<CommandBase*>::const_iterator;
	CommandList commandList;
};
#endif // !VisualizerBroker_
