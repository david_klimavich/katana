#ifndef SceneRegistrationCommand_
#define SceneRegistrationCommand_

#include "CommandBase.h"

class SceneRegistrationCommand : public CommandBase {
public:
	virtual void Execute() override = 0;
};

#endif