#include "InputableRegistrationCommand.h"
#include "InputableAttorney.h"

InputableRegistrationCommand::InputableRegistrationCommand(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
	: mpInputable(&inputable_), mKey(key_), mEventType(eventType_)
{}

InputableRegistrationCommand::~InputableRegistrationCommand()
{}

void InputableRegistrationCommand::Execute()
{
	InputableAttorney::Registration::SceneRegistration(*mpInputable, mKey, mEventType);
	delete this;
}
