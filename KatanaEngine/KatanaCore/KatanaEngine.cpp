#include "AzulCore.h"
#include "KatanaEngine.h"
#include "ModelManagerAttorney.h"
#include "ShaderManagerAttorney.h"
#include "TextureManagerAttorney.h"
#include "SceneManagerAttorney.h"
#include "Visualizer.h"
#include "TimeManager.h"


KatanaEngine* KatanaEngine::instance = nullptr;

//-----------------------------------------------------------------------------
// Game::Initialize()
//		Allows the engine to perform any initialization it needs to before 
//      starting to run.  This is where it can query for any required services 
//      and load any non-graphic related content. 
//-----------------------------------------------------------------------------

void KatanaEngine::Initialize() {
	InitGameWindow();
}

//-----------------------------------------------------------------------------
// Game::LoadContent()
//		Allows you to load all content needed for your engine,
//	    such as objects, graphics, etc.
//-----------------------------------------------------------------------------
void KatanaEngine::LoadContent() {
	LoadAllResources();
	SetStartingScene();
}

//-----------------------------------------------------------------------------
// Game::Update()
//      Called once per frame, update data, tranformations, etc
//      Use this function to control process order
//      Input, AI, Physics, Animation, and Graphics
//-----------------------------------------------------------------------------
void KatanaEngine::Update() {
	TimeManager::ProcessTime();
	SceneManagerAttorney::GameLoop::Update();
}

//-----------------------------------------------------------------------------
// Game::Draw()
//		This function is called once per frame
//	    Use this for draw graphics to the screen.
//      Only do rendering here
//-----------------------------------------------------------------------------
void KatanaEngine::Draw() {
	SceneManagerAttorney::GameLoop::Draw();
}

void KatanaEngine::run() {
	GetInstance().privateRun();
}

void KatanaEngine::EndGame()
{
	GetInstance().PrivateEndGame(); 
}

int KatanaEngine::GetWindowHeight() {
	return GetInstance().PrivateGetHeight();
}

int KatanaEngine::GetWindowWidth() {
	return GetInstance().PrivateGetWidth();
}

float KatanaEngine::GetTimeInSeconds() {
	return GetInstance().PrivateGetTimeInSeconds();
}

void KatanaEngine::privateRun() {
	this->Engine::run();
}

void KatanaEngine::PrivateEndGame()
{

}

float KatanaEngine::PrivateGetTimeInSeconds() {
	return Engine::GetTimeInSeconds();
}

int KatanaEngine::PrivateGetHeight() {
	return this->getHeight();
}

int KatanaEngine::PrivateGetWidth() {
	return this->getWidth();
}


//-----------------------------------------------------------------------------
// Game::UnLoadContent()
//       unload content (resources loaded above)
//       unload all content that was loaded before the Engine Loop started
//-----------------------------------------------------------------------------
void KatanaEngine::UnLoadContent() {
	// general clean up.
	ModelManagerAttorney::CleanUp();
	ShaderManagerAttorney::CleanUp();
	TextureManagerAttorney::CleanUp();
	SceneManagerAttorney::GameLoop::CleanUp();

#if Visualizer_ON
	Visualizer::Delete();
#endif // Visualizer_ON


}

