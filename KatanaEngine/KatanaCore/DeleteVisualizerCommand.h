#ifndef DeleteVisualizerCommand_
#define DeleteVisualizerCommand_

#include "CommandBase.h"
#include "Visualizer.h"

class DeleteVisualizerCommand : public CommandBase {
public:
	DeleteVisualizerCommand() = default;
	~DeleteVisualizerCommand() = default;

	void Execute() final {
		Visualizer::Delete();
	};

private:

};
#endif // !DeleteVisualizerCommand_
