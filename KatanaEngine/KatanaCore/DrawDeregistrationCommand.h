#ifndef DrawDeregistrationCommand_
#define DrawDeregistrationCommand_

#include "CommandBase.h"
#include "Drawable.h"
#include "DebugOut.h"
#include "DrawableAttorney.h"

class DrawDeregistrationCommand : public CommandBase {
public:
	explicit DrawDeregistrationCommand(Drawable* drawable_)
		: drawable(drawable_) {}

	void Execute() override {
		DrawableAttorney::Registration::SceneDeregistration(*drawable);
	};
private:
	Drawable* drawable;
};

#endif // !DrawDeregistrationCommand_
