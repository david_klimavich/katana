#ifndef CollisionTestSelfCommand_
#define CollisionTestSelfCommand_ 

#include "SceneRegistrationCommand.h"
#include "CollisionTestCommandBase.h"
#include "CollidableGroup.h"
#include "ContainerAlias.h"
#include "DebugOut.h"
#include "Color.h"
#include "../Resources/EngineSettings.h"

template <class CollidableType>
class CollisionTestSelfCommand : public CollisionTestCommandBase {
private:
	typename const CollidableGroup<CollidableType>::CollidableCollection& mCollidableCollection;

public:

	CollisionTestSelfCommand() : mCollidableCollection(CollidableGroup<CollidableType>::GetCollideableCollection()) {}
	~CollisionTestSelfCommand() {
		//DebugMsg::out("CollisionTestCommandBase Destructor\n");
		//mCollidableCollection.clear();
	}

	void Execute() override {

		for(auto it1 = mCollidableCollection.begin(); it1 != mCollidableCollection.end(); it1++) {
			auto it2 = it1;
			for(it2++; it2 != mCollidableCollection.end(); it2++) {

				// Test basic spheres first
				bool test_result = Katana_Math::TestIntersection((*it1)->GetDefaultVolume(), (*it2)->GetDefaultVolume());

				if(test_result) {
#if Visualizer_ON
					// Visualize Bsphere Collision 
					Visualizer::ShowBSphere((*it1)->GetDefaultVolume(), Color::DarkGray);
					Visualizer::ShowBSphere((*it2)->GetDefaultVolume(), Color::DarkGray);
#endif // Collision_ON

					test_result = Katana_Math::TestIntersection((*it1), *it2);
					if(test_result) {
#if Collision_ON
						(*it1)->Collision(**it2);
						(*it2)->Collision(**it1);
#endif // Collision_ON
#if Visualizer_ON
						Visualizer::ShowCollisionVolume((*it1), Color::Red);
						Visualizer::ShowCollisionVolume((*it2), Color::Red);
#endif // Visualizer_ON
					} else {
#if Visualizer_ON
						Visualizer::ShowCollisionVolume((*it1), Color::Blue);
						Visualizer::ShowCollisionVolume((*it2), Color::Blue);
#endif // Visualizer_ON
					}
				} else {
#if Visualizer_ON
					// Visualize Bsphere Collision 
					Visualizer::ShowBSphere((*it1)->GetDefaultVolume(), Color::Green);
					Visualizer::ShowBSphere((*it2)->GetDefaultVolume(), Color::Green);
#endif // VisualizerOn
				}

			}
		}
	};


};
#endif // !CollisionTestSelfCommand_
