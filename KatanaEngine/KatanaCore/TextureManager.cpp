#include "TextureManager.h"
#include "DebugOut.h"
#include <string.h>

TextureManager* TextureManager::instance = nullptr;
TextureManager::TextureManager() {}

// interface
void TextureManager::LoadTexture(std::string key_, std::string fileName_) {
	GetInstance().PrivateLoadTexture(key_, fileName_);
}

Texture* TextureManager::GetTexture(std::string key_) {
	return GetInstance().PrivateGetTexture(key_);
}

void TextureManager::PrivateLoadTexture(std::string key_, std::string fileName_) {
	if(!TextureInMap(key_)) {
		// Default File Path
		std::string relativePath = rootPath + fileName_;

		// Load specified Texture
		Texture *pTexture = new Texture(relativePath.c_str());

		// insert into the TextureMap
		textureMap.insert({ key_, pTexture });
	}
}

Texture* TextureManager::PrivateGetTexture(std::string key_) {
	auto search = textureMap.find(key_);
	assert(search != textureMap.end());
	return search->second;
}

bool TextureManager::TextureInMap(std::string key_) {
	auto search = textureMap.find(key_);

	if(search == textureMap.end()) {
		return false;
	} else {
		return true;
	}
}

void TextureManager::CleanUp() {
	GetInstance().PrivateCleanUp();
}

void TextureManager::PrivateCleanUp() {
	for(auto it = textureMap.begin(); it != textureMap.end(); it++) {
		delete it->second;
	}
	textureMap.clear();
}

TextureManager::~TextureManager() {

}