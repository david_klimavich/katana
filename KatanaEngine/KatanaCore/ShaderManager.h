#ifndef ShaderManager_
#define ShaderManager_

#include <map>
#include <string>
#include "ShaderObject.h"
#include "KatanaEngine.h"

class ShaderManagerAttorney;
class ShaderManager {
public:
	static void LoadShader(std::string key_, std::string fileName_);
	static ShaderObject* GetShader(std::string key_);

private:
	ShaderManager() = default;
	ShaderManager(const ShaderManager&) = delete;
	ShaderManager operator=(const ShaderManager&) = delete;
	~ShaderManager() {
		delete instance;
	};

	friend class ShaderManagerAttorney;
	static void CleanUp();
	void PrivateCleanUp();

	static ShaderManager* instance;
	static ShaderManager& GetInstance() {
		if(!instance) {
			instance = new ShaderManager();
		}
		return *instance;
	}

	// Private Methods
	void PrivateLoadShader(std::string key_, std::string fileName_);

	ShaderObject* PrivateGetShader(std::string key_);
	bool ShaderInMap(std::string relativeName_);

	std::map<std::string, ShaderObject*> shaderMap;
	std::string rootPath = "Shaders/";

};



#endif // !ShaderManager_

