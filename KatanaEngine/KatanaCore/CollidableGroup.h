#ifndef CollidableGroup_
#define CollidableGroup_

#include "ContainerAlias.h"
//#include "Collidable.h"
#include "CollidableGroupDeleteCommand.h"
#include "CollidableGroupComputeCommand.h"
#include "CollisionVolumeAABB.h"

template<class CollidableType>
class CollidableGroup : public Align16 {
	friend class CollidableGroupDeleteCommand<CollidableType>;
	friend class CollidableGroupComputeCommand<CollidableType>;

public:

	static const std::list<CollidableType*>& GetCollideableCollection() {
		return GetInstance().PrivateGetColliableCollection();
	};

	static void Register(CollidableType& collidable_) {
		GetInstance().PrivateRegister(collidable_);
	};
	static void Deregister(CollidableType& collidable_) {
		GetInstance().PrivateDeregister(collidable_);
	};

	using CollidableCollection = std::list<CollidableType*>;

	static const CollisionVolumeAABB& GetGroupAABB() {
		return GetInstance().mVolumeAABB;
	}

private:
	CollidableGroup() = default;
	CollidableGroup<CollidableType>(const CollidableGroup<CollidableType>&) = delete;
	CollidableGroup<CollidableType>& operator=(const CollidableGroup<CollidableType>&) = delete;
	~CollidableGroup() {
		//mCollidableCollection.clear();
		instance = nullptr;
	};

	static void Delete() {
		delete instance;
	}

	static CollidableGroup<CollidableType>* instance;
	static CollidableGroup<CollidableType>& GetInstance() {
		if(!instance) {
			instance = new CollidableGroup<CollidableType>;
			auto *delCmd = new CollidableGroupDeleteCommand<CollidableType>();
			auto *computeCmd = new CollidableGroupComputeCommand<CollidableType>();
			SceneManager::GetActiveScene()->AddDeleteCommand(delCmd);
			SceneManager::GetActiveScene()->AddComputeCommand(computeCmd);
		}
		return *instance;
	}

	CollidableCollection mCollidableCollection;

	void PrivateDeregister(CollidableType& collidable_) {
		mCollidableCollection.remove(&collidable_);
	};
	void PrivateRegister(CollidableType& collideable_) {
		mCollidableCollection.insert(mCollidableCollection.end(), &collideable_);
	};

	const std::list<CollidableType*>& PrivateGetColliableCollection() {
		return mCollidableCollection;
	};

	//friend class CollidableGroupComputeCommand;
	void ComputeAABB() {
		mVolumeAABB.ResetAABB();
		for(auto it = mCollidableCollection.begin(); it != mCollidableCollection.end(); it++) {
			mVolumeAABB.ComputeData((*it)->GetDefaultVolume());
		}
	};

	// members
	CollisionVolumeAABB mVolumeAABB;
};

template <typename CollidableType>
CollidableGroup<CollidableType>* CollidableGroup<CollidableType>::instance = nullptr;


#endif //!CollidableGroup_
