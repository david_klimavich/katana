#include "AlarmableManager.h"
#include "AlarmableAttorney.h"
#include "KatanaEngine.h"
#include "TimeManager.h"

AlarmableManager::AlarmableManager() {}

AlarmableManager::~AlarmableManager() {}

void AlarmableManager::Register(Alarmable &alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_) {
	AlarmableAttorney::Manager::SetRef(alarmable_, (alarmableMap.insert({ (TimeManager::GetTimeInSeconds() + time_),{ &alarmable_ , alarmID_ } })), alarmID_);
}
void AlarmableManager::Deregister(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_) {
	TimeLineMap::iterator it = AlarmableAttorney::Manager::GetRef(alarmable_, alarmID_);
	alarmableMap.erase(it);
}

void AlarmableManager::ProcessAlarms() {
	TimeLineMap::iterator it = alarmableMap.begin();
	while(it != alarmableMap.end()) {
		if(TimeManager::GetTimeInSeconds() > it->first) {
			// trigger alarm
			it->second.first->TriggerAlarm(it->second.second);
			// move iterator forward
			it++;
			// remove the triggered alarm from the map
			alarmableMap.erase(alarmableMap.begin());
		} else {
			break;
		}
	}
}


