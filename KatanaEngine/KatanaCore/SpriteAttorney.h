#ifndef SpriteAttorney_
#define SpriteAttorney_

#include "Sprite.h"

class Sprite2DManager;
class Drawable2DRegistrationCommand;
class Drawable2D2DDeregistrationCommand;

class SpriteAttorney {
public:
	class GameLoop {
	private:
		friend class Sprite2DManager;
		static void Draw2D(Sprite& sprite_) {
			sprite_.Draw2D();
		}
	};

	class Registration {
		friend class SpriteRegistrationCommand;
		friend class SpriteDeregistrationCommand;
		friend class Sprite;
	private:
		static void SceneRegistration(Sprite& sprite_) {
			sprite_.SceneRegistration();
		}
		static void SceneDeregistration(Sprite& sprite_) {
			sprite_.SceneDeregistration();
		}

	};
};
#endif // !Drawable2DAttorney.h
