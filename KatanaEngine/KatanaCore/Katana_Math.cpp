#include "Katana_Math.h"
#include "Collidable.h"
#include "CollisionVolumeBSphere.h"
#include "CollisionVolumeAABB.h"
#include "CollisionVolumeOBB.h"

bool Katana_Math::TestIntersection(const CollisionVolumeBSphere & A, const CollisionVolumeBSphere & B) {
	float totalRadius = A.GetRadius() + B.GetRadius();
	float distance = GetDistanceApart(A, B);

	if(totalRadius * totalRadius > distance) {
		return true;
	}

	return false;
}

bool Katana_Math::TestIntersection(const CollisionVolumeBSphere & sphere_, const CollisionVolumeAABB & box_) {

	// step 1: clamp the center point of the bsphere to the AABB box
	Vect centerClamped = sphere_.GetCenter();

	// Clamp X
	if(centerClamped.X() < box_.getMin().X()) {
		centerClamped.X() = box_.getMin().X();
	} else if(centerClamped.X() > box_.getMax().X()) {
		centerClamped.X() = box_.getMax().X();
	}

	// Clamp Y
	if(centerClamped.Y() < box_.getMin().Y()) {
		centerClamped.Y() = box_.getMin().Y();
	} else if(centerClamped.Y() > box_.getMax().Y()) {
		centerClamped.Y() = box_.getMax().Y();
	}

	// Clamp Z
	if(centerClamped.Z() < box_.getMin().Z()) {
		centerClamped.Z() = box_.getMin().Z();
	} else if(centerClamped.Z() > box_.getMax().Z()) {
		centerClamped.Z() = box_.getMax().Z();
	}

	// Step 2: Test clamped point for intersection with the Bsphere.
	float distanceSquared = (sphere_.GetCenter() - centerClamped).magSqr();
	float radiusSquared = sphere_.GetRadius() * sphere_.GetRadius();

	if(distanceSquared < radiusSquared) {
		return true;
	}
	return false;
}

bool Katana_Math::TestIntersection(const CollisionVolumeAABB & A, const CollisionVolumeAABB & B) {
	if(A.getMin().X() <= B.getMax().X() && A.getMax().X() >= B.getMin().X() &&
		A.getMin().Y() <= B.getMax().Y() && A.getMax().Y() >= B.getMin().Y() &&
		A.getMin().Z() <= B.getMax().Z() && A.getMax().Z() >= B.getMin().Z()) {
		return true;
	}
	return false;
}

bool Katana_Math::TestIntersection(const CollisionVolumeOBB & OBB_, const CollisionVolumeBSphere & BSphere_) {
	// compute world inverse 
	const Matrix& world = OBB_.getWorld();
	const Matrix& worldInv = world.getInv();

	// compute C' = (world inverse) * (center)
	Vect C_prime = BSphere_.GetCenter() * worldInv;

	// compute C_Clamped 
	Vect C_Clamped = C_prime;

	Katana_Math::ClampVector(C_Clamped, OBB_.getMin(), OBB_.getMax());

	// compute C_Clamped' = (C_Clamped * world)
	Vect C_Clamped_prime = C_Clamped * world;

	// Test if C_Clamped' is inside BSphere 'other_'
	float distSquared = (BSphere_.GetCenter() - C_Clamped_prime).magSqr();

	if(distSquared < (BSphere_.GetRadius() * BSphere_.GetRadius())) {
		return true;
	} else {
		return false;
	}

}

bool Katana_Math::TestIntersection(const CollisionVolumeBB & A, const CollisionVolumeBB & B) {
	const int arraySize = 15;
	Vect vectArray[arraySize];
	// 3 face normals for A
	vectArray[0] = A.getWorld().get(ROW_0); // ax
	vectArray[1] = A.getWorld().get(ROW_1); // ay
	vectArray[2] = A.getWorld().get(ROW_2); // az

	// 3 face normals for B	
	vectArray[3] = B.getWorld().get(ROW_0); // bx
	vectArray[4] = B.getWorld().get(ROW_1);	// by
	vectArray[5] = B.getWorld().get(ROW_2);	// bz

	// 3X3 cross products of the 6 (x,y,z on each OBB) axes
	vectArray[6] = vectArray[0].cross(vectArray[3]);
	vectArray[7] = vectArray[0].cross(vectArray[4]);
	vectArray[8] = vectArray[0].cross(vectArray[5]);

	vectArray[9] = vectArray[1].cross(vectArray[3]);
	vectArray[10] = vectArray[1].cross(vectArray[4]);
	vectArray[11] = vectArray[1].cross(vectArray[5]);

	vectArray[12] = vectArray[2].cross(vectArray[3]);
	vectArray[13] = vectArray[2].cross(vectArray[4]);
	vectArray[14] = vectArray[2].cross(vectArray[5]);

	// If there is overlap on ALL axes, then we have intersection
	for(int i = 0; i < arraySize; i++) {
		if(vectArray[i].magSqr() > FLT_EPSILON) {
			if(!TestOverlap(vectArray[i], A, B)) {
				return false;
			}
		}
	}
	return true;
}

bool Katana_Math::TestIntersection(Collidable * collidable_1_, Collidable * collidable_2_) {
	const CollisionVolume& volume_1 = collidable_1_->GetCollisionVolume();
	const CollisionVolume& volume_2 = collidable_2_->GetCollisionVolume();

	return volume_1.IntersectAccept(volume_2);
}

float Katana_Math::GetDistanceSquared(const Vect & vectA_, const Vect & vectB_)
{
	return ((vectB_.X() - vectA_.X()) * (vectB_.X() - vectA_.X())
			+(vectB_.Y() - vectA_.Y()) * (vectB_.Y() - vectA_.Y())
			+(vectB_.Z() - vectA_.Z()) * (vectB_.Z() - vectA_.Z()));
}

float Katana_Math::Max(float a, float b) {
	if(a > b) {
		return a;
	} else {
		return b;
	}
}

float Katana_Math::Min(float a, float b) {
	if(a < b) {
		return a;
	} else {
		return b;
	}
}

void Katana_Math::ClampVector(Vect & input, const Vect & min, const Vect & max) {

	// clamp X
	if(input.X() < min.X()) {
		input.X() = min.X();
	} else if(input.X() > max.X()) {
		input.X() = max.X();
	}

	// clamp y
	if(input.Y() < min.Y()) {
		input.Y() = min.Y();
	} else if(input.Y() > max.Y()) {
		input.Y() = max.Y();
	}

	// clamp z
	if(input.Z() < min.Z()) {
		input.Z() = min.Z();
	} else if(input.Z() > max.Z()) {
		input.Z() = max.Z();
	}
}

float Katana_Math::MaxProjection(const Vect & v, const CollisionVolumeBB & BB, const Matrix& matrix_) {
	// v is a pure vector
	// Move v to local space: v' = v * worldInverse
	Vect v_prime = v * matrix_; //v * BB.getWorld().getInv();

	// Compute the max projection: ProjMax = (|v' * a.x| + |v' * b.y|) / ||v||
	float projMax = fabsf(v_prime.X() * BB.getHalfDiagonal().X()) +
		fabsf(v_prime.Y() * BB.getHalfDiagonal().Y()) +
		fabsf(v_prime.Z() * BB.getHalfDiagonal().Z());

	projMax /= v.mag();

	// Return (s^2) * ProjMax;

	return BB.getScaleSquared() * projMax;
}

bool Katana_Math::TestOverlap(const Vect& v, const CollisionVolumeBB & A, const CollisionVolumeBB & B) {
	// Return D <= proj_A + proj_B
	//if(v.magSqr() > FLT_EPSILON)
	//{
		// Compute D = |(C2 - C1) dot v)| / ||v||
	float dist = fabsf(((B.getCenter() - A.getCenter()).dot(v)) / v.mag());

	// Compute Proj_A = Max OBB1 projection along v
	float proj_A = MaxProjection(v, A, A.getInv());

	// Compute Proj_B = Max OBB2 projection along v
	float proj_B = MaxProjection(v, B, B.getInv());

	return (dist <= (proj_A + proj_B));
}

float Katana_Math::GetDistanceApart(const CollisionVolumeBSphere & A, const CollisionVolumeBSphere & B) {
	float distance = (A.GetCenter() - B.GetCenter()).magSqr();
	return distance;
}

float Katana_Math::GetCollisionRadius(const CollisionVolumeBSphere & A, const CollisionVolumeBSphere & B) {
	float radius = A.GetRadius() + B.GetRadius();
	return radius;
}
