#include "ModelManager.h"
#include "DebugOut.h"
#include <string.h>

// statics
ModelManager* ModelManager::instance = nullptr;
std::string ModelManager::UnitBox_WF = "UnitBox_WF";
std::string ModelManager::UnitSphere = "UnitSphere";
std::string ModelManager::UnitSquareXY = "UnitSquareXY";

ModelManager::ModelManager() {
	LoadDefault();
}

void ModelManager::LoadDefault() {
	// Load specified Model
	Model *pUnitBox_WF = new Model(Model::PreMadedeModels::UnitBox_WF);
	Model *pUnitSphere = new Model(Model::PreMadedeModels::UnitSphere);
	Model *pUnitSquareXY = new Model(Model::PreMadedeModels::UnitSquareXY);

	// insert into the ModelMap
	modelMap.insert({ UnitBox_WF, pUnitBox_WF });
	modelMap.insert({ UnitSphere, pUnitSphere });
	modelMap.insert({ UnitSquareXY, pUnitSquareXY });
}

// interface
void ModelManager::LoadModel(std::string key_, std::string fileName_) {
	GetInstance().PrivateLoadModel(key_, fileName_);
}

Model* ModelManager::GetModel(std::string key_) {
	return GetInstance().PrivateGetModel(key_);
}

void ModelManager::PrivateLoadModel(std::string key_, std::string fileName_) {
	if(!ModelInMap(key_)) {

		// Default File Path
		std::string relativePath = rootPath + fileName_;

		// Load specified Model
		Model *pModel = new Model(relativePath.c_str());

		// insert into the ModelMap
		modelMap.insert({ key_, pModel });
	}
}

Model* ModelManager::PrivateGetModel(std::string key_) {
	auto search = modelMap.find(key_);
	return search->second;
}

bool ModelManager::ModelInMap(std::string key_) {
	auto search = modelMap.find(key_);

	if(search == modelMap.end()) {
		return false;
	} else {
		return true;
	}
}

void ModelManager::CleanUp() {
	GetInstance().PrivateCleanUp();
}

void ModelManager::PrivateCleanUp() {
	for(auto it = modelMap.begin(); it != modelMap.end(); it++) {
		delete it->second;
	}
	modelMap.clear();
}