#ifndef ContainerAlias_
#define ContainerAlias_

#include <list>
#include <map>
#include <string>
#include "Keyboard.h"

class Updateable;
class Drawable;
class Sprite;
class Alarmable;
class Inputable;
class Collidable;
class CollisionTestCommandBase;
class SingleKeyEventManager;
class Model;
class Shader;
class Texture;
class Image;
class CommandBase;

// Katana Lists
namespace KL {
	using UpdateableList = std::list<Updateable*>;
	using DrawableList = std::list<Drawable*>;
	using SpriteList = std::list<Sprite*>;
	using AlarmableList = std::list<Alarmable*>;
	using InputableList = std::list<Inputable*>;
	using CollidableList = std::list<Collidable*>;
	using CollisionTestCmdList = std::list <CollisionTestCommandBase*>;
	using GroupCollectionDelete = std::list<CommandBase*>;
	using GroupCollectionCompute = std::list<CommandBase*>;
}

// Katana List Iterators
namespace KLI {
	using UpdateableListIt = std::list<Updateable*>::iterator;
	using DrawableListIt = std::list<Drawable*>::iterator;
	using SpriteListIt = std::list<Sprite*>::iterator;
	using AlarmableListIt = std::list<Alarmable*>::iterator;
}

// Katana Maps
namespace KM {
	using ModelMap = std::map<std::string, Model*>;
	using TextureMap = std::map<std::string, Texture*>;
	using ShaderMap = std::map<std::string, Shader*>;
	using ImageMap = std::map<std::string, Image*>;

	using AlarmMultiMap = std::multimap<float, Alarmable*>;
	using SingleKeyMap = std::map<AZUL_KEY, SingleKeyEventManager*>;
}

// Katana Map Iterators
namespace KMI {
	using AlarmMultiMapIt = std::multimap<float, Alarmable*>::iterator;
}


#endif // !ContainerAlias_