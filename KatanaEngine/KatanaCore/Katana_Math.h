#ifndef Katana_Math_
#define Katana_Math_

#include "Vect.h"


class CollisionVolumeBSphere;
class CollisionVolumeAABB;
class CollisionVolumeOBB;
class CollisionVolumeBB;
class Collidable;

namespace Katana_Math {

	// Vector Math
	float GetDistanceSquared(const Vect& vectA_, const Vect& vectB_); 

	float Max(float a, float b);
	float Min(float a, float b);

	void ClampVector(Vect& input, const Vect& min, const Vect& max);
	float MaxProjection(const Vect& v, const CollisionVolumeBB& OBB, const Matrix& matrix_);
	bool TestOverlap(const Vect& v, const CollisionVolumeBB& A, const CollisionVolumeBB& B);

	float GetDistanceApart(const CollisionVolumeBSphere& A, const CollisionVolumeBSphere & B);
	float GetCollisionRadius(const CollisionVolumeBSphere & A, const CollisionVolumeBSphere & B);

	// BSphere Tests
	bool TestIntersection(const CollisionVolumeBSphere & A, const CollisionVolumeBSphere & B);
	bool TestIntersection(const CollisionVolumeBSphere & A, const CollisionVolumeAABB & B);

	// ABB Tests 
	bool TestIntersection(const CollisionVolumeAABB & A, const CollisionVolumeAABB & B);

	// OBB Tests
	bool TestIntersection(const CollisionVolumeOBB & OBB_, const CollisionVolumeBSphere & BSphere_);
	bool TestIntersection(const CollisionVolumeBB & OBB_, const CollisionVolumeBB& otherOBB_);
	bool TestIntersection(Collidable * A, Collidable * B);
};
#endif