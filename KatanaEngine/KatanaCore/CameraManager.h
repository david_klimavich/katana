#ifndef CameraManager_
#define CameraManager_

#include "Camera.h"
#include "Align16.h"

class GodCamController;
class CameraManager : public Align16 {
public:
	CameraManager();
	~CameraManager();


	// Interface
	Camera* GetActiveCamera();
	Camera* GetActive2DCamera();

	void SetActiveCamera(Camera&);

	void ToggleGodCam();
	void RestoreDefualtCamera();


private:

	Camera* mpDefaultCamera;
	Camera* mpActiveCamera;
	Camera* mpUserCam;
	Camera* mpGodCam;

	Camera* mpActive2DCamera;

	GodCamController* mpGodCamController;
};
#endif