#ifndef SpriteDeRegistrationCommand_
#define SpriteDeRegistrationCommand_

#include "CommandBase.h"
#include "Sprite.h"
#include "DebugOut.h"
#include "SpriteAttorney.h"

class SpriteRegistrationCommand : public CommandBase {
public:
	explicit SpriteRegistrationCommand(Sprite* drawable2D_) :
		mpSprite(drawable2D_) {};
	~SpriteRegistrationCommand() = default;

	void Execute() override {
		SpriteAttorney::Registration::SceneRegistration(*mpSprite);
	}

private:
	Sprite * mpSprite;
};
#endif // !SpriteDeRegistrationCommand_
