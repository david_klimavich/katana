#ifndef SceneManagerAttorney_
#define SceneManagerAttorney_

#include "SceneManager.h"
#include "Scene.h"

class Drawable;
class Updateable;
class SceneManagerAttorney {
public:

	class GameLoop {
	private:
		friend class KatanaEngine;
		static void Update() {
			SceneManager::Update();
		}
		static void Draw() {
			SceneManager::Draw();
		}
		static void CleanUp() {
			SceneManager::CleanUp();
		}
	};

};
#endif // !SceneManagerAttorney_
