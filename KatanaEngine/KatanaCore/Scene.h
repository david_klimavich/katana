#ifndef Scene_
#define Scene_

#include <list>
#include "CameraManager.h"
#include "SceneRegistrationBroker.h"
#include "UpdateableManager.h"
#include "DrawableManager.h"
#include "SpriteManager.h"
#include "AlarmableManager.h"
#include "CollisionManager.h"
#include "RegistrationState.h"
#include "InputEventType.h"
#include "KeyboardEventManager.h"
#include "Keyboard.h"
#include "VisualizerBroker.h"

class Drawable;
class Sprite;
class Updateable;
class Alarmable;
class Inputable;
class CommandBase;
class DestroySceneCommand;
class Scene {
	friend class SceneAttorney;
public:
	Scene();
	// copying scenes should be allowed but it is not for now until I write all of the other
	// copy constructors / assignemnet operator overlaods to make it safe.
	Scene(const Scene&) = delete;
	Scene & operator=(const Scene&) = delete;
	virtual ~Scene();

	void EndScene();
	void DeleteScene();
	virtual void CleanUp() = 0;

	CameraManager& GetCameraManager();
	void AddDeleteCommand(CommandBase* cmd_);
	void AddComputeCommand(CommandBase* cmd_);
	void AddVisualizerCommand(CommandBase* cmd_);

protected:
	template<typename CollisionType_1, typename CollisionType_2>
	void SetCollisionPair() {
		mCollidableMangaer.SetCollisionPair<CollisionType_1, CollisionType_2>();
	};

	template<typename CollisionType>
	void SetCollisionSelf() {
		mCollidableMangaer.SetCollisionSelf <CollisionType>();
	}
private:
	// to be used with broker
	void SubmitCommand(CommandBase*);

	// to be moved to private 
	void Register(Updateable&);
	void Register(Drawable&);
	void Register(Sprite&);
	void Register(Alarmable&, float, AlarmableManager::ALARM_ID);
	void Register(Inputable &, AZUL_KEY, INPUT_EVENT_TYPE);

	void Deregister(Updateable&);
	void Deregister(Drawable&);
	void Deregister(Sprite&);
	void Deregister(Alarmable&, AlarmableManager::ALARM_ID);
	void Deregister(Inputable &, AZUL_KEY, INPUT_EVENT_TYPE);

	virtual void Init() = 0;
	void Update();
	void Draw();

	// Debug
	//void PrintFrameTime();
	//void PrintTotalTime();

	// members
	CameraManager mCameraManager;
	SceneRegistrationBroker mBroker;
	UpdateableManager mUpdateableManager;
	DrawableManager mDrawableManager;
	Sprite2DManager mDrawable2DManager;
	AlarmableManager mAlarmableManager;
	CollisionManager mCollidableMangaer;
	KeyboardEventManager mKeyboardEventManager;
	DestroySceneCommand* mpDestroySceneCmd;

};
#endif // !Scene_
