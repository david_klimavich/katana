#ifndef SpriteDeregistrationCommand_
#define SpriteDeregistrationCommand_

#include "CommandBase.h"
#include "DebugOut.h"
#include "SpriteAttorney.h"

class SpriteDeregistrationCommand : public CommandBase {
public:
	explicit SpriteDeregistrationCommand(Sprite* sprite_) :
		mpSprite(sprite_) {};
	~SpriteDeregistrationCommand() = default;

	void Execute() override {
		SpriteAttorney::Registration::SceneDeregistration(*mpSprite);
	}
private:
	Sprite * mpSprite;
};
#endif // !Draw2DDeregistrationCommand_
