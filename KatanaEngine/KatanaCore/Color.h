#ifndef Color_
#define Color_

#include "Vect.h"

class Color {
public:
	static const Vect Black;
	static const Vect White;
	static const Vect LightGray;
	static const Vect DarkGray;
	static const Vect Red;
	static const Vect Green;
	static const Vect Blue;
};

#endif //Color_