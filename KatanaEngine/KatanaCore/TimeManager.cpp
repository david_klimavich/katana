#include "TimeManager.h"

TimeManager* TimeManager::instance = nullptr;
TimeManager::TimeManager()
	: mTimeAtLastFrame(0.0f) {}


float TimeManager::GetTimeInSeconds() {
	return GetInstance().PrivateGetTimeInSeconds();
}

float TimeManager::GetFrameTime() {
	return GetInstance().PrivateGetFrameTime();
}

void TimeManager::ProcessTime() {
	GetInstance().PrivateTestForFreezeFrame();
	GetInstance().PrivateMarkFrameTime();
}

float TimeManager::PrivateGetTimeInSeconds() {
	return mFreezTime.GetTimeInSeconds(KatanaEngine::GetTimeInSeconds());
}

float TimeManager::PrivateGetFrameTime() {
	return (PrivateGetTimeInSeconds() - mTimeAtLastFrame);
}

void TimeManager::PrivateMarkFrameTime() {
	mTimeAtLastFrame = PrivateGetTimeInSeconds();
}

void TimeManager::PrivateTestForFreezeFrame() {
	mFreezTime.GetTimeInSeconds(KatanaEngine::GetTimeInSeconds());
}
TimeManager::~TimeManager() {}
