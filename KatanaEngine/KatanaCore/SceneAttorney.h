#ifndef SceneAttorney_
#define SceneAttorney_

#include "Scene.h"
#include "SceneManager.h"

class BaseCommand;
class SceneAttorney {
public:
	class Command {
		friend class SceneManager;
		friend class GameObject; 
		// SUBMIT COMMAND
		static void SubmitCommand(CommandBase* commandBase_) {
			SceneManager::GetActiveScene()->SubmitCommand(commandBase_);
		};
	};
	class Registration {
		friend class Inputable;
		friend class Updateable;
		friend class Alarmable;
		friend class Drawable;
		friend class GameObject;
		friend class Collidable;
		friend class Sprite;

		// SUBMIT COMMAND
		static void SubmitCommand(CommandBase* commandBase_) {
			SceneManager::GetActiveScene()->SubmitCommand(commandBase_);
		};

		// REGISTERS
		static void Register(Updateable& updateable_) {
			SceneManager::GetActiveScene()->Register(updateable_);
		}
		static void Register(Drawable& drawable_) {
			SceneManager::GetActiveScene()->Register(drawable_);
		};
		static void Register(Alarmable& alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_) {
			SceneManager::GetActiveScene()->Register(alarmable_, time_, alarmID_);
		};
		static void Register(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_) {
			SceneManager::GetActiveScene()->Register(inputable_, key_, eventType_);
		};
		static void Register(Sprite& sprite_) {
			SceneManager::GetActiveScene()->Register(sprite_);
		};

		// DEREGISTERS
		static void Deregister(Updateable& updateable_) {
			SceneManager::GetActiveScene()->Deregister(updateable_);
		};
		static void Deregister(Drawable& drawable_) {
			SceneManager::GetActiveScene()->Deregister(drawable_);
		};
		static void Deregister(Sprite& sprite_) {
			SceneManager::GetActiveScene()->Deregister(sprite_);
		};
		static void Deregister(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_) {
			SceneManager::GetActiveScene()->Deregister(alarmable_, alarmID_);
		};
		static void Deregister(Inputable& inputable_, AZUL_KEY key_, INPUT_EVENT_TYPE eventType_) {
			SceneManager::GetActiveScene()->Deregister(inputable_, key_, eventType_);
		};
	};

	class GameLoop {
		friend class SceneManager;
		static void Update(Scene& scene_) {
			scene_.Update();
		};
		static void Draw(Scene& scene_) {
			scene_.Draw();
		};
	};

	class Initialize {
		friend class SceneManager;
		static void InitScene() {
			SceneManager::GetActiveScene()->Init();
		}
	};
};
#endif // !SceneAttorney_
