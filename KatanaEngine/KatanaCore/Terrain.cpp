#include "Terrain.h"
#include "Texture.h"
#include "Model.h"
#include "GpuVertTypes.h"
#include "TextureTGA.h"
#include "TextureManager.h"
#include "CameraManager.h"
#include "SceneManager.h"
#include "ShaderManager.h"
#include "Scene.h"
#include "Color.h"
#include "GraphicsObject_TextureFlat.h"

#include <assert.h>


Terrain::Terrain(TerrainSettings& settings_) : mpTerrainModel(nullptr), mMaxHeight(settings_.maxHeight) {
	int imgWidth, imgHeight, icomp;
	unsigned int dtype;
	GLbyte *imgData = gltReadTGABits(settings_.heightMapFile, &imgWidth, &imgHeight, &icomp, &dtype);
	assert(imgWidth == imgHeight && imgWidth > 0);
	mSideLength = imgWidth; // set member

	int uRep = settings_.uRep;
	int vRep = settings_.vRep;

	const int NUM_VERTS = (imgWidth + 1) * (imgHeight + 1);
	const int NUM_TRIANGLES = ((imgWidth) * (imgWidth)) * 2;

	VertexStride_VUN *pVerts = new VertexStride_VUN[NUM_VERTS];
	TriangleIndex* pTriList = new TriangleIndex[NUM_TRIANGLES];

	uint8_t height_value = 0;
	//int halfSide = (int(mSideLength)) / 2;
	int totalLength = settings_.sideLength;


	// walk all the pixels of the heightmap 
	for (int i = 0; i < imgWidth + 1; ++i)
	{
		for (int j = 0; j < imgWidth + 1; ++j)
		{
			height_value = static_cast<uint8_t>(imgData[PixelIndex(i, j)]);
			height_value = static_cast<uint8_t>(((float)height_value / 255.0f) * mMaxHeight);

			// calculate x,y,z of a single vertex 
			float x = (float(j) / float(imgWidth)) * totalLength;
			float y = static_cast<float>(height_value);
			float z = (float(i) / float(imgWidth)) * totalLength; 

			// calculate normals 
			float nx = 0; 
			float ny = 1;
			float nz = 0; 


			// INVESTIGATE ORDER OF PVERTS / TRIANGLES USING PVERTS
			// add to array
			int test = VertexIndex(i, j); 
			test;
			pVerts[VertexIndex(i, j)].set(x, y, z, 
				((float)j / imgWidth) * uRep, ((float)i / imgWidth) * vRep, 
				nx, ny, nz);
		}
	}

	// construct triangle from pVerts
	for (int i = 0; i < imgWidth; ++i)
	{
		for (int j = 0; j < imgWidth; ++j)
		{
			TriangleIndex &pTri_1 = pTriList[TriIndex(i, j)];
			TriangleIndex &pTri_2 = pTriList[TriIndex(i, j) + 1];

			
			unsigned int topLeftInd = VertexIndex(i, j);
			unsigned int topRightInd = VertexIndex(i, j + 1);
			unsigned int bottomLeftInd = VertexIndex(i + 1, j);
			unsigned int bottomRightInd = VertexIndex(i + 1, j + 1); 

			pTri_1.set(topLeftInd, bottomLeftInd, topRightInd);
			pTri_2.set(topRightInd, bottomLeftInd, bottomRightInd);
		}
	}




	//// walk all verticies of heightMap.
	//for(int i = 0; i < imgWidth - 1; i++) {
	//	for(int j = 0; j < imgWidth - 1; j++) {
	//		height_value = static_cast<uint8_t>(imgData[PixelIndex(i,j)]);
	//		height_value = uint8_t(((float)height_value / 255.0f) * mMaxHeight);

	//		// calculate x,y,z of vertex
	//		float x = ((float)-j + halfSide) * totalLength;
	//		float y = ((float)height_value);
	//		float z = ((float)i + halfSide) * totalLength;

	//		// compute the normal
	//		float nx = 0;
	//		float ny = 1;
	//		float nz = 0;

	//		pVerts[VertexIndex(i, j)].set(x, y, z,
	//			((float)i / imgWidth) * vRep,
	//			((float)j / imgWidth) * uRep,
	//			nx, ny, nz);
	//	}
	//}

	//// construct all the tris 
	//for(int i = 0; i < imgWidth - 1; i++)
	//	for(int j = 0; j < imgWidth -1; j++) {

	//		TriangleIndex &pTri_1 = pTriList[TriIndex(i, j)];
	//		TriangleIndex &pTri_2 = pTriList[TriIndex(i, j) + 1];

	//		unsigned int topLeft = VertexIndex(i, j);
	//		unsigned int topRight = VertexIndex(i + 1, j);
	//		unsigned int bottomRight = VertexIndex(i + 1, j + 1);
	//		unsigned int bottomLeft = VertexIndex(i, j + 1);

	//		pTri_1.set(topLeft, bottomLeft, topRight);
	//		pTri_2.set(bottomLeft, bottomRight, topRight);
	//	}

	mpTerrainModel = new Model(pVerts, NUM_VERTS, pTriList, NUM_TRIANGLES);
	mpGObj_Terrain = new GraphicsObject_TextureFlat(mpTerrainModel, ShaderManager::GetShader("flat_shader"), TextureManager::GetTexture("HMTest4"));

	// clean up, clean up, everybody do your share
	delete[] pVerts;
	delete[] pTriList;

	Drawable::SubmitRegister();
}

void Terrain::Draw() {
	mpGObj_Terrain->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

Terrain::~Terrain() {
	delete mpTerrainModel;
	delete mpGObj_Terrain;

	mpTerrainModel = nullptr;
	mpGObj_Terrain = nullptr;
}

int Terrain::VertexIndex(int i, int j) {
	return (i * (mSideLength + 1)) + j;
}

int Terrain::PixelIndex(int i, int j) {
	return (i * mSideLength) + j; 
}

int Terrain::TriIndex(int i, int j) {
	return ((i * (mSideLength)) + j) * 2;
}

