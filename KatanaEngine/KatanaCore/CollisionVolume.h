#ifndef CollisionVolume_
#define CollisionVolume_

class CollisionVolumeBSphere;
class CollisionVolumeAABB;
class CollisionVolumeOBB;
class Model;
class Matrix;
class Vect;

class CollisionVolume {
public:
	CollisionVolume() = default;
	virtual ~CollisionVolume() = default;

	virtual void ComputeData(Model* model_, const Matrix& matrix_) = 0;
	virtual void DebugView(const Vect& color_) const = 0;
	virtual bool IntersectAccept(const CollisionVolume& other_) const = 0;
	virtual bool IntersectVisit(const CollisionVolumeBSphere& other) const = 0;
	virtual bool IntersectVisit(const CollisionVolumeAABB& other_) const = 0;
	virtual bool IntersectVisit(const CollisionVolumeOBB& other_) const = 0;

private:

};

#endif