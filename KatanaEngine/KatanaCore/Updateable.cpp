#include <assert.h>
#include "Updateable.h"
#include "SceneAttorney.h"
#include "UpdateRegistartionCommand.h"
#include "UpdateDeregistrationCommand.h"

Updateable::Updateable() {
	registrationCmd = new UpdateRegistrationCommand(this);
	deregistrationCmd = new UpdateDeregistrationCommand(this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
}

Updateable::~Updateable() {
	delete registrationCmd;
	delete deregistrationCmd;

	registrationCmd = nullptr;
	deregistrationCmd = nullptr;
}

REGISTRATION_STATE Updateable::GET_REG_STATE() {
	return REG_STATE;
}

void Updateable::SetIt(KLI::UpdateableListIt& it_) {
	it = it_;
}
KLI::UpdateableListIt& Updateable::GetIt() {
	return it;
}

void Updateable::SubmitRegister() {
	assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
	SceneAttorney::Registration::SubmitCommand(registrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
}

void Updateable::SubmitDeregister() {
    assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
	SceneAttorney::Registration::SubmitCommand(deregistrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
}

void Updateable::SceneRegistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
	SceneAttorney::Registration::Register(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
}

void Updateable::SceneDeregistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
	SceneAttorney::Registration::Deregister(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
}


