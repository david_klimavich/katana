#include "Color.h"

const Vect Color::Black = Vect(0, 0, 0, 1);
const Vect Color::White = Vect(1, 1, 1, 1);
const Vect Color::LightGray = Vect(.75f, .75f, .75f, 1);
const Vect Color::DarkGray = Vect(.5f, .5f, .5f, 1);
const Vect Color::Red = Vect(1, 0, 0, 1);
const Vect Color::Green = Vect(0, 1, 0, 1);
const Vect Color::Blue = Vect(0, 0, 1, 1);