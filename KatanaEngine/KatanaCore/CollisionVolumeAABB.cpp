#include "CollisionVolumeAABB.h"
#include "CollisionVolumeBSphere.h"
#include "Model.h"
#include "Matrix.h"
#include "Visualizer.h"
#include "Katana_Math.h"

CollisionVolumeAABB::CollisionVolumeAABB() : mpWorld(new Matrix(IDENTITY)), mScaleSquared(0.0f) {}

CollisionVolumeAABB::~CollisionVolumeAABB() {
	delete mpWorld;
}

void CollisionVolumeAABB::ResetAABB() {
	min = Vect(FLT_MAX, FLT_MAX, FLT_MAX);
	max = Vect(-FLT_MAX, -FLT_MAX, -FLT_MAX);
}

void CollisionVolumeAABB::ComputeData(Model * model_, const Matrix & matrix_) {

	Vect* vectArray = model_->getVectList();
	min = vectArray[0] * matrix_;
	max = vectArray[0] * matrix_;

	for(int i = 0; i < model_->getVectNum(); i++) {
		Vect tmp = vectArray[i] * matrix_;

		// clamp x
		if(tmp.X() < min.X()) {
			min.X() = tmp.X();
		} else if(tmp.X() > max.X()) {
			max.X() = tmp.X();
		}

		// clamp y
		if(tmp.Y() < min.Y()) {
			min.Y() = tmp.Y();
		} else if(tmp.Y() > max.Y()) {
			max.Y() = tmp.Y();
		}

		// clamp z
		if(tmp.Z() < min.Z()) {
			min.Z() = tmp.Z();
		} else if(tmp.Z() > max.Z()) {
			max.Z() = tmp.Z();
		}
	}

	//mpWorld = &matrix_;

	// temp - no need to do this every frame ==========================
	// Step 2: Get the diagonal radius 
	mHalfDiagonal = 0.5f * (max - min);

	// Step 3: Get the center in model space
	mCenter = (min + mHalfDiagonal); // world matrix for AABB center is IDENTITY. 

	// Step 4: Scale Squared
	mScaleSquared = 1.0f;//mpWorld->get(ROW_0).magSqr();
	// temp - no need to do this every frame ==========================
}

void CollisionVolumeAABB::ComputeData(const CollisionVolumeBSphere & bsphere_) {
	float r = bsphere_.GetRadius();
	Vect tempMax = bsphere_.GetCenter() + Vect(r, r, r);
	Vect tempMin = bsphere_.GetCenter() - Vect(r, r, r);

	min.X() = Katana_Math::Min(tempMin.X(), min.X());
	min.Y() = Katana_Math::Min(tempMin.Y(), min.Y());
	min.Z() = Katana_Math::Min(tempMin.Z(), min.Z());

	max.X() = Katana_Math::Max(tempMax.X(), max.X());
	max.Y() = Katana_Math::Max(tempMax.Y(), max.Y());
	max.Z() = Katana_Math::Max(tempMax.Z(), max.Z());
}

void CollisionVolumeAABB::DebugView(const Vect & color_) const {
	Visualizer::ShowAABB(*this, color_);
}

bool CollisionVolumeAABB::IntersectAccept(const CollisionVolume & other_) const {
	return other_.IntersectVisit(*this);
}

bool CollisionVolumeAABB::IntersectVisit(const CollisionVolumeBSphere & other_) const {
	return Katana_Math::TestIntersection(other_, *this);
}

bool CollisionVolumeAABB::IntersectVisit(const CollisionVolumeAABB & other_) const {
	return Katana_Math::TestIntersection(*this, other_);
}

bool CollisionVolumeAABB::IntersectVisit(const CollisionVolumeOBB & other_) const {
	return Katana_Math::TestIntersection(*this, (CollisionVolumeBB&)other_);
}
