#ifndef CommandBase_
#define CommandBase_


class CommandBase {
public:
	CommandBase()
	{};
	~CommandBase()
	{};

	virtual void Execute() = 0;

private:


};

#endif