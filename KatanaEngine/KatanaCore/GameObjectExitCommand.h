#ifndef GameObjectExitCommand_
#define GameObjectExitCommand_

#include "CommandBase.h"
#include "GameObject.h"

class GameObjectExitCommand : public CommandBase {
public:
	explicit GameObjectExitCommand(GameObject& gameObject_)
		: gameObject(&gameObject_) {};

	void Execute() override {
		DebugMsg::out("SceneExit...\n");
		gameObject->SceneExit();
	};

private:
	GameObject* gameObject;
};


#endif // !GameObjectExitCommand_
