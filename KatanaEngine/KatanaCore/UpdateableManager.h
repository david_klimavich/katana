#ifndef UpdateableManager_
#define UpdateableManager_

#include <list>
#include "ContainerAlias.h"

class Updateable;
class UpdateableManager {
public:
	UpdateableManager()
	{};
	~UpdateableManager();

	void Register(Updateable&);
	void Deregister(Updateable&);

	void ProcessElements();

private:

	KL::UpdateableList updateableList;
};

#endif // !UpdateableManager_

