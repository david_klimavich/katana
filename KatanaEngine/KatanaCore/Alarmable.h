///-------------------------------------------------------------------------------------------------
/// @file KatanaCore\Alarmable.h.
/// Declares the alarmable class
///-------------------------------------------------------------------------------------------------
#ifndef Alarmable_
#define Alarmable_

#include "RegistrationState.h"
#include "ContainerAlias.h"
#include "AlarmableManager.h"

class AlarmRegistrationCommand;
class AlarmDeregistrationCommand;
class Alarmable {
	friend class AlarmableAttorney;
public:
	/// Default constructor
	Alarmable();
	/// Destructor
	virtual ~Alarmable();

	///-------------------------------------------------------------------------------------------------
	/// Trigger specifed alarm.
	/// @param alarmID_ Identifier for the alarm to trigger.
	///-------------------------------------------------------------------------------------------------
	void TriggerAlarm(AlarmableManager::ALARM_ID alarmID_); // move to private

protected:

	///-------------------------------------------------------------------------------------------------
	/// Submit register of specified alarm.
	/// @param parameter1 The first parameter.
	/// @param alarmID_   Identifier for the alarm.
	///-------------------------------------------------------------------------------------------------
	void SubmitRegister(float, AlarmableManager::ALARM_ID alarmID_);

	///-------------------------------------------------------------------------------------------------
	/// Submit deregister of specified alarm. 
	/// @param alarmID_ Identifier for the alarm.
	///-------------------------------------------------------------------------------------------------
	void SubmitDeregister(AlarmableManager::ALARM_ID alarmID_);
private:
	/// The alarm map iterator
	using AlarmMapIt = AlarmableManager::TimeLineMap::iterator;

	/// registration data struct for each Alarm.
	struct RegistrationData {
		AlarmableManager::TimeLineMap::iterator pDeleteRef;
		REGISTRATION_STATE REG_STATE;
		AlarmRegistrationCommand* pRegistrationCmd;
		AlarmDeregistrationCommand*pDeregistrationCmd;
	};

	RegistrationData mAlarmArray[AlarmableManager::ALARM_COUNT];

	///-------------------------------------------------------------------------------------------------
	/// Scene registration
	/// @param time_    Time till @c alarmID_ is triggered.
	/// @param alarmID_ Identifier for the alarm.
	///-------------------------------------------------------------------------------------------------
	void SceneRegistration(float time_, AlarmableManager::ALARM_ID alarmID_);

	///-------------------------------------------------------------------------------------------------
	/// Deregistered specified alarm from the scene. 
	/// @param alarmID_ Identifier for the alarm.
	///-------------------------------------------------------------------------------------------------
	void SceneDeregistration(AlarmableManager::ALARM_ID alarmID_);

	///-------------------------------------------------------------------------------------------------
	/// Sets a reference in @c RegistrationData for quick remove on dereg.
	/// @param it_      Iterator to copy.
	/// @param alarmID_ Identifier for the alarm.
	///-------------------------------------------------------------------------------------------------
	void SetRef(AlarmableManager::TimeLineMap::iterator it_, AlarmableManager::ALARM_ID alarmID_);

	///-------------------------------------------------------------------------------------------------
	/// Gets a reference
	/// @param alarmID_ Identifier for the alarm.
	/// @return The reference.
	///-------------------------------------------------------------------------------------------------
	AlarmMapIt&  GetRef(AlarmableManager::ALARM_ID alarmID_);

	/// Alarm 0
	virtual void Alarm0() = 0;
	/// Alarm 1
	virtual void Alarm1() = 0;
	/// Alarm 2
	virtual void Alarm2() = 0;

public:

	///-------------------------------------------------------------------------------------------------
	/// Gets alarm 0
	/// @return The alarm 0.
	///-------------------------------------------------------------------------------------------------
	const RegistrationData& GetAlarm0();

	///-------------------------------------------------------------------------------------------------
	/// Gets alarm 1
	/// @return The alarm 1.
	///-------------------------------------------------------------------------------------------------
	const RegistrationData& GetAlarm1();

	///-------------------------------------------------------------------------------------------------
	/// Gets alarm 2
	/// @return The alarm 2.
	///-------------------------------------------------------------------------------------------------
	const RegistrationData& GetAlarm2();



};

#endif // !Alarmable_
