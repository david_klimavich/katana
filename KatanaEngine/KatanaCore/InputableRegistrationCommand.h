#ifndef InputableRegistrationCommand_
#define InputabelRegistrationCommand_

#include "CommandBase.h"
#include "Inputable.h"
class InputableRegistrationCommand : public CommandBase {
public:
	InputableRegistrationCommand(Inputable&, AZUL_KEY, INPUT_EVENT_TYPE);
	~InputableRegistrationCommand();

	void Execute() override;

private:
	Inputable* mpInputable;
	AZUL_KEY mKey;
	INPUT_EVENT_TYPE mEventType;
};
#endif // !InputableRegistrationCommand_
