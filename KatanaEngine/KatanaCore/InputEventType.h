#ifndef INPUT_EVENT_TYPE_
#define INPUT_EVENT_TYPE_

enum class INPUT_EVENT_TYPE {
	KEY_PRESS,
	KEY_RELEASE,
	KEY_HELD_DOWN,
	KEY_HELD_UP
};
#endif // !INPUT_EVENT_TYPE_
