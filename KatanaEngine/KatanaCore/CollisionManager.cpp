#include "CollisionManager.h"

CollisionManager::~CollisionManager() {
	for(auto it = testCmdCollection.begin(); it != testCmdCollection.end(); it++) {
		CollisionTestCommandBase* temp = (*it);
		delete temp;
	}

	for(auto it = mGroupCollectionDelete.begin(); it != mGroupCollectionDelete.end(); it++) {
		(*it)->Execute();
	}
}

void CollisionManager::ProcessCollisions() {
	for(auto it = mGroupCollectionCompute.begin(); it != mGroupCollectionCompute.end(); it++) {
		(*it)->Execute();
	}

	for(auto it = testCmdCollection.begin(); it != testCmdCollection.end(); it++) {
		(*it)->Execute();
	}
}

void CollisionManager::AddDeleteCommand(CommandBase* cmd_) {
	mGroupCollectionDelete.insert(mGroupCollectionDelete.begin(), cmd_);
}

void CollisionManager::AddComputeCommand(CommandBase *cmd_) {
	mGroupCollectionCompute.insert(mGroupCollectionCompute.begin(), cmd_);
}

