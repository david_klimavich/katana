#ifndef TimeManager_
#define TimeManager_
#include "KatanaEngine.h"
#include "FreezeTime.h"

class TimeManager {

private:
	TimeManager();
	TimeManager(const TimeManager&) = delete;
	TimeManager operator=(const TimeManager&) = delete;
	~TimeManager();

	static TimeManager *instance;
	static TimeManager& GetInstance() {
		if(!instance)
		{
			instance = new TimeManager;
		}
		return *instance;
	}

public:

	static float GetTimeInSeconds();
	static float GetFrameTime();
	static void ProcessTime();

private:

	float PrivateGetTimeInSeconds();
	float PrivateGetFrameTime();
	void PrivateMarkFrameTime();
	void PrivateTestForFreezeFrame();

	// members
	float mTimeAtLastFrame;
	FreezeTime mFreezTime;

};
#endif // !TimeManager_
