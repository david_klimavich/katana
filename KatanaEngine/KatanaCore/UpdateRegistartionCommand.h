#ifndef UpdateRegistrationCommand_
#define UpdateRegistrationCommand_

#include "CommandBase.h"
#include "SceneManager.h"
#include "Scene.h"
#include "Updateable.h"
#include "DebugOut.h"
#include "UpdateableAttorney.h"

class UpdateRegistrationCommand : public CommandBase {

public:

	explicit UpdateRegistrationCommand(Updateable* updateable_)
		: updateable(updateable_) {}


	void Execute() override {
		UpdateableAttorney::Registration::SceneRegistration(*updateable);
	}

private:

	Updateable* updateable;

};
#endif // !UpdateRegistrationCommand_
