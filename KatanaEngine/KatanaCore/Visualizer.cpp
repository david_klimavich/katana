#include "Visualizer.h"
#include "SceneManager.h"
#include "ShaderManager.h"
#include "ModelManager.h"
#include "Scene.h"
#include "CollisionVolumeBSphere.h"
#include "CollisionVolumeAABB.h"
#include "CollisionVolumeOBB.h"
#include "VisualizerRenderCommand.h"
#include "VisualizerRenderAABB_cmd.h"
#include "VisualizerRenderOBB_cmd.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Color.h"
#include "Collidable.h"

Visualizer* Visualizer::instance = nullptr;
Vect Visualizer::DEFAULT_COLOR = Color::Blue;
Visualizer::Visualizer() {
	WFUnitSphere = new GraphicsObject_WireframeConstantColor(
		ModelManager::GetModel(ModelManager::UnitSphere),
		ShaderManager::GetShader("colorConstantRender_shader"),
		DEFAULT_COLOR);

	WFAABB = new GraphicsObject_WireframeConstantColor(
		ModelManager::GetModel(ModelManager::UnitBox_WF),
		ShaderManager::GetShader("colorConstantRender_shader"),
		DEFAULT_COLOR);

	WFOBB = new GraphicsObject_WireframeConstantColor(
		ModelManager::GetModel(ModelManager::UnitBox_WF),
		ShaderManager::GetShader("colorConstantRender_shader"),
		DEFAULT_COLOR);

}

void Visualizer::ShowBSphere(const CollisionVolumeBSphere & sphere_, const Vect & color_) {
	GetInstance().PrivShowBSphere(sphere_, color_);
}

void Visualizer::ShowAABB(const CollisionVolumeAABB & AABB_, const Vect & color_) {
	GetInstance().PrivShowAABB(AABB_, color_);
}

void Visualizer::ShowOBB(const CollisionVolumeOBB & OBB_, const Vect & color_) {
	GetInstance().PrivShowOBB(OBB_, color_);
}

void Visualizer::ProcessVisualizerBroker() {
	GetInstance().PrivProcessVisualizerBroker();
}

void Visualizer::ShowCollisionVolume(const Collidable * collidable_, const Vect & color_) {
	GetInstance().PrivShowCollisionVolume(collidable_, color_);
}

void Visualizer::PrivRenderBSphere(Matrix & sphereWorld_, const Vect & color_) const {
	WFUnitSphere->SetWorld(sphereWorld_);
	WFUnitSphere->pWireColor->set(color_);
	WFUnitSphere->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

void Visualizer::PrivRenderAABB(Matrix & AABBWorld_, const Vect & color_) const {
	WFAABB->SetWorld(AABBWorld_);
	WFAABB->pWireColor->set(color_);
	WFAABB->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

void Visualizer::PrivRenderOBB(Matrix & OBBWorld_, const Vect & color_) const {
	WFOBB->SetWorld(OBBWorld_);
	WFOBB->pWireColor->set(color_);
	WFOBB->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

void Visualizer::PrivShowBSphere(const CollisionVolumeBSphere & sphere_, const Vect & color_) {

	Vect BSSize = sphere_.GetRadius() * Vect(1, 1, 1);
	Matrix worldBS = Matrix(SCALE, BSSize) * Matrix(TRANS, sphere_.GetCenter());

	//PrivRenderBSphere(worldBS, color_);
	renderCmd = new VisualizerRenderCommand(worldBS, color_);

	// register to visualizer broker
	mVisualizerBroker.AddCommand(renderCmd);
}

void Visualizer::PrivShowAABB(const CollisionVolumeAABB & AABB_, const Vect & color_) {
	Vect scale_vect = AABB_.getMax() - AABB_.getMin();
	Matrix scale = Matrix(SCALE, scale_vect);

	Vect trans_vect = .5f * (AABB_.getMax() + AABB_.getMin());
	Matrix trans = Matrix(TRANS, trans_vect);

	Matrix world = scale * trans;

	renderCmd = new VisualizerRenderAABB_cmd(world, color_);

	mVisualizerBroker.AddCommand(renderCmd);
}

void Visualizer::PrivShowOBB(const CollisionVolumeOBB & OBB_, const Vect & color_) {
	Vect scale_vect = OBB_.getMax() - OBB_.getMin();
	Matrix scale = Matrix(SCALE, scale_vect);

	Vect trans_vect = .5f * (OBB_.getMin() + OBB_.getMax());
	Matrix trans = Matrix(TRANS, trans_vect);

	Matrix world = scale * trans;
	world *= OBB_.getWorld();

	renderCmd = new VisualizerRenderOBB_cmd(world, color_);
	mVisualizerBroker.AddCommand(renderCmd);
}

void Visualizer::PrivProcessVisualizerBroker() {
	mVisualizerBroker.ProcessElements();
}

void Visualizer::PrivShowCollisionVolume(const Collidable * collidable_, const Vect & color_) {
	collidable_->GetCollisionVolume().DebugView(color_);
}
