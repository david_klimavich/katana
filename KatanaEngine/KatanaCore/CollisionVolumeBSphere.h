#ifndef CollisionVolumeBSphere_
#define CollisionVolumeBSphere_
#include "CollisionVolume.h"
#include "Vect.h"

class Model;
class CollisionVolumeBSphere : public CollisionVolume, public Align16 {
public:
	CollisionVolumeBSphere() = default;

	void ComputeData(Model* model_, const Matrix& matrix_) override;
	void DebugView(const Vect& col) const override;
	bool IntersectAccept(const CollisionVolume& other_) const override;
	bool IntersectVisit(const CollisionVolumeBSphere& other) const override;
	bool IntersectVisit(const CollisionVolumeAABB& other_) const override;
	bool IntersectVisit(const CollisionVolumeOBB& other_) const override;

	const Vect&  GetCenter() const;
	float GetRadius() const;

private:
	Vect mCenter;
	float mRadius;
};
#endif