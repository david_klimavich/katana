#include "CollisionVolumeOBB.h"
#include "CollisionVolumeBSphere.h"
#include "CollisionVolumeAABB.h"
#include "Model.h"
#include "Matrix.h"
#include "Visualizer.h"
#include "Katana_Math.h"

CollisionVolumeOBB::CollisionVolumeOBB() : mpWorld(new Matrix(IDENTITY)), mScaleSquared(0.0f) {}

CollisionVolumeOBB::~CollisionVolumeOBB() {}

void CollisionVolumeOBB::ComputeData(Model * model_, const Matrix & matrix_) {
	// Step 1: Get the Max and Min points in model space
	min = model_->getMinAABB();
	max = model_->getMaxAABB();
	//mpWorld = &matrix_;
	mWorldInv = mpWorld->getInv();

	// Step 2: Get the diagonal radius 
	mHalfDiagonal = 0.5f * (max - min);

	// Step 3: Get the center in model space
	mCenter = (min + mHalfDiagonal) * matrix_;

	// Step 4: Scale Squared
	mScaleSquared = matrix_.get(ROW_0).magSqr();
}

void CollisionVolumeOBB::DebugView(const Vect & color_) const {
	Visualizer::ShowOBB(*this, color_);
}

bool CollisionVolumeOBB::IntersectAccept(const CollisionVolume & other_) const {
	return other_.IntersectVisit(*this);
}

bool CollisionVolumeOBB::IntersectVisit(const CollisionVolumeBSphere & other_) const {
	return Katana_Math::TestIntersection(*this, other_);
}

bool CollisionVolumeOBB::IntersectVisit(const CollisionVolumeAABB & other_) const {
	return Katana_Math::TestIntersection(*this, other_);;
}

bool CollisionVolumeOBB::IntersectVisit(const CollisionVolumeOBB & other_) const {
	return Katana_Math::TestIntersection(*this, other_);
}
