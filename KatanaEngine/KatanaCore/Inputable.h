#ifndef Inputable_
#define Inputable_

#include "AzulCore.h"
#include "InputEventType.h"

class InputableAttorney;
class Inputable {
	friend class InputableAttorney;

public:
	Inputable();
	virtual ~Inputable();

protected:
	void SubmitKeyRegistration(AZUL_KEY k, INPUT_EVENT_TYPE e);
	void SubmitKeyDeregistration(AZUL_KEY k, INPUT_EVENT_TYPE e);

private:
	void SceneRegistration(AZUL_KEY k, INPUT_EVENT_TYPE e);
	void SceneDeregistration(AZUL_KEY k, INPUT_EVENT_TYPE e);

	virtual void KeyPressed(AZUL_KEY) = 0;
	virtual void KeyReleased(AZUL_KEY) = 0;
	virtual void KeyHeldDown(AZUL_KEY) = 0; 
	virtual void KeyHeldUp(AZUL_KEY) = 0; 

};

#endif // !Inputable_
