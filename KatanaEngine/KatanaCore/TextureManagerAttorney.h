#ifndef TextureManagerAttorney_
#define TextureManagerAttorney_
#include "TextureManager.h"

class TextureManagerAttorney {
	friend class KatanaEngine;

private:
	static void CleanUp()
	{
		TextureManager::CleanUp();
	}
};
#endif // !TextureManagerAttorney_
