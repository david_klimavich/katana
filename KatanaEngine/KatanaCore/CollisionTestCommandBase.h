#ifndef CollisionTestCommandBase_
#define CollisionTestCommandBase_

class CollisionTestCommandBase {
public:
	CollisionTestCommandBase() = default;
	virtual ~CollisionTestCommandBase() = default;
	virtual void Execute() = 0;

private:

};
#endif // !CollisionTestCommandBase_
