#include "GodCam.h"
#include "Camera.h"
#include "KatanaEngine.h"

GodCamController::GodCamController() : mpGodCam(nullptr) {}
GodCamController::~GodCamController() {
	delete mpGodCam;
	mpGodCam = nullptr;
}

void GodCamController::InitGodCam(const Camera& /*sceneCam_*/) {

	mpGodCam = new Camera(Camera::Type::PERSPECTIVE_3D);
	mCamPos = Vect(50.0f, 50.0f, 150.0f);
	mCamUp = Vect(0, 1, 0);
	mCamDir = Vect(0.0f, 0.0f, 1.0f);
	mCamRot = Matrix(IDENTITY);

	mpGodCam->setViewport(0, 0, KatanaEngine::GetWindowWidth(), KatanaEngine::GetWindowHeight());

	mpGodCam->setPerspective(35.0f, float(KatanaEngine::GetWindowWidth()) / float(KatanaEngine::GetWindowHeight()),
		1.0f, 5000.0f);

	mpGodCam->setOrientAndPosition(mCamUp, mCamDir, mCamPos);
	mpGodCam->updateCamera();


}

Camera & GodCamController::GetCam() {
	return *mpGodCam;
}

void GodCamController::SceneEntry() {
	Updateable::SubmitRegister();
}

void GodCamController::SceneExit() {
	Updateable::SubmitDeregister();
	delete mpGodCam;
}


void GodCamController::Update() {
	//rotation 
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_KP_6)) {
		mCamRot *= Matrix(ROT_Y, -mCamRotSpeed);
	}
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_KP_4)) {
		mCamRot *= Matrix(ROT_Y, mCamRotSpeed);
	}
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_KP_8)) {
		mCamRot *= Matrix(ROT_AXIS_ANGLE, Vect(1, 0, 0) * mCamRot, mCamRotSpeed);
	}
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_KP_5)) {
		mCamRot *= Matrix(ROT_AXIS_ANGLE, Vect(1, 0, 0) * mCamRot, -mCamRotSpeed);
	}

	//movement
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_I)) {
		mCamPos += Vect(0, 0, 1) * mCamRot * mCamTransSpeed;
	}
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_K)) {
		mCamPos += Vect(0, 0, 1) * mCamRot * -mCamTransSpeed;
	}

	if(Keyboard::GetKeyState(AZUL_KEY::KEY_J)) {
		mCamPos += Vect(1, 0, 0) * mCamRot * mCamTransSpeed;
	}
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_L)) {
		mCamPos += Vect(1, 0, 0) *  mCamRot * -mCamTransSpeed;
	}

	if(Keyboard::GetKeyState(AZUL_KEY::KEY_U)) {
		mCamPos += Vect(0, 1, 0) * mCamTransSpeed;
	}

	if(Keyboard::GetKeyState(AZUL_KEY::KEY_O)) {
		mCamPos += Vect(0, 1, 0) * -mCamTransSpeed;
	}

	// Update the camera settings
	mpGodCam->setOrientAndPosition(mCamUp * mCamRot, mCamPos + mCamDir * mCamRot, mCamPos);
	mpGodCam->updateCamera();
}
