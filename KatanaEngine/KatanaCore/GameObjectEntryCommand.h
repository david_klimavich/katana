#ifndef GameObjectEntryCommand_
#define GameObjectEntryCommand_

#include "CommandBase.h"
#include "GameObject.h"

class GameObjectEntryCommand : public CommandBase {
public:
	explicit GameObjectEntryCommand(GameObject& gameObject_)
		: pGameObject(&gameObject_) {

	};
	void Execute() override {
		DebugMsg::out("SceneEntry...\n");
		pGameObject->SceneEntry();
	};

private:
	GameObject* pGameObject;
};


#endif // !GameObjectEntryCommand_
#pragma once
