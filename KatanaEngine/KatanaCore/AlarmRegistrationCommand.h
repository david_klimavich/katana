#ifndef AlarmRegistrationCommand_
#define AlarmRegistrationCommand_

#include "CommandBase.h"
#include "SceneManager.h"
#include "Scene.h"
#include "AlarmableAttorney.h"
#include "DebugOut.h"

class AlarmRegistrationCommand : public CommandBase {

public:

	AlarmRegistrationCommand(Alarmable* alarmable_, AlarmableManager::ALARM_ID alarmID_)
		: mpAlarmable(alarmable_), mAlarmID(alarmID_)
	{}

	void Execute() override
	{
		//DebugMsg::out("Executing AlarmReg CMD\n");
		AlarmableAttorney::Registration::SceneRegistration(*mpAlarmable, time, mAlarmID);
	}

	// maybe?
	void SetTimer(float time_)
	{
		time = time_;
	}

private:

	Alarmable* mpAlarmable;
	AlarmableManager::ALARM_ID mAlarmID;
	float time = 0.0f;
};
#endif // !AlarmRegistrationCommand_
