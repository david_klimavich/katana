#ifndef VisualizerRenderOBB_cmd_
#define VisualizerRenderOBB_cmd_

#include "CommandBase.h"
#include "Matrix.h"

class VisualizerRenderOBB_cmd : public CommandBase, public Align16 {
public:
	VisualizerRenderOBB_cmd(Matrix& world_, const Vect& color_) : mWorld(world_), mColor(color_) {}

	void Execute() override {
		Visualizer::GetInstance().PrivRenderOBB(mWorld, mColor);
	}

private:
	Matrix mWorld;
	const Vect mColor;

};
#endif // !VisualizerRenderOBB_cmd_
