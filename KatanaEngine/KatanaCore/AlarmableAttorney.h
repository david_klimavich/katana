#ifndef AlarmableAttorney_
#define AlarmableAttorney_

#include "Alarmable.h"
#include "ContainerAlias.h"

class AlarmableAttorney {
public:
	class Registration {
		friend class AlarmRegistrationCommand;
		friend class AlarmDeregistrationCommand;
		static void SceneRegistration(Alarmable& alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_) {
			alarmable_.SceneRegistration(time_, alarmID_);
		}

		static void SceneDeregistration(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_) {
			alarmable_.SceneDeregistration(alarmID_);
		}
	};

	class Manager {
		friend class AlarmableManager;
		static void SubmitRegistration(Alarmable& alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_) {
			alarmable_.SubmitRegister(time_, alarmID_);
		}
		static void SubmitDeregistration(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_) {
			alarmable_.SubmitDeregister(alarmID_);
		}
		static void SetRef(Alarmable& alarmable_, AlarmableManager::TimeLineMap::iterator  it_, AlarmableManager::ALARM_ID alarmID_) {
			alarmable_.SetRef(it_, alarmID_);
		}
		static AlarmableManager::TimeLineMap::iterator&  GetRef(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_) {
			return alarmable_.GetRef(alarmID_);
		}

		static void ProcessAlarm() {

		}
	};
};
#endif // !AlarmableAttorney_
