#include "Inputable.h"
#include "InputableRegistrationCommand.h"
#include "InputableDeregistrationCommand.h"
#include "SceneAttorney.h"

Inputable::Inputable()
{}

Inputable::~Inputable()
{}

void Inputable::SubmitKeyRegistration(AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
{
	InputableRegistrationCommand* inputableRegCmd = new InputableRegistrationCommand(*this, key_, eventType_);
	SceneAttorney::Registration::SubmitCommand(inputableRegCmd);
}

void Inputable::SubmitKeyDeregistration(AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
{
	InputableDeregistrationCommand* inputableDeregCmd = new InputableDeregistrationCommand(*this, key_, eventType_);
	SceneAttorney::Registration::SubmitCommand(inputableDeregCmd);
}

void Inputable::SceneRegistration(AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
{
	SceneAttorney::Registration::Register(*this, key_, eventType_);
}

void Inputable::SceneDeregistration(AZUL_KEY key_, INPUT_EVENT_TYPE eventType_)
{
	SceneAttorney::Registration::Deregister(*this, key_, eventType_);
}