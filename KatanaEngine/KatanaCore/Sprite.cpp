#include <assert.h>
#include "Sprite.h"
#include "ShaderManager.h"
#include "TextureManager.h"
#include "SceneAttorney.h"
#include "SpriteRegistrationCommand.h"
#include "SpriteDeregistrationCommand.h"

Sprite::Sprite(std::string imageKey_) {
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
	mpRegistrationCmd = new SpriteRegistrationCommand(this); 
	mpDeregistrationCmd = new SpriteDeregistrationCommand(this);

	// create sprites
	mpModelSprite = new Model(Model::PreMadedeModels::UnitSquareXY); 
	mpShaderObject_sprite = ShaderManager::GetShader("spriteRender_shader");
	mpTexture = TextureManager::GetTexture(imageKey_); 
	mpRect = new Rect(
		0,
		0,
		static_cast<float>(mpTexture->getWidth()), 
		static_cast<float>(mpTexture->getHeight()));

	// create image from textures and rect
	mpImage = new Image(mpTexture, mpRect); //ImageManager::GetImage(imageKey_);

	// construct sprite graphics object
	mpGObj_sprite = new GraphicsObject_Sprite(
		mpModelSprite,
		mpShaderObject_sprite,
		mpImage,
		mpImage->pImageRect);

	mpGObj_sprite->origPosX = 1;
	mpGObj_sprite->origPosY = 1;

}
Sprite::~Sprite() {
	delete mpImage;
	delete mpModelSprite;
	delete mpGObj_sprite;
	delete mpRegistrationCmd;
	delete mpDeregistrationCmd;

	mpImage = nullptr;
	mpModelSprite = nullptr;
	mpGObj_sprite = nullptr;
	mpRegistrationCmd = nullptr;
	mpDeregistrationCmd = nullptr;
}

void Sprite::Draw2D() {
	mpGObj_sprite->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActive2DCamera());
}

void Sprite::SetIt(KLI::SpriteListIt ref_) {
	listRefToSelf = ref_;
}

KLI::SpriteListIt Sprite::GetItRef() {
	return listRefToSelf;
}

void Sprite::SubmitRegister() {
	assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_DEREGISTERED);
	SceneAttorney::Registration::SubmitCommand(mpRegistrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_REGISTRATION;
}

void Sprite::SubmitDeregister() {
	assert(REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED);
	SceneAttorney::Registration::SubmitCommand(mpDeregistrationCmd);
	REG_STATE = REGISTRATION_STATE::PENDING_DEREGISTRATION;
}

void Sprite::SceneRegistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_REGISTRATION);
	SceneAttorney::Registration::Register(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_REGISTERED;
}

void Sprite::SceneDeregistration() {
	assert(REG_STATE == REGISTRATION_STATE::PENDING_DEREGISTRATION);
	SceneAttorney::Registration::Deregister(*this);
	REG_STATE = REGISTRATION_STATE::CURRENTLY_DEREGISTERED;
}

void Sprite::SetScale(const float x, const float y)
{
	mScale.set(SCALE, x, y, 1.0f);
	mWorld = mScale * mRotZ * mPos;
	mpGObj_sprite->SetWorld(mWorld);
}

void Sprite::SetPositionByFactor(const float FactorX, const float FactorY) {
	mPos.set(TRANS, mpGObj_sprite->origPosX * FactorX, mpGObj_sprite->origPosY * FactorY, 0.0f);
	mRotZ.set(ROT_Z, 0.0f); 
	mScale.set(SCALE, 1.0f, 1.0f, 1.0f); 
	
	mWorld = mScale * mRotZ * mPos;
	mpGObj_sprite->SetWorld(mWorld);
}


