///-------------------------------------------------------------------------------------------------
/// @file KatanaCore\AlarmableManager.h.
/// Declares the alarmable manager class
///-------------------------------------------------------------------------------------------------
#ifndef AlarmableManager_
#define AlarmableManager_

#include <map>
#include "ContainerAlias.h"

class Alarmable;
class AlarmableManager {
public:
	/// Default constructor
	AlarmableManager();
	/// Destructor
	~AlarmableManager();

	/// Values that represent alarm Identifiers
	enum ALARM_ID {
		ALARM_0 = 0,
		ALARM_1 = 1,
		ALARM_2 = 2
	};

	/// Number of alarms
	static const int ALARM_COUNT = 3;

	/// Process registered alarms.
	void ProcessAlarms();

	///-------------------------------------------------------------------------------------------------
	/// Registers this object
	/// @param [in] alarmable_ Alarmable to register.
	/// @param 	    time_	   Time until alarm will be triggered.
	/// @param 	    alarmID_   Specified alarm.
	///-------------------------------------------------------------------------------------------------
	void Register(Alarmable& alarmable_, float time_, AlarmableManager::ALARM_ID alarmID_);

	///-------------------------------------------------------------------------------------------------
	/// Deregisters this object
	/// @param [in] alarmable_ The first parameter.
	/// @param 	    alarmID_   Specified alarm.
	///-------------------------------------------------------------------------------------------------
	void Deregister(Alarmable& alarmable_, AlarmableManager::ALARM_ID alarmID_);

	using AlarmEvent = std::pair<Alarmable*, ALARM_ID>;
	using TimeLineMap = std::multimap<float, AlarmEvent>;
private:


	/// members

	TimeLineMap alarmableMap;

};


#endif