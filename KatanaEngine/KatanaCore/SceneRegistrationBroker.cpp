#include "SceneRegistrationBroker.h"
#include "CommandBase.h"
#include "DebugOut.h"

void SceneRegistrationBroker::AddCommand(CommandBase* cmd) {
	commandList.insert(commandList.end(), cmd);
}

void SceneRegistrationBroker::ProcessCommands() {

	for(auto it = commandList.begin(); it != commandList.end(); it++) {
		(*it)->Execute();
	}
	commandList.clear();
}

SceneRegistrationBroker::SceneRegistrationBroker() {}

SceneRegistrationBroker::~SceneRegistrationBroker() {}