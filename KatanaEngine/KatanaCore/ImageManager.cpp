#include "ImageManager.h"
#include "Image.h"
#include <assert.h>

ImageManager* ImageManager::instance = nullptr;
void ImageManager::LoadImageFromFile(std::string key_, std::string fileName_) {
	GetInstance().PrivateLoadImage(key_, fileName_);
}

Image* ImageManager::GetImage(std::string key_) {
	return GetInstance().PrivateGetImage(key_);
}

void ImageManager::PrivateLoadImage(std::string key_, std::string fileName_) {
	// Default File Path
	std::string rootPath = "Sprites/";
	std::string relativePath = rootPath + fileName_;

	// Load specified texture 
	// temp (load via the texture manager?) 
	Texture* pTexture = new Texture(relativePath.c_str());
	assert(pTexture); 

	// make image out of rect and the loaded texture
	Rect* pRect = new Rect(
		0.0f, // x
		0.0f, // y
		static_cast<float>(pTexture->getWidth()), // image width
		static_cast<float>(pTexture->getHeight())); // image height

	Image *pImage = new Image(pTexture, pRect);

	// insert into the ModelMap
	mImageMap.insert({ key_, pImage });
}

Image* ImageManager::PrivateGetImage(std::string key_) {
	auto search = mImageMap.find(key_);
	assert(search->second);
	return (search->second);
}

