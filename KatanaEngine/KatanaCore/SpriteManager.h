#ifndef SpriteManager_
#define SpriteManager_
#include "ContainerAlias.h"

class Sprite;
class Sprite2DManager {
public:
	Sprite2DManager() = default;
	~Sprite2DManager() = default;

	void Register(Sprite&);
	void Deregister(Sprite&);

	void ProcessElements();

private:
	KL::SpriteList spriteList;
};
#endif // !SpriteManager_
