#ifndef TextureManager_
#define TextureManager_

#include <map>
#include <string>
#include "Texture.h"
#include "KatanaEngine.h"

class TextureManagerAttorney;
class TextureManager {
public:
	static void LoadTexture(std::string key_, std::string fileName_);
	static Texture* GetTexture(std::string key_);


private:
	TextureManager();
	TextureManager(const TextureManager&) = delete;
	TextureManager operator=(const TextureManager&) = delete;
	~TextureManager();

	friend class TextureManagerAttorney;
	static void CleanUp();
	void PrivateCleanUp();

	static TextureManager* instance;
	static TextureManager& GetInstance() {
		if(!instance) {
			instance = new TextureManager();
		}
		return *instance;
	}

	// Private Methods
	void PrivateLoadTexture(std::string key_, std::string fileName_);
	Texture* PrivateGetTexture(std::string key_);

	bool TextureInMap(std::string key_);

	std::map<std::string, Texture*> textureMap;
	std::string rootPath = "Textures/";
};

#endif // !TextureManager_
