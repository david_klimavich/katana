#ifndef GodCamController_
#define GodCamController_

#include "GameObject.h"

class Matrix;
class Vect;

class GodCamController : public GameObject {
public:
	GodCamController();
	~GodCamController();

	void InitGodCam(const Camera&);
	Camera& GetCam();

private:
	void Update() override;
	void Draw() override {};

	void Alarm0() override {};
	void Alarm1() override {};
	void Alarm2() override {};

	void Collision(Collidable&) override {};
	void KeyPressed(AZUL_KEY) override {};
	void KeyReleased(AZUL_KEY) override {};
	void KeyHeldDown(AZUL_KEY) override {};
	void KeyHeldUp(AZUL_KEY) override {};

	void SceneEntry() override;
	void SceneExit() override;

	// members
	Camera* mpGodCam;

	// Camera vars
	Vect mCamPos;
	Matrix mCamRot;		// No rotation initially
	Vect mCamUp;			// Using local Y axis as 'Up'
	Vect mCamDir;			// Using the local Z axis as 'forward'
	float mCamTransSpeed = 2;
	float mCamRotSpeed = .02f;

};
#endif