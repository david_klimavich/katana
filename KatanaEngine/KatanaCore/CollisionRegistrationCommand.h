#ifndef ColiisionRegistrationCommand_
#define CollisionRegistrationCommand_

#include "SceneRegistrationCommand.h"
#include "CollidableGroup.h"

template <class CollidableType>
class CollisionRegistrationCommand : public SceneRegistrationCommand {
public:
	explicit CollisionRegistrationCommand(CollidableType& collidable_) : mpCollidable(&collidable_) {};
	~CollisionRegistrationCommand() = default;

	void Execute() override {
		CollidableGroup<CollidableType>::Register(*mpCollidable);
	};

private:
	CollidableType* mpCollidable;
};

#endif // !ColiisionRegistrationCommand_

