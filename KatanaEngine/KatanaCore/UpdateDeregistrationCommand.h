#ifndef UpdateDeregistrationCommand_
#define UpdateDeregistrationCommand_

#include "CommandBase.h"
#include "Updateable.h"
#include "DebugOut.h"

class UpdateDeregistrationCommand : public CommandBase {

public:

	explicit UpdateDeregistrationCommand(Updateable* updateable_)
		: updateable(updateable_) {}

	void Execute() override {
		UpdateableAttorney::Registration::SceneDeregistration(*updateable);
	}

private:

	Updateable* updateable;

};
#endif // !UpdateDeregistrationCommand_