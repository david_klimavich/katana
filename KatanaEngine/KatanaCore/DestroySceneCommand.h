#ifndef DestorySceneCommand_
#define DestorySceneCommand_
#include "CommandBase.h"
#include "SceneAttorney.h"

class DestroySceneCommand : public CommandBase {
public:
	explicit DestroySceneCommand(Scene&);
	~DestroySceneCommand();

	void Execute() override;
private:
	Scene* mpScene;

};
#endif