#include "ShaderManager.h"
#include "DebugOut.h"
#include <string.h>

// statics 
ShaderManager* ShaderManager::instance = nullptr;

// interface
void ShaderManager::LoadShader(std::string key_, std::string fileName_) {
	GetInstance().PrivateLoadShader(key_, fileName_);
}

ShaderObject* ShaderManager::GetShader(std::string key_) {
	return GetInstance().PrivateGetShader(key_);
}

void ShaderManager::PrivateLoadShader(std::string key_, std::string fileName_) {
	if(!ShaderInMap(key_)) {
		// Default File Path
		std::string relativePath = rootPath + fileName_;

		// Load specified Shader
		ShaderObject *pShader = new ShaderObject(relativePath.c_str());

		// insert into the ShaderMap
		shaderMap.insert({ key_, pShader });
	}
}

ShaderObject* ShaderManager::PrivateGetShader(std::string key_) {
	auto search = shaderMap.find(key_);
	return search->second;
}

bool ShaderManager::ShaderInMap(std::string key_) {
	auto search = shaderMap.find(key_);

	if(search == shaderMap.end()) {
		return false;
	} else {
		return true;
	}
}

void ShaderManager::CleanUp() {
	GetInstance().PrivateCleanUp();
}

void ShaderManager::PrivateCleanUp() {
	for(auto it = shaderMap.begin(); it != shaderMap.end(); it++) {
		delete it->second;
	}
	shaderMap.clear();
}