#ifndef CollidableGroupCompute_
#define CollidableGroupCompute_

#include "CommandBase.h"
#include "Visualizer.h"
#include "../Resources/EngineSettings.h"

template<class CollisionType>
class CollidableGroupComputeCommand : public CommandBase {
public:
	void Execute() {
		CollidableGroup<CollisionType>::GetInstance().ComputeAABB();
#if Visualizer_ON
		// todo: fix this line
		Visualizer::ShowAABB(CollidableGroup<CollisionType>::GetInstance().mVolumeAABB, Color::Blue);
#endif
	};

};


#endif // !CollidableGroupCompute_

