#ifndef KeyboardEventManager_
#define KeyboardEventManager_

#include "AzulCore.h"
#include "InputEventType.h"
#include "SingleKeyEventManager.h"
#include "ContainerAlias.h"
#include <map>

class Inputable;
class KeyboardEventManager {
public:
	KeyboardEventManager();
	virtual ~KeyboardEventManager();

	void Register(Inputable&, AZUL_KEY k, INPUT_EVENT_TYPE e);
	void Deregister(Inputable&, AZUL_KEY k, INPUT_EVENT_TYPE e);
	void ProcessKeyEvents();

private:
	KM::SingleKeyMap skEventManagersMap;
};
#endif // !KeyboardEventManager_
