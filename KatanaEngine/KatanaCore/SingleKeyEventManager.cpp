#include "SingleKeyEventManager.h"
#include "InputableAttorney.h"
#include "DebugOut.h"

SingleKeyEventManager::SingleKeyEventManager(AZUL_KEY key_)
	: mKey(key_), lastKeyState(false), currentlyDown(false) {}

SingleKeyEventManager::~SingleKeyEventManager() {
	pressedList.clear();
	releasedList.clear();
}

void SingleKeyEventManager::Register(Inputable &inputable_, INPUT_EVENT_TYPE eventType_) {
	if(eventType_ == INPUT_EVENT_TYPE::KEY_PRESS) {
		pressedList.insert(pressedList.end(), &inputable_);
	} else if (eventType_ == INPUT_EVENT_TYPE::KEY_RELEASE) {
		releasedList.insert(releasedList.end(), &inputable_);
	}
	else if (eventType_ == INPUT_EVENT_TYPE::KEY_HELD_DOWN)
	{
		heldDownList.insert(heldDownList.end(), &inputable_);
	}
	else
	{
		heldUpList.insert(heldUpList.end(), &inputable_);
	}

}

void SingleKeyEventManager::Deregister(Inputable &inputable_, INPUT_EVENT_TYPE eventType_) {
	//temp
	if(eventType_ == INPUT_EVENT_TYPE::KEY_PRESS) {
		pressedList.remove(&inputable_);
	} else if(eventType_ == INPUT_EVENT_TYPE::KEY_RELEASE) {
		releasedList.remove(&inputable_);
	}
	else if (eventType_ == INPUT_EVENT_TYPE::KEY_HELD_DOWN)
	{
		heldDownList.remove(&inputable_); 
	}
	else if (eventType_ == INPUT_EVENT_TYPE::KEY_HELD_UP)
	{
		heldUpList.remove(&inputable_);
	}
}

void SingleKeyEventManager::ProcessKeyEvent() {
	// check for mKey change and if so was the change keyup or keydown
	if(Keyboard::GetKeyState(mKey) != lastKeyState && !currentlyDown) {
		ProcessPressedList();
		currentlyDown = true;
	} else if(Keyboard::GetKeyState(mKey) != lastKeyState && currentlyDown) {
		ProcessReleasedList();
		currentlyDown = false;
	}
	else if (Keyboard::GetKeyState(mKey) == lastKeyState && currentlyDown)
	{
		ProcessHeldDownList(); 
	}
	else if (Keyboard::GetKeyState(mKey) == lastKeyState && !currentlyDown)
	{
		ProcessHeldUpList();
	}
	// save mKey state
	lastKeyState = Keyboard::GetKeyState(mKey);
}

bool SingleKeyEventManager::IsEmpty() {
	if((pressedList.size() + releasedList.size() + heldDownList.size() + heldUpList.size()) <= 0){
		return true;
	}
	return false;
}

void SingleKeyEventManager::ProcessPressedList() {
	for(auto it = pressedList.begin(); it != pressedList.end(); it++) {
		InputableAttorney::KeyEvent::KeyPressed(**it, mKey);
	}
}

void SingleKeyEventManager::ProcessReleasedList() {
	for(auto it = releasedList.begin(); it != releasedList.end(); it++) {
		InputableAttorney::KeyEvent::KeyReleased(**it, mKey);
	}
}

void SingleKeyEventManager::ProcessHeldDownList()
{
	for (Inputable* pInputable : heldDownList)
	{
		InputableAttorney::KeyEvent::KeyHeldDown(*pInputable, mKey);
	}
}

void SingleKeyEventManager::ProcessHeldUpList()
{
}
