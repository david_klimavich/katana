#ifndef AlarmDeregistrationCommand_
#define AlarmDeregistrationCommand_

#include "CommandBase.h"
#include "SceneManager.h"
#include "Scene.h"
#include "AlarmableAttorney.h"
#include "DebugOut.h"

class AlarmDeregistrationCommand : public CommandBase {

public:

	AlarmDeregistrationCommand(Alarmable* alarmable_, AlarmableManager::ALARM_ID alarmID_)
		: mpAlarmable(alarmable_), mAlarmID(alarmID_) {}


	void Execute() override {
		AlarmableAttorney::Registration::SceneDeregistration(*mpAlarmable, mAlarmID);
	}

	// maybe?
	void SetTimer(float time_) {
		time = time_;
	}

private:

	Alarmable* mpAlarmable;
	AlarmableManager::ALARM_ID mAlarmID;
	float time = 0.0f;
};
#endif // !AlarmDeregistrationCommand_
