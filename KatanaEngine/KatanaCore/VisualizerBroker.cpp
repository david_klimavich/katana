#include "VisualizerBroker.h"
#include "CommandBase.h"

void ViualizerBroker::AddCommand(CommandBase *commandBase_) {
 	commandList.push_back(commandBase_);
}


void ViualizerBroker::ProcessElements() {
	/*for(auto it = commandList.begin(); it != commandList.end(); it++)
	{
		(*it)->Execute();
	}*/

	auto it = commandList.begin();
	while(it != commandList.end())
	{
		(*it)->Execute();
		auto tmp = (it);
		it++;
		commandList.erase(tmp);
	}

	commandList.clear();
}

