#ifndef CollisionDeregistrationCommand_
#define CollisionDeregistrationCommand_

#include "SceneRegistrationCommand.h"

template <class CollidableType>
class CollisionDeregistrationCommand : public SceneRegistrationCommand {
public:
	explicit CollisionDeregistrationCommand(CollidableType& collideable_) : mpCollidable(&collideable_) {};
	~CollisionDeregistrationCommand() = default;

	void Execute() override {
		CollidableGroup<CollidableType>::Deregister(*mpCollidable);
	};

private:
	CollidableType * mpCollidable;
};

#endif
