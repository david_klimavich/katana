#ifndef Drawable_
#define Drawable_

#include "ContainerAlias.h"
#include "RegistrationState.h"

class DrawRegistrationCommand;
class DrawDeregistrationCommand;

class Drawable {
	friend class DrawableAttorney;

public:
	Drawable();
	Drawable(const Drawable&);
	Drawable& operator=(const Drawable&);
	virtual ~Drawable();

	// set these up with attorney later
	KLI::DrawableListIt& GetIt();
	void SetIt(KLI::DrawableListIt&);

protected:
	void SubmitRegister();
	void SubmitDeregister();

private:
	virtual void Draw() = 0;

	void SceneRegistration();
	void SceneDeregistration();

	REGISTRATION_STATE REG_STATE;
	DrawRegistrationCommand* registrationCmd;
	DrawDeregistrationCommand* deregistrationCmd;
	KLI::DrawableListIt it;

};

#endif