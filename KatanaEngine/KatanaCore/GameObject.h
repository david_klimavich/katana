///-------------------------------------------------------------------------------------------------
/// @file KatanaCore\GameObject.h.
/// Declares the game object class.
///-------------------------------------------------------------------------------------------------
#ifndef GameObject_
#define GameObject_

#include "Drawable.h"
#include "Updateable.h"
#include "Alarmable.h"
#include "Inputable.h"
#include "Collidable.h"

/// A game object.
class GameObject : public Drawable, public Updateable, public Alarmable, public Inputable, public Collidable, public Align16 {
public:

	GameObject();
	virtual ~GameObject();

	void SubmitEntry();
	void SubmitExit();
	void MarkForDestroy(); 

private:
	friend class GameObjectExitCommand;
	friend class GameObjectEntryCommand;
	friend class DestroyObjectCommand;

	virtual void SceneEntry() = 0;
	virtual void SceneExit() = 0;

	GameObjectEntryCommand* mpEntryCommand;
	GameObjectExitCommand* mpExitCommand;
	DestroyObjectCommand* mpDestroyObjectCommand; 
};
#endif