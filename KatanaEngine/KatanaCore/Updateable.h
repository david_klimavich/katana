#ifndef Updateable_
#define Updateable_

#include "UpdateableManager.h"
#include "ContainerAlias.h"
#include "RegistrationState.h"

class UpdateDeregistrationCommand;
class UpdateRegistrationCommand;

class Updateable {
	friend class UpdateableAttorney;

public:
	Updateable();
	virtual ~Updateable();

	REGISTRATION_STATE GET_REG_STATE();
	void SetIt(KLI::UpdateableListIt&);
	KLI::UpdateableListIt& GetIt();

protected:

	///-------------------------------------------------------------------------------------------------
	/// @fn	void Updateable::SubmitRegister();
	///
	/// @brief	Submit GameObject to be registered for updating
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void SubmitRegister();

	///-------------------------------------------------------------------------------------------------
	/// @fn	void Updateable::SubmitDeregister();
	///
	/// @brief	Submit GameObject to be deregistered for updating
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void SubmitDeregister();

private:
	virtual void Update() = 0;

	///-------------------------------------------------------------------------------------------------
	/// @fn	void Updateable::SceneRegistration();
	///
	/// @brief	Registers updateable to scene allowing derived classes to use Update() each frame
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void SceneRegistration();

	///-------------------------------------------------------------------------------------------------
	/// @fn	void Updateable::SceneDeregistration();
	///
	/// @brief	Deregisters updateable from scene making Update() not be called on the derived object. 
	///
	/// @author	David Desktop
	/// @date	2/19/2018
	///-------------------------------------------------------------------------------------------------

	void SceneDeregistration();

	REGISTRATION_STATE REG_STATE;
	UpdateRegistrationCommand* registrationCmd;
	UpdateDeregistrationCommand* deregistrationCmd;
	KLI::UpdateableListIt it;

};

#endif