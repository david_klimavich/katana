#ifndef RegistrationState_
#define RegistrationState_

enum class REGISTRATION_STATE {
	CURRENTLY_REGISTERED,
	CURRENTLY_DEREGISTERED,
	PENDING_REGISTRATION,
	PENDING_DEREGISTRATION
};
#endif // !RegistrationState_
