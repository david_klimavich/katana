#ifndef CollisionVolumeOBB_
#define CollisionVolumeOBB_

#include "CollisionVolumeBB.h"
#include "Vect.h"
#include "Matrix.h"

class CollisionVolumeOBB : public CollisionVolumeBB, public Align16 {
public:
	CollisionVolumeOBB();
	~CollisionVolumeOBB();

	const Vect& getMin() const {
		return min;
	}
	const Vect& getMax() const {
		return max;
	}
	const Matrix& getWorld() const override {
		return *mpWorld;
	}
	const Vect& getHalfDiagonal() const override {
		return mHalfDiagonal;
	}
	const Vect& getCenter() const override {
		return mCenter;
	}
	float getScaleSquared() const override {
		return mScaleSquared;
	}
	const Matrix& getInv() const override {
		return mWorldInv;
	}

	void ComputeData(Model* model_,const Matrix& matrix_);
	void DebugView(const Vect& color_) const override;
	bool IntersectAccept(const CollisionVolume& other_) const override;
	bool IntersectVisit(const CollisionVolumeBSphere& other) const override;
	bool IntersectVisit(const CollisionVolumeAABB& other_) const override;
	bool IntersectVisit(const CollisionVolumeOBB& other_) const override;

private:
	Vect min;
	Vect max;
	Vect pos;

	Matrix* mpWorld;
	Matrix  mWorldInv;

	Vect mHalfDiagonal;
	Vect mCenter;

	float mScaleSquared;
};
#endif // !CollisionVolumeOBB_
