
#include "Game.h"

int CALLBACK WinMain(HINSTANCE , HINSTANCE ,  LPSTR , int)                  
{          
	// create Game
	Game *pGame = new Game();    
	assert(pGame);

	// start the game
	pGame->run();                                  

	return 0;                                       
}

