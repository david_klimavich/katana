#ifndef LoadAllResources_
#define LoadAllResources_

#include "../KatanaCore/KatanaEngine.h"
#include "../KatanaCore/TextureManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/SpriteManager.h"
#include "../KatanaCore/SceneManager.h"


#include "../User Code/Level_0.h"
#include "../User Code/MainMenu.h"


void KatanaEngine::InitGameWindow() {
	// Game Window Device setup
	this->setWindowName("Katana Engine");
	this->setWidthHeight(1920, 1080);
	this->SetClearColor(0.4f, 0.4f, 0.8f, 1.0f);
}
void KatanaEngine::LoadAllResources() {
	// Textures
	TextureManager::LoadTexture("frigate_tex", "space_frigate.tga");
	TextureManager::LoadTexture("ground_tex", "groundTexture.tga"); 
	TextureManager::LoadTexture("HMTest16", "HMTest16.tga");
	TextureManager::LoadTexture("HMTest4", "HMTest4.tga");
	TextureManager::LoadTexture("HMTest1", "HMTest1.tga");
	//TextureManager::LoadTexture("test_map", "test_map.tga");
	TextureManager::LoadTexture("tank_track_tex", "track.tga");
	TextureManager::LoadTexture("tank_body_tex", "body.tga");

	// Sprite Textures 
	TextureManager::LoadTexture("stitch_tex", "stitch.tga");
	TextureManager::LoadTexture("tank_hud", "tank_hud.tga");
	TextureManager::LoadTexture("heart_tex", "heart.tga");

	TextureManager::LoadTexture("StartScreen_tex", "StartScreen.tga");
	TextureManager::LoadTexture("StartGame_tex", "StartGame.tga");
	TextureManager::LoadTexture("StartGameSelected_tex", "StartGameSelected.tga");
	TextureManager::LoadTexture("QuitGame_tex", "QuitGame.tga");
	TextureManager::LoadTexture("QuitGameSelected_tex", "QuitGameSelected.tga");
	
	TextureManager::LoadTexture("Options_tex", "Options.tga");
	TextureManager::LoadTexture("OptionsSelected_tex", "OptionsSelected.tga");

	TextureManager::LoadTexture("tower_tex", "tower.tga");
	TextureManager::LoadTexture("farmhouse_tex", "farmhouse.tga");


	// Shaders
	ShaderManager::LoadShader("flat_shader", "textureFlatRender");
	ShaderManager::LoadShader("LightRender_shader", "textureLightRender");
	ShaderManager::LoadShader("colorConstantRender_shader", "colorConstantRender");
	ShaderManager::LoadShader("colorNoTextureRender_shader", "colorNoTextureRender");
	ShaderManager::LoadShader("spriteRender_shader", "spriteRender");

	// Models
	ModelManager::LoadModel("axis_model", "Axis.azul");
	ModelManager::LoadModel("plane_model", "Plane.azul");
	ModelManager::LoadModel("space_frigate_model", "space_frigate.azul");
	ModelManager::LoadModel("tank_turret_model", "t99turret.azul");
	ModelManager::LoadModel("tank_body_model", "t99body.azul");
	ModelManager::LoadModel("watchtower_model", "wooden watch tower2_61.azul");
	ModelManager::LoadModel("farmhouse_model", "farmhouse.azul");
	//ModelManager::LoadModel("Axis", Model::PreMadedeModels::UnitSphere);
}

void KatanaEngine::SetStartingScene() {
	//Level_0* scene = new Level_0;
	//SceneManager::TriggerSceneChange(scene);

	MainMenu* mainMenu = new MainMenu; 
	SceneManager::TriggerSceneChange(mainMenu);
}

#endif // !LoadAllResources_