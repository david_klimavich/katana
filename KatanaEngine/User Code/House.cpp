#include "House.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/TextureManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/CameraManager.h"

House::House(const Vect& pos_, float angle_) : mHousePos(pos_)
{
	mpGObj_House = new GraphicsObject_TextureFlat(ModelManager::GetModel("farmhouse_model"), ShaderManager::GetShader("flat_shader"),
		TextureManager::GetTexture("farmhouse_tex"));

	mHouseScale.set(SCALE, 1.5, 1.5, 1.5);
	mHouseRot.set(IDENTITY);
	mHouseRot *= Matrix(ROT_Y, angle_);
	//mHousePos.set(0, 0, 0);

	mHouseWorld = mHouseScale * mHouseRot * Matrix(TRANS, mHousePos);
	mpGObj_House->SetWorld(mHouseWorld);
}

House::~House()
{
}

void House::Draw()
{
	mpGObj_House->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

void House::SceneEntry()
{
	Drawable::SubmitRegister();
}

void House::SceneExit()
{
	Drawable::SubmitDeregister(); 
}
