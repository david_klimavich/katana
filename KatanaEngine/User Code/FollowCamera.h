#ifndef FollowCamera_
#define FollowCamera_

#include "Matrix.h"
#include "Camera.h"

class FollowCamera {
public:
	FollowCamera();
	~FollowCamera();

	void UpdateCamera(const Vect& objectPos_, const Matrix& objectRot_);
	Camera& GetCamera();

	// TankPos, TankRot, 
private:
	Camera * mpCamera;

	Matrix mRotPos;
	Vect mCamPos;
	Vect mLookAt;
	Vect mCamOffset;
};

#endif