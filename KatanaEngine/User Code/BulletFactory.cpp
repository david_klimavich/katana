#include "BulletFactory.h"
#include "Bullet.h"
#include "DebugOut.h"

BulletFactory* BulletFactory::instance = nullptr;
BulletFactory::BulletFactory()
	: bulletCount(100) {
	for (unsigned int i = 0; i < 100; ++i)
	{
		Bullet* pBullet = new Bullet; 
		bulletStack.push(pBullet); 
	}

}

void BulletFactory::CreateBullet(const Vect &pos_, const Matrix& rot_, Collidable& owner_) {
	GetInstance().PrivateCreateBullet(pos_, rot_, owner_);
}

void BulletFactory::RecycleBullet(Bullet &bullet_) {
	GetInstance().PrivateRecycleBullet(bullet_);
}

void BulletFactory::PrivateCreateBullet(const Vect &pos_, const Matrix& rot_, Collidable& owner_) {
	if(!bulletStack.empty())
	{
		bulletStack.top()->SubmitEntry(); // submit register 
		bulletStack.top()->Init(pos_, rot_, owner_);
		bulletStack.pop();
	}
	else
	{
		Bullet* newBullet = new Bullet;
		newBullet->SubmitEntry();
		newBullet->Init(pos_, rot_, owner_);
	}
}

void BulletFactory::PrivateRecycleBullet(Bullet &bullet_) {
	bulletStack.push(&bullet_);
}
