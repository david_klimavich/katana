#ifndef TesterObject_
#define TesterObject_

#include "../KatanaCore/GameObject.h"

class TesterObject : public GameObject {
public:
	TesterObject();

	void Update() override;
	void Draw() override;

private:
	void Alarm0() override {};
	void Alarm1() override {};
	void Alarm2() override {};

	void KeyPressed(AZUL_KEY) override {};
	void KeyReleased(AZUL_KEY) override {};

	void SceneEntry() override {};
	void SceneExit() override {};

};
#endif // !TesterObject_
