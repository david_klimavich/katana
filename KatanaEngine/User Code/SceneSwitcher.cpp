#include "SceneSwitcher.h"
#include "../KatanaCore/Scene.h"
#include "../KatanaCore/SceneManager.h"
#include "Level_0.h"
#include "../KatanaCore/CameraManager.h"


SceneSwitcher::SceneSwitcher() : mpScene(new Level_0) {}

SceneSwitcher::SceneSwitcher(Scene& scene_) : mpScene(&scene_) {}

SceneSwitcher::~SceneSwitcher() {

}

void SceneSwitcher::Update() {}

void SceneSwitcher::SceneEntry() {
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_1, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_G, INPUT_EVENT_TYPE::KEY_PRESS);
}

void SceneSwitcher::SceneExit() {
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_1, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_G, INPUT_EVENT_TYPE::KEY_PRESS);
}

void SceneSwitcher::KeyPressed(AZUL_KEY key_) {
	if(key_ == AZUL_KEY::KEY_1) {
		SceneManager::TriggerSceneChange(mpScene);
	}
	if(key_ == AZUL_KEY::KEY_G) {
		SceneManager::GetActiveScene()->GetCameraManager().ToggleGodCam();
	}
}
