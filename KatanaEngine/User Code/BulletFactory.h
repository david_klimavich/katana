#ifndef BulletFactory_
#define BulletFactory_

#include "../KatanaCore/GameObject.h"
#include <stack>

class Bullet;
class BulletFactory {
public:

	static void CreateBullet(const Vect&, const Matrix&, Collidable& owner_);
	static void RecycleBullet(Bullet&);

private:
	void PrivateCreateBullet(const Vect&, const Matrix&, Collidable& owner_);
	void PrivateRecycleBullet(Bullet&);

	BulletFactory();
	BulletFactory(const BulletFactory&) = delete;
	BulletFactory operator=(const BulletFactory&) = delete;
	~BulletFactory() = default;

	static BulletFactory* instance;
	static BulletFactory& GetInstance() {
		if(!instance)
		{
			instance = new BulletFactory;
		}
		return *instance;
	}

	// members
	std::stack<Bullet*> bulletStack;
	int bulletCount;
};

#endif // !BulletFactory_
