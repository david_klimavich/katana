#ifndef House_
#define House_

#include "../KatanaCore/GameObject.h"

class House : public GameObject
{
public:
	House(const Vect& pos_, float angle_);
	~House();

private:
	void Draw() override;
	void SceneEntry() override final;
	void SceneExit() override final;

	// NOT USED ===============================================
	void Collision(Collidable&) override {};
	void Update() override {};

	void Alarm0() override final {};
	void Alarm1() override final {};
	void Alarm2() override final {};


	void KeyPressed(AZUL_KEY) override final {};
	void KeyReleased(AZUL_KEY) override final {};
	void KeyHeldDown(AZUL_KEY) override final {};
	void KeyHeldUp(AZUL_KEY) override final {};
	// NOT USED ===============================================


	// tower transform 
	Matrix mHouseWorld;
	Matrix mHouseScale;
	Vect mHousePos;
	Matrix mHouseRot;

	GraphicsObject_TextureFlat* mpGObj_House;
};

#endif // !House_
