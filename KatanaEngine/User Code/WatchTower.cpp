#include "WatchTower.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/TextureManager.h"
#include "../KatanaCore/CameraManager.h"

WatchTower::WatchTower(const Vect& pos_) :mTowerPos(pos_)
{
	Vect LightColor(1.50f, 1.50f, 1.50f, 1.0f);
	Vect LightPos(1.0f, 1.0f, 1.0f, 1.0f);

	mpGObj_Tower = new GraphicsObject_TextureFlat(ModelManager::GetModel("watchtower_model"), ShaderManager::GetShader("flat_shader"),
		TextureManager::GetTexture("tower_tex"));

	mTowerScale.set(SCALE, 10, 10, 10);
	mTowerRot.set(IDENTITY);
	mTowerRot *= Matrix(ROT_X, -1.57f);
	//mTowerPos.set(0, 0, 0);

	mTowerWorld = mTowerScale * mTowerRot * Matrix(TRANS, mTowerPos);
	mpGObj_Tower->SetWorld(mTowerWorld);
}

WatchTower::~WatchTower()
{
}

void WatchTower::Draw()
{
	mpGObj_Tower->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

void WatchTower::SceneEntry()
{
	Drawable::SubmitRegister();
}

void WatchTower::SceneExit()
{
	Drawable::SubmitDeregister();
}

