#include "StartGameButton.h"
#include "../KatanaCore/Sprite.h"
#include "../KatanaCore/KatanaEngine.h"

StartGameButton::StartGameButton(Scene * scene_, float x_, float y_) : 
	ButtonBase(x_,y_), mpScene(scene_)
{
	mpStartGame = new Sprite("StartGame_tex");
	mpStartGame->SetPositionByFactor(xPos, yPos);
	mpStartGame->SetScale(1.0f, 1.0f);

	mpStartGameSelected = new Sprite("StartGameSelected_tex");
	mpStartGameSelected->SetPositionByFactor(xPos, yPos);
	mpStartGameSelected->SetScale(1.0f, 1.0f);

}

StartGameButton::~StartGameButton()
{
	delete mpStartGame; 
	delete mpStartGameSelected;
}

void StartGameButton::Init()
{
	mpStartGame->SubmitRegister();
}

void StartGameButton::Select()
{
	if (mpStartGame->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpStartGame->SubmitDeregister(); 

	if (mpStartGameSelected->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpStartGameSelected->SubmitRegister(); 
}

void StartGameButton::Deselect()
{
	if (mpStartGameSelected->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpStartGameSelected->SubmitDeregister(); 
	
	if (mpStartGame->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpStartGame->SubmitRegister(); 
}
