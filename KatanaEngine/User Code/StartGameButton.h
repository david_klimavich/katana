#ifndef StartGameButton_
#define StartGameButton_

#include "ButtonBase.h"
#include "../KatanaCore/SceneManager.h"

class Sprite;
class StartGameButton : public ButtonBase
{
public:
	StartGameButton(Scene* scene_, float x_, float y_);
	~StartGameButton(); 

	void Init() override final;
	void Execute() override final {
		SceneManager::TriggerSceneChange(mpScene);
	}
	void Select() override final;
	void Deselect() override final; 

private:
	Scene* mpScene; 

	Sprite* mpStartGame;
	Sprite* mpStartGameSelected;
};


#endif