#include "TankPatrolState.h"
#include "Tank.h"

void TankPatrolState::Run(Tank &tank_) {
	//if(tank_.mTankPos.)
	tank_.mTankPos += Vect(0, 0, 1) * tank_.mTankRot * tank_.mTankSpeed;
}

void TankPatrolState::Alarm_0(Tank& tank_) {
	// turn 90 degrees
	tank_.mTankRot *= Matrix(ROT_Y, .5f);
}
