#ifndef TankDefaultState_
#define TankDefaultState_

#include "TankStateBase.h"

class TankDefaultState : public TankStateBase {
public:
	TankDefaultState();
	~TankDefaultState();

	void Run(Tank&) override;
	void Alarm_0(Tank&) override {};

private:

};

#endif