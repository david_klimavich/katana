#ifndef TankPatrolState_
#define TankPatrolState_
#include "TankStateBase.h"

class TankPatrolState : public TankStateBase {
	void Run(Tank&) override;
	void Alarm_0(Tank&) override;
};
#endif // !TankPatrolState_
