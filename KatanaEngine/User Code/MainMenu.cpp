#include "MainMenu.h"
#include "../KatanaCore/SceneManager.h"
#include "../KatanaCore/Sprite.h"
#include "Level_0.h"
#include "../KatanaCore/KatanaEngine.h"
#include "QuitGameButton.h"
#include "StartGameButton.h"
#include "MainMenuOptions.h"
#include "ChangeSceneButton.h"

MainMenu::MainMenu()
{
	mpBackground = new Sprite("StartScreen_tex");
	mpBackground->SetPositionByFactor(KatanaEngine::GetWindowWidth() / 2.f, (KatanaEngine::GetWindowHeight() / 2.f));
	mpBackground->SetScale(1.0f, 1.0f);


	float xPos = (KatanaEngine::GetWindowWidth() / 1.3f);
	float yPos = (KatanaEngine::GetWindowWidth() / 2.25f);
	
	// start button
	mpStartGameScene = new Level_0;
	mpStartButton = new StartGameButton(mpStartGameScene, xPos, yPos);

	// options button
	yPos -= 100.f;
	mpOptionsButton = new ChangeSceneButton(new MainMenuOptions, xPos, yPos);

	// quit button  
	yPos -= 100.f;
	mpQuitButton = new QuitGameButton(xPos, yPos);

	// Register Buttons in list
	mButtonList.AddNode(mpStartButton);
	mButtonList.AddNode(mpOptionsButton);
	mButtonList.AddNode(mpQuitButton); 

	// player controller 
	mpPlayerController = new PlayerController;
}

MainMenu::~MainMenu()
{
	delete mpPlayerController; 
	delete mpBackground; 
	delete mpStartButton; 
	delete mpOptionsButton; 
	delete mpQuitButton; 
}

void MainMenu::CleanUp()
{
	mpPlayerController->GameObject::SubmitExit();
}

void MainMenu::Init()
{
	// sprites 
	mpBackground->SubmitRegister();

	// init all buttons
	//mpStartButton->Init();
	//mpOptionsButton->Init(); 
	//mpQuitButton->Init();

	mpStartButton->Deselect(); 
	mpOptionsButton->Deselect(); 
	mpQuitButton->Deselect(); 



	mpSelectedButton = mpStartButton;
	mpSelectedButton->Select(); 

	mpPlayerController->SetControllableObject(this);
	mpPlayerController->GameObject::SubmitEntry(); 
}

void MainMenu::ForwardKeyPress(AZUL_KEY key_)
{
	switch (key_) {
	case AZUL_KEY::KEY_ARROW_DOWN:
		mpSelectedButton->Deselect(); 
		mpSelectedButton = mpSelectedButton->mpNext; 
		mpSelectedButton->Select(); 
		break;
	case AZUL_KEY::KEY_ARROW_UP:
		mpSelectedButton->Deselect(); 
		mpSelectedButton = mpSelectedButton->mpPrev;
		mpSelectedButton->Select(); 
		break;
	case AZUL_KEY::KEY_ENTER:
		mpSelectedButton->Execute(); 
		break; 
	}
}

void MainMenu::ForwardKeyRelease(AZUL_KEY /*key_*/)
{

}


