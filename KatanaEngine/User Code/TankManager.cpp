#include "TankManager.h"
#include "Vect.h"
#include "Tank.h"
#include "TankAIController.h"
#include "TankPlayerController.h"
#include "../KatanaCore/Katana_Math.h"

TankManager* TankManager::instance = nullptr; 

TankManager::TankManager() : mPlayerHealth(500.0f), 
mEnemyHealth(100.0f),
mPlayerSpeed(.50f), 
mEnemySpeed(0.0f),
mpPlayerTank(nullptr)
{}

TankManager::~TankManager()
{
	// iterate over tank list and delete all tanks remaining
	for (Tank* pTank : tankList)
	{
		delete pTank;
	}
}

Tank* TankManager::PrivateSpawnPlayerTank(const Vect & pos_)
{
	// Create player tank
	mpTankPlayerController = new TankPlayerController;
	mpPlayerTank = mTankFactory.SpawnTankAtPos(pos_, mPlayerHealth, mPlayerSpeed, *mpTankPlayerController);
	tankList.push_back(mpPlayerTank);
	return mpPlayerTank;
}

void TankManager::PrivateSpawnEnemyTanks(unsigned int numTanks_, const Vect& startRange_, const Vect& endRange_)
{
	// create AI controller
	assert(mpPlayerTank); 
	mpTankAIController = new TankAIController(*mpPlayerTank);

	// Create enemy tanks
	for (unsigned int i = 0; i < numTanks_; ++i)
	{
		// calculate position
		Vect pos; 
		CalculateFreePosition(pos, startRange_, endRange_); 

		// spawn tank
		Tank* pTank = mTankFactory.SpawnTankAtPos(pos, mEnemyHealth, mEnemySpeed, *mpTankAIController);
		tankList.push_back(pTank);
	}
}

// NOTE: if there are too many tanks in too small an area this function will never find a free position
void TankManager::CalculateFreePosition(Vect& returnPos, const Vect& startRange_, const Vect& endRange_)
{
	bool searching = true; 

	while (searching)
	{
		searching = false; 
		// generate rand pos in range of 'start' and 'end'
		returnPos = Vect(rand() % static_cast<int>(endRange_.X()) + startRange_.X(),
			0, rand() % static_cast<int>(endRange_.Z()) + startRange_.Z());

		for (Tank* pTank : tankList)
		{
			// test distance against all other exsisting tanks
			if (Katana_Math::GetDistanceSquared(returnPos, pTank->GetPosition()) < (50.0f) * (50.0f))
			{
				// another tank is too close that position - recalculate
				searching = true; 
				break;
			}
		}
	}

}

void TankManager::PrivateCleanUp()
{
	// iterate over tank list and SubmitExit all tanks remaining
	for (Tank* pTank : tankList)
	{
		pTank->GameObject::SubmitExit();
	}
}

void TankManager::PrivateSubmitExit(Tank & tank_)
{
	mTankFactory.RecycleTank(tank_);
	tankList.remove(&tank_); 
}

