#ifndef SceneSwitcher_
#define SceneSwitcher_

#include "../KatanaCore/GameObject.h"
class Scene;
class SceneSwitcher : public GameObject {
public:
	SceneSwitcher();
	SceneSwitcher(Scene&);
	~SceneSwitcher();

private:
	void Update() override;
	void Draw() override {};

	void Alarm0() override {};
	void Alarm1() override {};
	void Alarm2() override {};

	void Collision(Collidable&) override {};

	void SceneEntry() override;
	void SceneExit() override;
	void KeyPressed(AZUL_KEY) override;
	void KeyReleased(AZUL_KEY) override {};

	Scene* mpScene;
};

#endif // !SceneSwitcher_
