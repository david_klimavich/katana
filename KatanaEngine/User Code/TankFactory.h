#ifndef TankSpawner_
#define TankFactory_

#include "TankAIController.h"
#include "../KatanaCore/Collidable.h"
#include <stack>

class Vect; 
class Tank;
class TankFactory {
public:
	TankFactory();
	TankFactory(const TankFactory&) = default;
	TankFactory& operator=(const TankFactory&) = default;
	~TankFactory();

	// Spawn tank using a vect as cooridinates
	Tank* SpawnTankAtPos(const Vect& pos_, float tankHealth_, float speed_, TankInputControllerBase& tankController_);
	void RecycleTank(Tank& tank_); 

	
private:

	std::stack<Tank*> tankStack;
	Collidable::CollisionVolumeType mTankCollisionVolume;
};
#endif // !TankFactory_
