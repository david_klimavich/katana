#ifndef TankPlayerController_
#define TankPlayerController_
#include "TankInputControllerBase.h"

class Tank;
class TankPlayerController : public TankInputControllerBase {
public:
	TankPlayerController() = default;
	~TankPlayerController() = default;

	void SceneEntry(Tank&) override;
	void Update(Tank&) override;
	void Alarm_0(Tank&) override {};
	void Alarm_1(Tank&) override {};
	void KeyPressed(AZUL_KEY, Tank&) override {};
};
#endif // !TankPlayerController_
