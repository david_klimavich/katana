#include "Level_0.h"
#include "../KatanaCore/KatanaEngine.h"
#include "../KatanaCore/CollisionManager.h"
#include "../KatanaCore/Terrain.h"
#include "../KatanaCore/Sprite.h"
#include "PlayerController.h"
#include "Bullet.h"
#include "Tank.h"
#include "TankManager.h"
#include "TankManagerAttorney.h"
#include "TankHUD.h"
#include "WatchTower.h"
#include "House.h"

Level_0::Level_0() {

}

Level_0::~Level_0() {
	TankManagerAttorney::DeleteTankManager();
	delete mpPlayerController;
	delete mpTankHUD; 
	delete mpTerrain; 

	for (auto it = EnviromentObjList.begin(); it != EnviromentObjList.end(); it++)
	{
		delete *it; 
	}
}

void Level_0::Init() {
	// create a new playercontroller 
	mpPlayerController = new PlayerController; 

	// Spawn player
	Tank* pPlayerTank = TankManager::SpawnPlayerTank(Vect(0, 0, 0));

	// Spawn enemies
	float range = 100.0f;
	Vect startRange(-range, 0, -range);
	Vect endRange(range, 0, range);
	mpPlayerController->SetControllableObject(pPlayerTank);
	mpPlayerController->GameObject::SubmitEntry(); 
	TankManager::SpawnEnemyTanks(5, startRange, endRange);

	// set up collision pairs for scene
	SetCollisionSelf<Tank>();
	SetCollisionPair<Tank, Bullet>();

	//terrain 
	TerrainSettings settings{ "Textures/HMTest4.tga", 10,0, 200, 1,1 };
	mpTerrain = new Terrain(settings);


	CreateTowers(); 
	CreateHouses(); 
	SubmitEnviromentForEntry();

	// HUD
	mpTankHUD = new TankHUD; 
}

void Level_0::CleanUp() {
	TankManager::CleanUp();
	mpTankHUD->CleanUp();
	mpPlayerController->GameObject::SubmitExit();
	SubmitEnviromentForExit(); 
}

void Level_0::CreateTowers()
{
	float space = 300.f;
	// create tower
	for (int i = 0; i < 1; i++)
	{
		float xPos = i * -space;
		for (int j = 0; j < 3; j++)
		{
			float zPos = j * -space;

			Vect pos(xPos, 0, zPos);
			WatchTower* pTower = new WatchTower(pos);
			EnviromentObjList.push_back(pTower);
		}
	}
}

void Level_0::CreateHouses()
{
	// create house
	Vect housePos(-200, 0, 50);
	float houseAngle = -1.5f;
	int numHousesPerSide = 15;

	for (int i = 0; i < numHousesPerSide; i++)
	{
		housePos = Vect(-200, 0, -50.f * i);
		House* pHouse = new House(housePos, houseAngle);
		EnviromentObjList.push_back(pHouse);
	}

	houseAngle = 1.5f;
	for (int i = 0; i < numHousesPerSide; i++)
	{
		housePos = Vect(200, 0, -50.f * i);
		House* pHouse = new House(housePos, houseAngle);
		EnviromentObjList.push_back(pHouse);
	}
}

void Level_0::SubmitEnviromentForEntry()
{
	for (GameObject* pGameObj : EnviromentObjList)
	{
		pGameObj->SubmitEntry(); 
	}
}

void Level_0::SubmitEnviromentForExit()
{
	for (GameObject* pGameObj : EnviromentObjList)
	{
		pGameObj->SubmitExit();
	}
}
