#ifndef Tank_
#define Tank_

#include "../KatanaCore/GameObject.h"
#include "../KatanaCore/CollisionVolumeBSphere.h"

#include "FollowCamera.h"
#include "TankInputControllerBase.h"
#include "ControllableObject.h"

class Vect; 
class Tank : public GameObject, public ControllableObject {
public:
	Tank(CollisionVolumeType type_);
	~Tank();

	void SetController(TankInputControllerBase& controller_) {
		mpTankController = &controller_;
	};

	void Init(const Vect& pos_, float tankHealth_, float tankSpeed_, TankInputControllerBase& tankController_);
	void Update() override;
	void Draw() override;
	void Collision(Collidable&) override;

	void TakeDamage(float damage_);

	inline const Vect& GetPosition() { return mTankPos; };

	void ForwardKeyPress(AZUL_KEY key_) override final;
	void ForwardKeyRelease(AZUL_KEY key_) override final;
	virtual void ForwardKeyHeldDown(AZUL_KEY key_) override final;
	virtual void ForwardKeyHeldUp(AZUL_KEY key_) override final; 

private:
	friend class TankPlayerController;
	friend class TankAIController;
	friend class TankPatrolState;

	void InitCam();
	void LookAt(Tank&);
	void Fire();

	void Alarm0() override final;
	void Alarm1() override final;
	void Alarm2() override final;

	void SceneEntry() override final;
	void SceneExit() override final;

	void KeyPressed(AZUL_KEY key_) override final;
	void KeyReleased(AZUL_KEY) override final {};
	void KeyHeldDown(AZUL_KEY) override final {};
	void KeyHeldUp(AZUL_KEY) override final {};

	// components 
	TankInputControllerBase* mpTankController;

	// members 
	GraphicsObject_TextureFlat *pGObj_TankBodyLight;
	GraphicsObject_TextureFlat *pGObj_TankTurretLight;
	CollisionVolumeBSphere* pColSphere;
	Collidable::CollisionVolumeType mCollisionVolumeType;

	FollowCamera mFollowCam;

	// tank transform
	Matrix mWorld;
	Matrix mTankScale;
	Vect mTankPos;
	Matrix mTankRot;

	// turret transform
	const float mTurretYOffset = 10.0f;
	Matrix mTurretWorld;
	Matrix mTurretScale;
	Vect mTurretPos;
	Matrix mTurretRot;

	// movement
	bool playerControl = false;
	float mTankSpeed = 1.0f;
	const float mTankRotAng = .035f;

	// stats
	float mHealth;
};

#endif // !Tank_
