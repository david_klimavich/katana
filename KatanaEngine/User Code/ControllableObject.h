#ifndef ControllableObject_
#define ControllableObject_

// make a engine wrapper for the AZUL Keyboard - Katana Keyboard
#include "../../AzulCore/include/Keyboard.h"
class ControllableObject
{
public:
	virtual void ForwardKeyPress(AZUL_KEY key_) = 0; 
	virtual void ForwardKeyRelease(AZUL_KEY key_) = 0; 
	virtual void ForwardKeyHeldDown(AZUL_KEY key_) = 0; 
	virtual void ForwardKeyHeldUp(AZUL_KEY key_) = 0; 

private:

};


#endif