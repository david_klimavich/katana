#include "TankFactory.h"
#include "Tank.h"
#include "TankInputControllerBase.h"

TankFactory::TankFactory() : mTankCollisionVolume(Collidable::CollisionVolumeType::AABB)
{}

TankFactory::~TankFactory()
{
	while(!tankStack.empty())
	{
		Tank* pTank = tankStack.top();
		tankStack.pop(); 
		delete pTank; 
	}
}

Tank* TankFactory::SpawnTankAtPos(const Vect & pos_, float tankHealth_, float tankSpeed_, TankInputControllerBase& tankController_)
{
	Tank* pTank;

	if (!tankStack.empty())
	{
		pTank = tankStack.top(); 
		tankStack.pop(); 

		pTank->Init(pos_, tankHealth_, tankSpeed_, tankController_);
	}
	else
	{
		pTank = new Tank(mTankCollisionVolume);
		pTank->Init(pos_, tankHealth_, tankSpeed_, tankController_); 
	}

	pTank->SubmitEntry();
	return pTank; 
}

void TankFactory::RecycleTank(Tank & tank_)
{
	tank_.GameObject::SubmitExit(); 
	tankStack.push(&tank_); 
}
