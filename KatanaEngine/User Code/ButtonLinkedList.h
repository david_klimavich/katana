#ifndef ButtonLinkedList_
#define ButtonLinkedList_

class ButtonBase; 

class ButtonLinkedList
{
public:
	ButtonLinkedList();
	~ButtonLinkedList();

	// Add a node to the list
	void AddNode(ButtonBase* pButton_);

	// Remove a node from the list
	void RemoveNode(ButtonBase* pButton_);


	ButtonBase* mpHead; 
	ButtonBase* mpTail; 

private:
	
};

#endif // !ButtonLinkedList_
