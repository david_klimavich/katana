#ifndef Bullet_
#define Bullet_
#include "../KatanaCore/GameObject.h"

class Tank; 
class Bullet : public GameObject {
public:
	// temp
	Bullet(); // maybe actually make this private and 
			  // only a bullet factory can create a bullet?
	~Bullet();

	void Collision(Collidable&) override;
private:
	// member functions
	friend class BulletFactory;
	void Init(const Vect&, const Matrix&, Collidable& Owner_);
	void DoDamage(Collidable* other_);

	// inherited functions
	void Update() override;
	void Draw() override;

	void Alarm0() override;
	void Alarm1() override {};
	void Alarm2() override {};


	void SceneEntry() override;
	void SceneExit() override;
	void KeyPressed(AZUL_KEY) override {};
	void KeyReleased(AZUL_KEY) override {};
	void KeyHeldDown(AZUL_KEY) override {};
	void KeyHeldUp(AZUL_KEY) override {};

	// members 
	GraphicsObject_WireframeConstantColor *mpGObj_Bullet;
	const float BULLETSPEED = 7.5f;
	const float BULLETSCALE = 2.0f;
	const float BULLETDAMAGE = 50.0f; 
	Matrix mWorld;
	Matrix mBulletScale;
	Vect mBulletPos;
	Matrix mBulletRot;

	// Tanks that fires the bullet
	Collidable* mpOwner; 

	bool inScene;
};

#endif
