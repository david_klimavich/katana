#include "TankHUD.h"
#include "../KatanaCore/Sprite.h"
#include "../KatanaCore/KatanaEngine.h"

TankHUD::TankHUD()
{
	// Sprites
	mpCrosshair = new Sprite("tank_hud");
	mpCrosshair->SetPositionByFactor(KatanaEngine::GetWindowWidth() / 2.f, (KatanaEngine::GetWindowHeight() / 2.f));// 100.0f);
	mpCrosshair->SetScale(.5f, .5f);
	mpCrosshair->SubmitRegister();


	// health
	float xPos = 0.f;
	float yPos = 50.f;
	for (int i = 0; i < HEALTH; ++i)
	{
		xPos += 90.f; 
		mpHealth[i] = new Sprite("heart_tex");
		mpHealth[i]->SetPositionByFactor(xPos, yPos); 
		mpHealth[i]->SetScale(.5f, .5f); 
		mpHealth[i]->SubmitRegister(); 
	}
}

TankHUD::~TankHUD()
{
	delete mpCrosshair; 

	for (int i = 0; i < HEALTH; ++i)
	{
		delete mpHealth[i];
	}
}

void TankHUD::CleanUp()
{
	mpCrosshair->SubmitDeregister(); 

	for (int i = 0; i < HEALTH; ++i)
	{
		mpHealth[i]->SubmitDeregister();
	}
}
