#ifndef TankStateBase_
#define TankStateBase_

class Tank;
class TankStateBase {
public:
	TankStateBase() = default;
	~TankStateBase() = default;

	virtual void Run(Tank&) = 0;
	virtual void Alarm_0(Tank&) = 0;

private:

};

#endif // !TankStateBase_
