#include "QuitGameButton.h"
#include "../KatanaCore/Sprite.h"
#include "../KatanaCore/KatanaEngine.h"

QuitGameButton::QuitGameButton(float x_, float y_) : ButtonBase(x_, y_)
{
	// quit game
	mpQuitGame = new Sprite("QuitGame_tex");
	mpQuitGame->SetPositionByFactor(xPos, yPos);
	mpQuitGame->SetScale(1.0f, 1.0f);

	// selected quit game
	mpQuitGameSelected = new Sprite("QuitGameSelected_tex");
	mpQuitGameSelected->SetPositionByFactor(xPos, yPos);
	mpQuitGameSelected->SetScale(1.0f, 1.0f);
}

QuitGameButton::~QuitGameButton()
{
	delete mpQuitGame; 
	delete mpQuitGameSelected;
}

void QuitGameButton::Init()
{
	mpQuitGame->SubmitRegister();
}

void QuitGameButton::Select()
{
	if(mpQuitGame->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpQuitGame->SubmitDeregister(); 
	
	if(mpQuitGameSelected->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpQuitGameSelected->SubmitRegister();
}

void QuitGameButton::Deselect()
{
	if(mpQuitGameSelected->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpQuitGameSelected->SubmitDeregister(); 

	if(mpQuitGame->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpQuitGame->SubmitRegister(); 
}
