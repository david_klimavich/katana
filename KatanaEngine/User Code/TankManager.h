#ifndef TankManager_
#define TankManager_

#include "TankFactory.h"
#include <list>

/*The TankManager is responsible for managing the tanks and encapsulating them from outside exposure*/
class TankAIController; 
class TankPlayerController;
class TankManager
{
public:
	// public interface

	static inline Tank* SpawnPlayerTank(const Vect& pos_) {
		return GetInstance().PrivateSpawnPlayerTank(pos_);
	}

	static inline void SpawnEnemyTanks(unsigned int numTanks_, const Vect& startRange_, const Vect& endRange_) {
		GetInstance().PrivateSpawnEnemyTanks(numTanks_, startRange_, endRange_);
	}
	static inline void CleanUp() { GetInstance().PrivateCleanUp(); }
	static inline void SubmitExit(Tank& tank_) { GetInstance().PrivateSubmitExit(tank_); }

	static void Delete()
	{
		delete instance;
		instance = nullptr;
	}

private:
	friend class TankManagerAttorney; 
	TankManager();
	TankManager(const TankManager&) = delete;
	TankManager operator=(const TankManager&) = delete;
	~TankManager();



	static TankManager* instance;
	static TankManager& GetInstance()
	{
		if (!instance)
		{
			instance = new TankManager; 
		}
		return *instance;  
	}

	// private interface
	Tank* PrivateSpawnPlayerTank(const Vect& pos_); 
	void PrivateSpawnEnemyTanks(unsigned int numTanks_, const Vect& startRange_, const Vect& endRange_);
	void PrivateCleanUp();
	void PrivateSubmitExit(Tank& tank_); 

	void CalculateFreePosition(Vect& returnPos, const Vect& startRange_, const Vect& endRange_); 

	float mPlayerHealth; 
	float mEnemyHealth; 
	float mPlayerSpeed;
	float mEnemySpeed; 
	TankFactory mTankFactory; 

	std::list<Tank*> tankList; 
	Tank* mpPlayerTank; 
	TankAIController* mpTankAIController; 
	TankPlayerController* mpTankPlayerController;
};


#endif