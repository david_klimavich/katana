#include "Bullet.h"
#include "DebugOut.h"
#include "BulletFactory.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/TextureManager.h"
#include "../KatanaCore/SceneManager.h"
#include "Tank.h"

Bullet::Bullet() {
	Vect Blue(0.0f, 0.0f, 1.0f, 1.0f);
	mpGObj_Bullet = new GraphicsObject_WireframeConstantColor(ModelManager::GetModel(ModelManager::UnitSphere), ShaderManager::GetShader("colorConstantRender_shader"), Blue);
	mBulletScale = Matrix(SCALE, (BULLETSCALE * Vect(1, 1, 1)));

	SetColliderModel(mpGObj_Bullet->getModel(), CollisionVolumeType::BSPHERE);
}

void Bullet::Update() {
	mBulletPos += Vect(0, 0, 1) * mBulletRot * BULLETSPEED;
	mWorld = mBulletScale * mBulletRot * Matrix(TRANS, mBulletPos);
	mpGObj_Bullet->SetWorld(mWorld);
	Collidable::UpdateCollisionData(mWorld);
}

void Bullet::Draw() {
	mpGObj_Bullet->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}


void Bullet::SceneEntry() {
	inScene = true; 
	DebugMsg::out("Bullet Fired\n");
	Alarmable::SubmitRegister(5.0f, AlarmableManager::ALARM_0);
	Updateable::SubmitRegister();
	Drawable::SubmitRegister();
	Collidable::CollisionRegistration<Bullet>(*this);
}

void Bullet::SceneExit() {
	inScene = false; 
	DebugMsg::out("Bullet leaves\n");
	Updateable::SubmitDeregister();
	Drawable::SubmitDeregister();
	Collidable::CollisionDeregistration<Bullet>(*this);
	if(Alarmable::GetAlarm0().REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED) {
		Alarmable::SubmitDeregister(AlarmableManager::ALARM_0);
	}

	BulletFactory::RecycleBullet(*this);
}

void Bullet::Init(const Vect& pos_, const Matrix& rot_, Collidable& owner_) {
	// set bullet pos);
	mBulletPos = pos_ + (Vect(0, 2.5, 25) * rot_);
	mBulletRot = rot_;
	mpOwner = &owner_; 
}

void Bullet::Alarm0() {
	// here we will submit scene exit!
	GameObject::SubmitExit();
}

Bullet::~Bullet() {
	delete mpGObj_Bullet;
}

void Bullet::Collision(Collidable &other_)
{
	if (&other_ != (this->mpOwner))
	{
		DoDamage(&other_); 
		if (inScene) // temp -> rough fix for bullet colliding with multiple objects at once
		{
			GameObject::SubmitExit();
		}
		inScene = false; 
	}
}

void Bullet::DoDamage(Collidable * other_)
{
	static_cast<Tank*>(other_)->TakeDamage(BULLETDAMAGE); 
}
