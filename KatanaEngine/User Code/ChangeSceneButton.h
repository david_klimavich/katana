#ifndef ChangeSceneButton_
#define ChangeSceneButton_

#include "ButtonBase.h"

class Scene; 
class Sprite; 
class ChangeSceneButton : public ButtonBase
{
public:
	ChangeSceneButton(Scene* pScene_, float x_, float y_);
	~ChangeSceneButton();

	void Init() override final; 
	void Execute() override final{}
	void Select() override final;
	void Deselect() override final; 

private:

	Scene* mpScene; 

	Sprite* mpButtonSprite;
	Sprite* mpButtonSelectedSprite; 
};
#endif // !ChangeSceneButton_ 
