#ifndef GameManager_
#define GameManager_

class GameManager
{
public:


private:
	GameManager();
	GameManager(const GameManager&) = delete;
	GameManager operator=(const GameManager&) = delete; 
	~GameManager() {
		instance = nullptr;
	};

	static GameManager* instance;
	static GameManager& GetInstance()
	{
		if (!instance)
		{
			instance = new GameManager();
		}
		return *instance;
	}


};

#endif