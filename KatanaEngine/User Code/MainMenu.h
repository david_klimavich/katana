#ifndef MainMenu_
#define MainMenu_

#include "../KatanaCore/Scene.h"
#include "PlayerController.h"
#include "ControllableObject.h"
#include "ButtonLinkedList.h"

class Sprite; 
class ButtonBase; 
class StartGameButton; 
class QuitGameButton; 
class ChangeSceneButton; 

class MainMenu : public Scene, public ControllableObject{
public:
	MainMenu();
	virtual ~MainMenu();
	void CleanUp() override final; 

	void ForwardKeyPress(AZUL_KEY key_) override final; 
	void ForwardKeyRelease(AZUL_KEY key_) override final;
	void ForwardKeyHeldDown(AZUL_KEY) override final {};
	void ForwardKeyHeldUp(AZUL_KEY) override final {};

private:
	// member functions
	virtual void Init() override final;

	//void MoveForward

	// members 
	PlayerController* mpPlayerController; 
	Sprite* mpBackground; 
	Scene* mpStartGameScene; 

	// buttons
	ButtonLinkedList mButtonList; 
	ButtonBase* mpSelectedButton;

	StartGameButton* mpStartButton; 
	ChangeSceneButton* mpOptionsButton;
	QuitGameButton* mpQuitButton; 
};
#endif // !MainMenu_
