#ifndef TankManagerAttorney_
#define TankManagerAttorney_
#include "TankManager.h"

class Level_0;

class TankManagerAttorney
{
private:
	friend class Level_0;
	static void DeleteTankManager() {
		TankManager::Delete(); 
	}
};

#endif