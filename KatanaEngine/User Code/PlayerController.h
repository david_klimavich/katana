#ifndef PlayerController_
#define PlayerController_

#include "../KatanaCore/GameObject.h"

class ControllableObject;
class PlayerController : public GameObject
{
public:
	PlayerController();
	virtual ~PlayerController();

	void SetControllableObject(ControllableObject* pControllableObject_);

private:
	void SceneEntry() override final;
	void SceneExit() override final; 

	void KeyPressed(AZUL_KEY) override final;
	void KeyReleased(AZUL_KEY) override final;
	void KeyHeldDown(AZUL_KEY key_) override final;
	void KeyHeldUp(AZUL_KEY key_) override final;

	
	// members 
	ControllableObject* mpControllableObject; 
	
	

	// not used in this class
	void Update() override final;
	void Draw() override final {};
	void Alarm0() override final {};
	void Alarm1() override final {};
	void Alarm2() override final {};
	void Collision(Collidable&) override final {};



};


#endif PlayerController_