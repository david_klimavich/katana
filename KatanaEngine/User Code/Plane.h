#ifndef Plane_
#define Plane_

#include "../KatanaCore/KatanaEngine.h"
#include "../KatanaCore/GameObject.h"

class Plane : public GameObject {
public:
	Plane();
	~Plane();

	void Draw() override;
	void Update() override;


private:

	void KeyPressed(AZUL_KEY) override {};
	void KeyReleased(AZUL_KEY) override {};
	void Alarm0() override {};
	void Alarm1() override {};
	void Alarm2() override {};
	void Collision(Collidable&) override {};
	void SceneEntry() override;
	void SceneExit() override;
	GraphicsObject_TextureFlat *pGObj_Plane;
	GraphicsObject_ColorNoTexture *pGObj_Axis;

	Matrix world;
};

#endif // !Plane_
