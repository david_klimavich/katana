#ifndef TankHUD_
#define TankHUD_

class Sprite;
class TankHUD
{
public:
	TankHUD();
	~TankHUD();

	void CleanUp(); 

private:
	const int HEALTH = 5; 

	Sprite* mpCrosshair;
	Sprite* mpHealth[5];

};

#endif