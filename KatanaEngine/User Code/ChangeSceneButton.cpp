#include "ChangeSceneButton.h"
#include "../KatanaCore/Sprite.h"

ChangeSceneButton::ChangeSceneButton(Scene * pScene_, float x_, float y_) : 
	ButtonBase(x_, y_), mpScene(pScene_)
{
	mpButtonSprite = new Sprite("Options_tex");
	mpButtonSprite->SetPositionByFactor(xPos, yPos);
	mpButtonSprite->SetScale(1.0f, 1.0f);

	mpButtonSelectedSprite = new Sprite("OptionsSelected_tex");
	mpButtonSelectedSprite->SetPositionByFactor(xPos, yPos);
	mpButtonSelectedSprite->SetScale(1.0f, 1.0f);

}

ChangeSceneButton::~ChangeSceneButton()
{
	delete mpButtonSprite; 
	delete mpButtonSelectedSprite; 
}

void ChangeSceneButton::Init()
{
	mpButtonSprite->SubmitRegister();
}

void ChangeSceneButton::Select()
{
	if (mpButtonSprite->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpButtonSprite->SubmitDeregister(); 

	if (mpButtonSelectedSprite->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpButtonSelectedSprite->SubmitRegister(); 
}

void ChangeSceneButton::Deselect()
{
	if (mpButtonSelectedSprite->GetRegState() == REGISTRATION_STATE::CURRENTLY_REGISTERED)
		mpButtonSelectedSprite->SubmitDeregister();

	if (mpButtonSprite->GetRegState() == REGISTRATION_STATE::CURRENTLY_DEREGISTERED)
		mpButtonSprite->SubmitRegister(); 
}
