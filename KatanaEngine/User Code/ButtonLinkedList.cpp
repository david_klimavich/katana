#include "ButtonLinkedList.h"
#include "ButtonBase.h"

ButtonLinkedList::ButtonLinkedList() : mpHead(nullptr), mpTail(nullptr)
{
}

ButtonLinkedList::~ButtonLinkedList()
{
}

void ButtonLinkedList::AddNode(ButtonBase * pButton_)
{
	// Head Case: mpHead == nullptr
	if (mpHead == nullptr)
	{
		mpHead = pButton_;
		mpTail = pButton_; 

		mpHead->mpNext = mpTail; 
	}
	else
	{
		mpTail->mpNext = pButton_; 
		pButton_->mpPrev = mpTail; 
		mpTail = pButton_; 
	}


	// connecting the tail to the head
	mpTail->mpNext = mpHead; 
	mpHead->mpPrev = mpTail; 
}
