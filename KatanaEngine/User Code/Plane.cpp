#include "Plane.h"
#include "../KatanaCore/SceneManager.h"
#include "../KatanaCore/Scene.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/TextureManager.h"


Plane::Plane() {
	pGObj_Plane = new GraphicsObject_TextureFlat(ModelManager::GetModel("plane_model"), ShaderManager::GetShader("flat_shader"),
		TextureManager::GetTexture("ground_tex"));
	pGObj_Axis = new GraphicsObject_ColorNoTexture(ModelManager::GetModel("axis_model"), ShaderManager::GetShader("colorNoTextureRender_shader"));

	world = Matrix(IDENTITY);
	pGObj_Axis->SetWorld(world);
	world = Matrix(SCALE, 1000, 1000, 1000);
	pGObj_Plane->SetWorld(world);
}

Plane::~Plane() {
	delete pGObj_Axis;
	delete pGObj_Plane;
}

void Plane::Update() {

}

void Plane::SceneEntry() {
	Drawable::SubmitRegister();
}

void Plane::SceneExit() {
	Drawable::SubmitDeregister();
}

void Plane::Draw() {
	pGObj_Plane->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
	pGObj_Axis->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
}

