#include "Tank.h"
#include "../KatanaCore/ModelManager.h"
#include "../KatanaCore/ShaderManager.h"
#include "../KatanaCore/TextureManager.h"
#include "../KatanaCore/SceneManager.h"
#include "BulletFactory.h"
#include "../KatanaCore/Visualizer.h"
#include "TankManager.h"
#include "ScoreManager.h"

Tank::Tank(CollisionVolumeType type_) :
	mCollisionVolumeType(type_)

{
	Vect LightColor(1.50f, 1.50f, 1.50f, 1.0f);
	Vect LightPos(1.0f, 1.0f, 1.0f, 1.0f);

	pGObj_TankBodyLight = new GraphicsObject_TextureFlat(ModelManager::GetModel("tank_body_model"), 
		ShaderManager::GetShader("flat_shader"), 
		TextureManager::GetTexture("tank_body_tex"), 
		TextureManager::GetTexture("tank_body_tex"), 
		TextureManager::GetTexture("tank_body_tex"));

	pGObj_TankTurretLight = new GraphicsObject_TextureFlat(ModelManager::GetModel("tank_turret_model"), 
		ShaderManager::GetShader("flat_shader"), 
		TextureManager::GetTexture("tank_body_tex"), 
		TextureManager::GetTexture("tank_track_tex"), 
		TextureManager::GetTexture("tank_body_tex"));

	SetColliderModel(pGObj_TankBodyLight->getModel(), mCollisionVolumeType);
}
Tank::~Tank() {
	delete pGObj_TankBodyLight;
	delete pGObj_TankTurretLight;
}

void Tank::Init(const Vect& pos_, float tankHealth_, float tankSpeed_, TankInputControllerBase& tankController_) {

	// Tank
	mTankScale.set(SCALE, 0.5f, 0.5f, 0.5f);
	mTankRot.set(IDENTITY);
	mTankPos = pos_;
	mWorld = mTankScale * mTankRot * Matrix(TRANS, mTankPos);
	pGObj_TankBodyLight->SetWorld(mWorld);

	
	// Turret
	Vect turretPos = mTankPos;
	turretPos += Vect(0, mTurretYOffset, 0);
	mTurretScale.set(SCALE, 0.5f, 0.5f, 0.5f);
	mTurretRot.set(IDENTITY);
	mTurretPos.set(turretPos);
	mTurretWorld = mTurretScale * mTurretRot * Matrix(TRANS, mTurretPos);
	pGObj_TankTurretLight->SetWorld(mTurretWorld);

	// Set Controller
	SetController(tankController_);

	// Set Stats
	mHealth = tankHealth_;
	mTankSpeed = tankSpeed_; 

}

void Tank::InitCam() {
	SceneManager::GetActiveScene()->GetCameraManager().SetActiveCamera(mFollowCam.GetCamera());
}

void Tank::LookAt(Tank &/*tank_*/) {

}

void Tank::Fire() {
	BulletFactory::CreateBullet(mTurretPos, mTurretRot, *this);
}

void Tank::TakeDamage(float damage_)
{
	this->mHealth -= damage_; 
	DebugMsg::out("Tank Hit\n");

	if (mHealth <= 0)
	{
		DebugMsg::out("Tank Dead\n");
		ScoreManager::TankDeath(); 
		ScoreManager::PrintScore(); 
 		TankManager::SubmitExit(*this); 
	}
}

void Tank::ForwardKeyPress(AZUL_KEY key_)
{
	switch (key_){
	case AZUL_KEY::KEY_SPACE:
		this->Fire(); 
	}
}

void Tank::ForwardKeyRelease(AZUL_KEY /*key_*/)
{

}

void Tank::ForwardKeyHeldDown(AZUL_KEY key_)
{
	switch (key_) {
	case AZUL_KEY::KEY_W:
		this->mTankPos += Vect(0, 0, 1) * mTankRot * mTankSpeed;
		break;
	case AZUL_KEY::KEY_A:
		this->mTankRot *= Matrix(ROT_Y, mTankRotAng);
		break;
	case AZUL_KEY::KEY_S:
		this->mTankPos += Vect(0, 0, 1) * mTankRot * -mTankSpeed;
		break;
	case AZUL_KEY::KEY_D:
		this->mTankRot *= Matrix(ROT_Y, -mTankRotAng);
		break;
	}

	// Turret Rotation 
	if (Keyboard::GetKeyState(AZUL_KEY::KEY_ARROW_LEFT)) {
		this->mTurretRot *= Matrix(ROT_Y, mTankRotAng);
	}

	if (Keyboard::GetKeyState(AZUL_KEY::KEY_ARROW_RIGHT)) {
		this->mTurretRot *= Matrix(ROT_Y, -mTankRotAng);
	}

	this->mFollowCam.UpdateCamera(mTurretPos, mTurretRot);
}

void Tank::ForwardKeyHeldUp(AZUL_KEY /*key_*/)
{
}

void Tank::Alarm0() {
	//DebugMsg::out("Alarm_0 Triggered\n");
	mpTankController->Alarm_0(*this);
	//Alarmable::SubmitRegister(2.0f, AlarmableManager::ALARM_0);
}

void Tank::Alarm1() {
	//DebugMsg::out("Alarm_1 Triggered\n");
	//Alarmable::SubmitRegister(5.0f, AlarmableManager::ALARM_1);
}

void Tank::Alarm2() {
	//DebugMsg::out("Alarm_2 Triggered\n");
	//Alarmable::SubmitRegister(5.0f, AlarmableManager::ALARM_2);
}

void Tank::SceneEntry() {
	mpTankController->SceneEntry(*this);

	Drawable::SubmitRegister();
	Updateable::SubmitRegister();

	Collidable::CollisionRegistration<Tank>(*this);
	Alarmable::SubmitRegister(2.0f, AlarmableManager::ALARM_0);
	Alarmable::SubmitRegister(5.0f, AlarmableManager::ALARM_1);
	Alarmable::SubmitRegister(7.0f, AlarmableManager::ALARM_2);
}

void Tank::SceneExit() {
	Drawable::SubmitDeregister();
	Updateable::SubmitDeregister();
	Collidable::CollisionDeregistration<Tank>(*this);

	if(Alarmable::GetAlarm0().REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED) {
		Alarmable::SubmitDeregister(AlarmableManager::ALARM_0);
	}
	if(Alarmable::GetAlarm1().REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED) {
		Alarmable::SubmitDeregister(AlarmableManager::ALARM_1);
	}
	if(Alarmable::GetAlarm2().REG_STATE == REGISTRATION_STATE::CURRENTLY_REGISTERED) {
		Alarmable::SubmitDeregister(AlarmableManager::ALARM_2);
	}
}

void Tank::KeyPressed(AZUL_KEY /*key_*/) {}

void Tank::Collision(Collidable &) {
	//DebugMsg::out("Collision\n");
}


void Tank::Update() {
	mpTankController->Update(*this);
	// Spaceship adjust matrix
	mWorld = mTankScale * mTankRot * Matrix(TRANS, mTankPos);
	pGObj_TankBodyLight->SetWorld(mWorld);

	mTurretPos.set(mTankPos + Vect(0, 5, 0));
	mTurretWorld = mTurretScale * mTurretRot * Matrix(TRANS, mTurretPos);
	pGObj_TankTurretLight->SetWorld(mTurretWorld);

	//Collidable::UpdateCollisionData(mWorld);
}

void Tank::Draw() {
	pGObj_TankBodyLight->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
	pGObj_TankTurretLight->Render(SceneManager::GetActiveScene()->GetCameraManager().GetActiveCamera());
	//Visualizer::ShowBSphere(GetBSphere());
}

