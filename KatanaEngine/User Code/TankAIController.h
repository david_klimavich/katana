#ifndef TankAIController_
#define TankAIController_
#include "TankInputControllerBase.h"

class Tank;
class TankStateBase;
class TankAIController : public TankInputControllerBase {
public:
	explicit TankAIController(Tank& player_);
	~TankAIController() = default;

	void SceneEntry(Tank&) override {};
	void Update(Tank&) override;
	void Alarm_0(Tank&) override;
	void Alarm_1(Tank&) override;
	void KeyPressed(AZUL_KEY, Tank&) override {};

private:
	TankStateBase * mpCurrentState;
	Tank* mpPlayer;
};
#endif // !TankPlayerController_
