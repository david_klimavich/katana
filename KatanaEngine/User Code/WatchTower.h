
#ifndef WatchTower_
#define WatchTower_

#include "../KatanaCore/GameObject.h"

class Model;
class Texture;
class WatchTower : public GameObject {
public:
	WatchTower(const Vect& pos_);
	~WatchTower();



private:
	void Draw() override;
	void SceneEntry() override final;
	void SceneExit() override final;

	// NOT USED ===============================================
	void Collision(Collidable&) override {};
	void Update() override {};

	void Alarm0() override final{};
	void Alarm1() override final{};
	void Alarm2() override final{};


	void KeyPressed(AZUL_KEY ) override final {};
	void KeyReleased(AZUL_KEY) override final {};
	void KeyHeldDown(AZUL_KEY) override final {};
	void KeyHeldUp(AZUL_KEY) override final {};
	// NOT USED ===============================================

	// tower transform 
	Matrix mTowerWorld;
	Matrix mTowerScale;
	Vect mTowerPos;
	Matrix mTowerRot;

	GraphicsObject_TextureFlat* mpGObj_Tower;
};
#endif // !WatchTower_
