#ifndef Level_0_
#define Level_0_

#include "../KatanaCore/Scene.h"
#include "TankManager.h"
#include <list>

class PlayerController; 
class Terrain;
class Tank;
class Sprite;
class TankHUD;
class WatchTower;
class House; 

class Level_0 : public Scene{

public:
	Level_0();
	~Level_0();

	void Init() override;
	void CleanUp() override;

private:
	void CreateTowers();
	void CreateHouses(); 
	void SubmitEnviromentForEntry(); 
	void SubmitEnviromentForExit(); 

	Terrain* mpTerrain;
	PlayerController* mpPlayerController;
	Sprite* mpSprite; 
	TankHUD* mpTankHUD; 

	std::list<GameObject*> EnviromentObjList; 
};
#endif // !Level_0_

