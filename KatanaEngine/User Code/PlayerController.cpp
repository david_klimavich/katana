#include "PlayerController.h"
#include "ControllableObject.h"
#include "../KatanaCore/CameraManager.h"

PlayerController::PlayerController()
{
	mpControllableObject = nullptr; 
}

PlayerController::~PlayerController()
{
}

void PlayerController::SetControllableObject(ControllableObject * pControllableObject_)
{
	mpControllableObject = pControllableObject_; 
}

void PlayerController::SceneEntry()
{
	// debug for god cam
	Updateable::SubmitRegister(); 

	// number keys
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_1, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_2, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_3, INPUT_EVENT_TYPE::KEY_PRESS);

	// arrow keys for main menu
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_UP, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_DOWN, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_LEFT, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_RIGHT, INPUT_EVENT_TYPE::KEY_PRESS);

	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ENTER, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ESCAPE, INPUT_EVENT_TYPE::KEY_PRESS);

	// ARROWS HELD DOWN
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_UP, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_DOWN, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_LEFT, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_ARROW_RIGHT, INPUT_EVENT_TYPE::KEY_HELD_DOWN);

	// WASD keys
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_W, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_A, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_S, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_D, INPUT_EVENT_TYPE::KEY_PRESS);

	// WASD HELD DOWN
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_W, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_A, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_S, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_D, INPUT_EVENT_TYPE::KEY_HELD_DOWN);

	Inputable::SubmitKeyRegistration(AZUL_KEY::KEY_SPACE, INPUT_EVENT_TYPE::KEY_PRESS);
}

void PlayerController::SceneExit()
{
	Updateable::SubmitDeregister();

	// num keys
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_1, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_2, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_3, INPUT_EVENT_TYPE::KEY_PRESS);


	// arrow keys for main menu
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_UP, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_DOWN, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_LEFT, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_RIGHT, INPUT_EVENT_TYPE::KEY_PRESS);
	
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ENTER, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ESCAPE, INPUT_EVENT_TYPE::KEY_PRESS);

	// arrows held down
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_UP, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_DOWN, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_LEFT, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_ARROW_RIGHT, INPUT_EVENT_TYPE::KEY_HELD_DOWN);

	// WASD keys
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_W, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_A, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_S, INPUT_EVENT_TYPE::KEY_PRESS);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_D, INPUT_EVENT_TYPE::KEY_PRESS);

	// WASD key held down
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_W, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_A, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_S, INPUT_EVENT_TYPE::KEY_HELD_DOWN);
	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_D, INPUT_EVENT_TYPE::KEY_HELD_DOWN);

	Inputable::SubmitKeyDeregistration(AZUL_KEY::KEY_SPACE, INPUT_EVENT_TYPE::KEY_PRESS);
}

void PlayerController::KeyPressed(AZUL_KEY key_)
{
	mpControllableObject->ForwardKeyPress(key_); 
}

void PlayerController::KeyReleased(AZUL_KEY key_)
{
	mpControllableObject->ForwardKeyRelease(key_);
}

void PlayerController::KeyHeldDown(AZUL_KEY key_)
{
	mpControllableObject->ForwardKeyHeldDown(key_);
}

void PlayerController::KeyHeldUp(AZUL_KEY key_)
{
	mpControllableObject->ForwardKeyHeldUp(key_);
}

void PlayerController::Update()
{
	if(Keyboard::GetKeyState(AZUL_KEY::KEY_G))
		SceneManager::GetActiveScene()->GetCameraManager().ToggleGodCam(); 
}

