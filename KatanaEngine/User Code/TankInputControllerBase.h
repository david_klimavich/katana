#ifndef TankInputControllerBase_
#define TankInputControllerBase_
#include "Keyboard.h"

class Tank;
class TankInputControllerBase {
public:
	TankInputControllerBase() = default;
	~TankInputControllerBase() = default;

	virtual void SceneEntry(Tank&) = 0;
	virtual void Update(Tank&) = 0;
	virtual void Alarm_0(Tank&) = 0;
	virtual void Alarm_1(Tank&) = 0;
	virtual void KeyPressed(AZUL_KEY, Tank&) = 0;
private:
};

#endif // !TankInputControllerBase_
