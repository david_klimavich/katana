#ifndef ButtonBase_
#define ButtonBase_

class ButtonBase
{
public:
	ButtonBase(float x_, float y_) : mpNext(nullptr), mpPrev(nullptr), xPos(x_), yPos(y_) {};

	virtual void Init() = 0; 
	virtual void Execute() = 0;
	virtual void Select() = 0; 
	virtual void Deselect() = 0; 

	ButtonBase* mpNext;
	ButtonBase* mpPrev; 

protected: 	
	float xPos;
	float yPos;

private:

};

#endif // !ButtonBase_
