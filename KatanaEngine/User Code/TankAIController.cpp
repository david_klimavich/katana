#include "TankAIController.h"
#include "TankStateBase.h"
#include "TankPatrolState.h"
#include "TankDefaultState.h"
#include "Tank.h"

TankAIController::TankAIController(Tank& player_) : mpPlayer(&player_),
mpCurrentState(new TankPatrolState) {}

void TankAIController::Update(Tank &tank_) {
	mpCurrentState->Run(tank_);
}

void TankAIController::Alarm_0(Tank& tank_) {
	mpCurrentState->Alarm_0(tank_);
}

void TankAIController::Alarm_1(Tank &tank_) {
	tank_.LookAt(*mpPlayer);
	tank_.Fire();
}
