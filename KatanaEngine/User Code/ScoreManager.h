#ifndef ScoreManager_
#define ScoreManager_

#include "DebugOut.h"

class ScoreManager
{
public: 
	static void TankDeath()
	{
		GetInstance().PrivateTankDeath(); 
	}

	static void PrintScore() { GetInstance().PrivatePrintScore(); }

private:
	ScoreManager();
	ScoreManager(const ScoreManager&) = delete;
	ScoreManager operator=(const ScoreManager&) = delete;
	~ScoreManager() = default;

	static ScoreManager* instance; 
	static ScoreManager& GetInstance()
	{
		if (!instance)
		{
			instance = new ScoreManager;
		}
		return *instance; 
	}

	// private member functions
	void PrivateTankDeath()
	{
		mTotalScore += 50; 
		// check for win condition?
	}
	void PrivatePrintScore()
	{
		DebugMsg::out("Score: %d\n", mTotalScore); 
	}
	
	
	// member variables
	int mTotalScore = 0; 
	int mTankPointValue = 50;

};

#endif