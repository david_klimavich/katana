#ifndef QuitGameButton_
#define QuitGameButton_

#include "ButtonBase.h"
#include "../KatanaCore/KatanaEngine.h" 

class Sprite; 
class QuitGameButton : public ButtonBase
{
public:
	QuitGameButton(float x_, float y_); 
	~QuitGameButton(); 

	void Init() override final; 
	void Execute() override final {
		KatanaEngine::EndGame();
	}
	void Select() override final;
	void Deselect() override final;
	
private:
	Sprite* mpQuitGame;
	Sprite* mpQuitGameSelected;
};

#endif // !QuitGameButton_
