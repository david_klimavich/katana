#include "FollowCamera.h"
#include "../KatanaCore/SceneManager.h"
#include "../KatanaCore/Scene.h"
#include "../KatanaCore/KatanaEngine.h"
FollowCamera::FollowCamera() : mpCamera(new Camera(Camera::Type::PERSPECTIVE_3D)), mLookAt(Vect(0, 0, 600)), mCamOffset(Vect(0, 7, -1)) {

	mpCamera->setViewport(0, 0, KatanaEngine::GetWindowWidth(), KatanaEngine::GetWindowHeight());
	mpCamera->setPerspective(60.0f, float(KatanaEngine::GetWindowWidth()) / float(KatanaEngine::GetWindowHeight()), 1.0f, 5000.0f);
}

FollowCamera::~FollowCamera() {
	delete mpCamera;
}

void FollowCamera::UpdateCamera(const Vect & objectPos_, const Matrix & objectRot_) {
	mRotPos = (objectRot_ * Matrix(TRANS, objectPos_));
	mCamPos = mLookAt * mRotPos;
	Vect newLookAt = mCamOffset * mRotPos;

	mpCamera->setOrientAndPosition(Vect(0, 1, 0), mCamPos, newLookAt);
	mpCamera->updateCamera();
}

Camera & FollowCamera::GetCamera() {
	return *mpCamera;
}
